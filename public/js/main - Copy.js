/* var headerHeight = $(".header").height();
$( "<div class='d-block d-lg-none' style='height:"+ headerHeight +"px'></div>").insertAfter(".header"); */

/*-----------------------------------------------------------------
    RIGHT CLICK DISABLED
-------------------------------------------------------------------*/
/* $(document).bind("contextmenu",function(e){
    return false;
});
 */
/*-----------------------------------------------------------------
    LOADER
-------------------------------------------------------------------*/
document.onreadystatechange = function() { 
    if (document.readyState !== "complete") { 
        document.querySelector("body").style.visibility = "hidden";
        document.querySelector("#loader").style.visibility = "visible";
    } else { 
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
};

/*-----------------------------------------------------------------
    NAVIGATION
-------------------------------------------------------------------*/
$(function () {
    headerSpacer = function () {
        return $('.header').outerHeight();
    }
    headerSpacer();
    $('.header-spacer').css('height', headerSpacer());
    $(window).resize(function () {
        headerSpacer();
        $('.header-spacer').css('height', headerSpacer());
    });
});

/*-----------------------------------------------------------------
    Hide Header on on scroll down
-------------------------------------------------------------------*/
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.header').outerHeight();

$(window).on('scroll', (e) => {
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();    
    if(Math.abs(lastScrollTop - st) <= delta)
        return;    
    if (st > lastScrollTop && st > navbarHeight){
        $('.header').removeClass('nav-down').addClass('nav-up');
    } else {
        if(st + $(window).height() < $(document).height()) {
            $('.header').removeClass('nav-up').addClass('nav-down');
        }
    }    
    lastScrollTop = st;
}


// Navigation
var rxtNav = $("#rxtNavWrap, .rxt-nav-overlay");
$(".rxt-nav-trigger").click(function () {
    rxtNav.toggleClass("menu-open");
});
$(".rxt-nav-overlay, .mobOnly-menu-close-link, .rxt-navigation > li > a").click(function () {
    rxtNav.removeClass("menu-open");
});

/* RXT Travel */
function viewport() {
    var t = window,
        r = "inner";
    return "innerWidth" in window || (r = "client", t = document.documentElement || document.body), t[r + "Width"]
}
viewport();
var rxtTravelRef = 1;
$(".rxt-travel").each(function () {
    $(this).wrap('<div class="rxt-travel-container-' + rxtTravelRef + '" data-ref="' + rxtTravelRef + '"></div>'), $(this).closest('[class*="rxt-travel-container-"]').before('<div class="rxt-travel-ref-' + rxtTravelRef + '"></div>'), rxtTravelRef++
}), travelMaxWidth = function () {
    $('[class*="rxt-travel-container-"]').each(function () {
        var t = $(this).find(".rxt-travel").attr("data-rxt-travel-max"),
            r = $(this).find(".rxt-travel").attr("data-rxt-travel-appendTo"),
            e = $(this).attr("data-ref"),
            a = $(this).attr("data-ref");
        viewport() <= t ? $(this).appendTo(r) : $(r).find(".rxt-travel-container-" + a).insertAfter(".rxt-travel-ref-" + e)
    })
}, travelMaxWidth(), $(window).on("resize", function () {
    travelMaxWidth()
});


/*-----------------------------------------------------------------
    PAGE SCROLL ANIMATION
-------------------------------------------------------------------*/
$(function () {
    AOS.init({
        duration: 600,
        easing: 'linear'
    });
});
/*-----------------------------------------------------------------
    BACK TO TOP
-------------------------------------------------------------------*/
$("body").append("<a class='back-to-top'></a>");
var btn = $('.back-to-top');
$(window).on('scroll', () => {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', (e) => {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '600');
});

/*-----------------------------------------------------------------
    SCROLL TO BOTTOM
-------------------------------------------------------------------*/
var heroHeight = $('.hero').outerHeight();
$('.scroll-down').on('click', () => {
    $('html, body').animate({scrollTop:heroHeight}, '1000');
})
/*-----------------------------------------------------------------
    MATERIAL INPUT FIELD
-------------------------------------------------------------------*/
function updateMaterial() {
    $('.form-group-material').each(function () {      
        if ($(this).find('.form-control').val() != undefined && $(this).find('.form-control').val() != null && $(this).find('.form-control').val() != '') {
            if ($(this).find('.form-control').val().length > 0) {
                $(this).addClass('on');
            }
        }  
    });
    $('.form-group-material .form-control').focus(function () {
        $(this).closest('.form-group-material').addClass('on');
    }).focusout(function () {
        if (!$(this).val()) {
            $(this).closest('.form-group-material').removeClass('on');
        }
    });
}
updateMaterial();

/*-----------------------------------------------------------------
    CURSOR
-------------------------------------------------------------------*/
/* $(document).mousemove(function(e) {
    const cursor = $('.cursor');
    cursor.css('left', e.clientX).css('top', e.clientY);
    $('a, .zoom-cursor').on('mouseenter', function() {
        cursor.addClass('cursor--zoom');
    });
    $('a, .zoom-cursor').on('mouseleave', function() {
        cursor.removeClass('cursor--zoom');
    });
}); */
function followCursor() {
    var cursor = $('.cursor');
    // followEffect
    function moveCursor(e) {
        var t = e.clientX + "px",
            n = e.clientY + "px";
        
        var circleCursor = anime({
            targets: '.cursor',
            duration: 50,
            left: t,
            top: n, 
            easing: 'linear'
        });
    }
    $(window).on('mousemove', moveCursor);

    // zoomEffect
    $('a, .zoom-cursor').on('mouseenter', function() {
        cursor.addClass('cursor--zoom');
    });
    $('a, .zoom-cursor').on('mouseleave', function() {
        cursor.removeClass('cursor--zoom');
    });

};

followCursor(); //Init
/*-----------------------------------------------------------------
    TYPING EFFECT
-------------------------------------------------------------------*/
/* if ($(".typing-string-01").length > 0) {
    var typewriter = new Typewriter('.typing-strings', {
        loop: true
    });
    typewriter.typeString('Web Development Company')
    .pauseFor(1000)
    .deleteAll()
    .typeString('Mobile App Development')
    .pauseFor(1000)
    .deleteAll()    
    .typeString('Artificial Intelligence')
    .pauseFor(1000)
    .deleteAll()
    .typeString('Custom Solutions')
    .start();
     
    TypewriterOne.typeString('Web Development Company')
        .start()
    
    var TypewriterTwo = new Typewriter('.typing-string-02', {
        loop: true
    });
    TypewriterTwo.typeString('Mobile App Development')
        .start()
} */




/*-----------------------------------------------------------------
    SWIPER SLIDER INIT
-------------------------------------------------------------------*/
if ($('.main-slider').length > 0) {
    /* var TypewriterOne = new Typewriter('.typing-string-01', {
        loop: true
    });
    var TypewriterTwo = new Typewriter('.typing-string-02', {
        loop: true
    });
    var TypewriterThree = new Typewriter('.typing-string-03', {
        loop: true
    });
    var TypewriterFour = new Typewriter('.typing-string-04', {
        loop: true
    });
    
    TypewriterOne.typeString('Web Development Company')
        .pauseFor(300)
        .deleteAll()
        .start() */

    var mainSlider = new Swiper('.main-slider', {
        fadeEffect: { crossFade: true },
        slidesPerView: 1,
        watchActiveIndex: true,
        loop: true,
        effect: 'fade',
        autoplay: {
            delay: 4500,
            disableOnInteraction: true,
        },
        spaceBetween: 0,
    });
     
    /* mainSlider.on('slideChange', function (e) {
        if (mainSlider.realIndex == 0) {
            TypewriterOne.typeString('Web Development Company')
            .deleteAll()
            .start()
         } else if (mainSlider.realIndex == 1) {
            TypewriterTwo.typeString('Mobile App Development')
                .pauseFor(0)
                .deleteAll()
                .start()                   
        }
         else if (mainSlider.realIndex == 2) {
            TypewriterThree.typeString('Artificial Intelligence')
                .pauseFor(0)
                .deleteAll()
                .start()
        }
        else if (mainSlider.realIndex == 3) {
            TypewriterFour.typeString('Custom Solutions')
               .pauseFor(0)
               .deleteAll()
               .start()
        }
    }); */

}

if ($('.recent-projects').length > 0) {   
    var swiper = new Swiper('.recent-projects', {
        direction: 'horizontal',
        slidesPerView: 1,
        mousewheel: true,
        autoplay: true,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            0: {
                pagination: false
            },
            768: {
                pagination: false
            },
            1024: {
                direction: 'horizontal',
            },
        }
    });
}

if ($('.testimonial-slider').length > 0) {   
    var swiper = new Swiper('.testimonial-slider', {
        slidesPerView: 1,
        spaceBetween: 10,
        watchActiveIndex: true,
        loop: true,
        autoplay: true,
        parallax: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
}

/* if ($('.app-dev-category').length > 0) {   
    var mobileDevSlider = new Swiper('.mobile-dev-slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        loopedSlides: 5,
        autoplay: false,
    });
    var mobileDevSliderThumbs = new Swiper('.mobile-dev-slider-slides', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 5,
        slideToClickedSlide: true,
        loopedSlides: 5
    });
    
    mobileDevSlider.on('slideChangeTransitionStart', function() {
        let index_currentSlide = mobileDevSlider.realIndex;
        let currentSlide = mobileDevSlider.slides[index_currentSlide]
        mobileDevSliderThumbs.slideTo(index_currentSlide, 1000, false);
    });

    mobileDevSliderThumbs.on('slideChangeTransitionStart', function() {
        let index_currentSlide = mobileDevSliderThumbs.realIndex;
        let currentSlide = mobileDevSliderThumbs.slides[index_currentSlide]
        galleryTop.slideTo(index_currentSlide, 1000, false);
    });
} */

if ($('.app-dev-category').length > 0) {   
    var mobileDevSlider = new Swiper('.mobile-dev-slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        loopedSlides: 4,
        autoplay: false,
    });
    var mobileDevSliderThumbs = new Swiper('.mobile-dev-slider-slides', {
        spaceBetween: 0,
        centeredSlides: false,
        slidesPerView: 5,
        slideToClickedSlide: true,
        loop: true,
        loopedSlides: 4
    });
    mobileDevSlider.controller.control = mobileDevSliderThumbs;
    mobileDevSliderThumbs.controller.control = mobileDevSlider;
}
/* $('.mobile-dev-slider-slides .slides-item').on('click', function (e) {
    $('.mobile-dev-slider-slides .slides-item').removeClass('active');
    var i = $(this).attr("data-id");
    $(".mobile-dev-slider .swiper-pagination-bullet:nth-child(" + i + ")").click();
    $(this).addClass('active');
}); */

 

if ($('.our-partner-slider').length > 0) {   
    var swiper = new Swiper('.our-partner-slider', {
        spaceBetween: 100,
        //slidesPerView: 5,
        loop: false,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
          },
        breakpoints: {
            0: {
                slidesPerView: 2,
                spaceBetween: 30,
                loop: true,
                autoplay: {
                    delay: 3000
                },
            },
            575: {
                slidesPerView: 3,
                autoplay: {
                    delay: 3000
                },
            },
            768: {
                slidesPerView: 4,
                loop: true,
                autoplay: {
                    delay: 3000
                },
            },
            991: {
                slidesPerView: 5
            }
        }
    });
}
if ($('.team-slider').length > 0) {   
    var swiper = new Swiper('.team-slider', {
        spaceBetween: 30,
        slidesPerView: 5,
        loop: true,
        freeMode: false,
        parallax: false,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        }, 
        breakpoints: {
            0: {
                slidesPerView: 1
            },
            575: {
                slidesPerView: 2
            },
            768: {
                slidesPerView: 2
            },
            991: {
                slidesPerView: 3
            },
            1439: {
                slidesPerView: 4
            }
        }
    });
}

if ($('.custom-dev-detail').length > 0) {
    var galleryTop = new Swiper('.custom-dev-detail', {
        spaceBetween: 10,
        parallax: true,
        loop: true,
        loopedSlides: 4
    });
    var galleryThumbs = new Swiper('.custom-dev-button', {
        spaceBetween: 10,
        centeredSlides: false,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true,
        loop: true,
        loopedSlides: 4
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
}
 



/*-----------------------------------------------------------------
    CATEGORY FILTER
-------------------------------------------------------------------*/
/* (function ( $ ) {
	$.fn.categoryFilter=function(selector){
		this.click( function() {
			var categoryValue = $(this).attr('data-filter');
			$(this).addClass('active').siblings().removeClass('active');
			if(categoryValue=="all") {
				$('.filter').show(1000);
			} else {
				$(".filter").not('.'+categoryValue).hide('3000');
            	$('.filter').filter('.'+categoryValue).show('3000');
			}
		});
	}
}(jQuery));
$('.category-filter .category-button').categoryFilter(); */

if ($('#category-list').length > 0) {   
    $('#category-list').lingGalleryFilter({
        tagContainer: $('.category-filter'),
    });
    $(".category-filter .active").click();
    $(".category-filter button").on('click', function () {
        $('.category-filter button').removeClass('active');
        $(this).addClass('active');
    });
}

/*-----------------------------------------------------------------
    TILT EFFECT
-------------------------------------------------------------------*/
if ($('.tool-tilt').length > 0) {
    $('.tool-tilt').tilt();
}

/*-----------------------------------------------------------------
    VALUE COUNTER
-------------------------------------------------------------------*/
(function($) {
	$.fn.jQuerySimpleCounter = function( options ) {
	    let settings = $.extend({
	        start:  0,
	        end:    100,
	        easing: 'swing',
	        duration: 400,
	        complete: ''
	    }, options );

	    const thisElement = $(this);

	    $({count: settings.start}).animate({count: settings.end}, {
			duration: settings.duration,
			easing: settings.easing,
			step: function() {
				let mathCount = Math.ceil(this.count);
				thisElement.text(mathCount);
			},
			complete: settings.complete
		});
	};

}(jQuery));


/*-----------------------------------------------------------------
    PARALLAX EFFECT
-------------------------------------------------------------------*/
var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
if (!isMobile) {
    if ($('.thumbnail').length > 0) {
        const images = document.querySelectorAll('.thumbnail');
        var parallax = new simpleParallax(images, {
            orientation: 'right',
            scale: 1.2,
            overflow: true,
            delay: .6,
            transition: 'cubic-bezier(0,0,0,1)'
        });
    }
    if ($('.thumbnail-left').length > 0) {
        const imagesLeft = document.querySelectorAll('.thumbnail-left');
        var parallax = new simpleParallax(imagesLeft, {
            orientation: 'left',
            scale: 1.2,
            overflow: true,
            delay: .6,
            transition: 'cubic-bezier(0,0,0,1)'
        });
    }
}

/*-----------------------------------------------------------------
    LOCAL STORAGE FOR CONTACT PAGE DROPDOWN SELECTION
-------------------------------------------------------------------*/
if (typeof(Storage) !== "undefined") {
    $('#webDevelopment').on('click', () => {
        localStorage.setItem('clickEventName', 'webdevelopment');
    });
    $('#mobileDevelopment').on('click', () => {
        localStorage.setItem('clickEventName', 'mobiledevelopment');
    });
    $('#aiDevelopment').on('click', () => {
        localStorage.setItem('clickEventName', 'aidevelopment');
    });
    $('#customSolution').on('click', () => {
        localStorage.setItem('clickEventName', 'customSolutiondevelopment');
    });
} else {
    $(document).text('No web storage Support.');
}