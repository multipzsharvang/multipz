<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Mail;





class WebsiteController extends Controller

{

    public function home()

    {

        return view('index');
    }

    public function work()

    {

        return view('work');
    }

    public function service()

    {

        return view('service');
    }



    public function contact()

    {

        return view('contact');
    }

    public function blog()

    {

        return view('blog');
    }

    public function about()

    {

        return view('about');
    }



    public function email(Request $request)
    {



        // dd(\Request::ip());




        \Mail::send(
            'contact_email',
            [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'selection' => $request->get('selection'),
                'country' => $request->get('country'),
                'company' => $request->get('company'),
                'projectdetails' => $request->get('projectdetails'),
                'year' => $request->get('year'),
                'greeting' => $request->get('greeting'),
                'indianTime' => $request->get('indianTime')
            ],
            function ($m) use ($request) {
                $m->from('info@multipz.com');
                $m->to('multipz.nikunjkothiya@gmail.com')->subject('Inquiry from Customer');
            }
        );





        //
        //    \Mail::send('contact_email',
        //
        //         array(
        //
        //             'name' => $request->get('name'),
        //
        //             'email' => $request->get('email'),
        //
        //             'selection' => $request->get('selection'),
        //
        //             'country' => $request->get('country'),
        //
        //             'company' => $request->get('company'),
        //
        //             'projectdetails' => $request->get('projectdetails'),
        //
        //             'year' => $request->get('year'),
        //
        //             'greeting' => $request->get('greeting'),
        //
        //             'indianTime' => $request->get('indianTime'),
        //
        //             
        //
        //         ), function($message) use ($request)
        //
        //           {
        //
        //              $message->from($request->email);
        //
        //              $message->subject("Inquiry from Customer");
        //
        //              $message->to('multipz.nikunjkothiya@gmail.com');
        //
        //                    //  ->cc('multipz.nikunjkothiya+14@gmail.com');  // for cc and bcc uncomment this
        //
        //                    //  ->bcc('multipz.nikunjkothiya+14@gmail.com');
        //
        //           });
        //
        //
        //
        //        
        //
        //           \Mail::send('feedback',
        //
        //         array(
        //
        //             'name' => $request->get('name'),
        //
        //             'year' => $request->get('year'),
        //
        //             'greeting' => $request->get('greeting'),
        //
        //             'indianTime' => $request->get('indianTime'),
        //
        //            
        //
        //             
        //
        //         ), function($messages) use ($request)
        //
        //           {
        //
        //              $messages->from('multipz.nikunjkothiya@gmail.com');
        //
        //              $messages->subject("Feedback Response");
        //
        //              $messages->to($request->email);
        //
        //           });
        \Mail::send(
            'feedback',
            [
                'name' => $request->get('name'),
   
                'year' => $request->get('year'),
   
                'greeting' => $request->get('greeting'),
   
                'indianTime' => $request->get('indianTime'),
            ],
            function ($m) use ($request) {
                $m->from('info@multipz.com');
                $m->to($request->email)->subject('Feedback Response');
            }
        );
        //
        //
        //
        return view('thank')->with(['success' => 'Thank you for contact with us!', 'name' => $request->get('name'), 'year' => $request->get('year'), 'greeting' => $request->get('greeting'), 'indianTime' => $request->get('indianTime'), 'messages' => $this]);
    }



    public function discusss(Request $request)
    {

        //dd($request->all());



        //from client to company ceo mail
        \Mail::send(
            'discuss_email',
            [
                'name' => $request->get('name'),

                'email' => $request->get('email'),

                'phone' => $request->get('phone'),

                'country' => $request->get('country'),

                'messagess' => $request->get('messagess'),

                'year' => $request->get('year'),

                'greeting' => $request->get('greeting'),

                'indianTime' => $request->get('indianTime'),
            ],
            function ($m) use ($request) {
                $m->from('info@multipz.com');
                $m->to('multipz.nikunjkothiya@gmail.com')->subject('Inquiry from Customer');
            }
        );


  //     \Mail::send(
  //         'discuss_email',

  //         array(

  //             'name' => $request->get('name'),

  //             'email' => $request->get('email'),

  //             'phone' => $request->get('phone'),

  //             'country' => $request->get('country'),

  //             'messagess' => $request->get('messagess'),

  //             'year' => $request->get('year'),

  //             'greeting' => $request->get('greeting'),

  //             'indianTime' => $request->get('indianTime'),

  //         ),
  //         function ($query) use ($request) {

  //             $query->from($request->email);

  //             $query->subject("Inquiry from Customer");

  //             $query->to('multipz.nikunjkothiya@gmail.com');

  //             // ->cc('multipz.nikunjkothiya+14@gmail.com');  // for cc and bcc uncomment this

  //             // ->bcc('multipz.nikunjkothiya+14@gmail.com');

  //         }
  //     );



        // sent back mail to client 

 //      \Mail::send(
 //          'feedback',

 //          array(

 //              'name' => $request->get('name'),

 //              'year' => $request->get('year'),

 //              'greeting' => $request->get('greeting'),

 //              'indianTime' => $request->get('indianTime'),



 //          ),
 //          function ($message) use ($request) {

 //              $message->from('multipz.nikunjkothiya@gmail.com');

 //              $message->subject("Feedback");

 //              $message->to($request->email);
 //          }
 //      );
         
        \Mail::send(
            'feedback',
            [
                'name' => $request->get('name'),

                'year' => $request->get('year'),

                'greeting' => $request->get('greeting'),

                'indianTime' => $request->get('indianTime'),
            ],
            function ($m) use ($request) {
                $m->from($request->email);
                $m->to('info@multipz.com')->subject('Feedback Response');
            }
        );


        return view('thank')->with(['success' => 'Thank you for contact with us!', 'name' => $request->get('name'), 'year' => $request->get('year'), 'greeting' => $request->get('greeting'), 'indianTime' => $request->get('indianTime'), 'messages' => $this]);
    }
}
