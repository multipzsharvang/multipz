/* var headerHeight = $(".header").height();
$( "<div class='d-block d-lg-none' style='height:"+ headerHeight +"px'></div>").insertAfter(".header"); */

/*-----------------------------------------------------------------
    RIGHT CLICK DISABLED
-------------------------------------------------------------------*/
$(document).bind("contextmenu",function(e){
    return false;
});

/*-----------------------------------------------------------------
    LOADER
-------------------------------------------------------------------*/
document.onreadystatechange = function() { 
    if (document.readyState !== "complete") { 
        document.querySelector("body").style.visibility = "hidden";
        document.querySelector("#loader").style.visibility = "visible";
    } else { 
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
};

/*-----------------------------------------------------------------
    NAVIGATION
-------------------------------------------------------------------*/
$(function () {
    headerSpacer = function () {
        return $('.header').outerHeight();
    }
    headerSpacer();
    $('.header-spacer').css('height', headerSpacer());
    $(window).resize(function () {
        headerSpacer();
        $('.header-spacer').css('height', headerSpacer());
    });
});

 
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.header').removeClass('nav-up').addClass('nav-down');
        }
    }
    
    lastScrollTop = st;
}


// Navigation
var rxtNav = $("#rxtNavWrap, .rxt-nav-overlay");
$(".rxt-nav-trigger").click(function () {
    rxtNav.toggleClass("menu-open");
});
$(".rxt-nav-overlay, .mobOnly-menu-close-link, .rxt-navigation > li > a").click(function () {
    rxtNav.removeClass("menu-open");
});

/* RXT Travel */
function viewport() {
    var t = window,
        r = "inner";
    return "innerWidth" in window || (r = "client", t = document.documentElement || document.body), t[r + "Width"]
}
viewport();
var rxtTravelRef = 1;
$(".rxt-travel").each(function () {
    $(this).wrap('<div class="rxt-travel-container-' + rxtTravelRef + '" data-ref="' + rxtTravelRef + '"></div>'), $(this).closest('[class*="rxt-travel-container-"]').before('<div class="rxt-travel-ref-' + rxtTravelRef + '"></div>'), rxtTravelRef++
}), travelMaxWidth = function () {
    $('[class*="rxt-travel-container-"]').each(function () {
        var t = $(this).find(".rxt-travel").attr("data-rxt-travel-max"),
            r = $(this).find(".rxt-travel").attr("data-rxt-travel-appendTo"),
            e = $(this).attr("data-ref"),
            a = $(this).attr("data-ref");
        viewport() <= t ? $(this).appendTo(r) : $(r).find(".rxt-travel-container-" + a).insertAfter(".rxt-travel-ref-" + e)
    })
}, travelMaxWidth(), $(window).on("resize", function () {
    travelMaxWidth()
});


/*-----------------------------------------------------------------
    PAGE SCROLL ANIMATION
-------------------------------------------------------------------*/
$(function () {
    AOS.init({
        duration: 600,
        easing: 'linear'
    });
});
/*-----------------------------------------------------------------
    BACK TO TOP
-------------------------------------------------------------------*/
$("body").append("<a class='back-to-top'></a>");
var btn = $('.back-to-top');
$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '600');
});

/*-----------------------------------------------------------------
    SCROLL TO BOTTOM
-------------------------------------------------------------------*/
var heroHeight = $('.hero').outerHeight();
$('.scroll-down').on('click', () => {
    $('html, body').animate({scrollTop:heroHeight}, '1000');
})
/*-----------------------------------------------------------------
    MATERIAL INPUT FIELD
-------------------------------------------------------------------*/
function updateMaterial() {
    $('.form-group-material').each(function () {      
        if ($(this).find('.form-control').val() != undefined && $(this).find('.form-control').val() != null && $(this).find('.form-control').val() != '') {
            if ($(this).find('.form-control').val().length > 0) {
                $(this).addClass('on');
            }
        }  
    });
    $('.form-group-material .form-control').focus(function () {
        $(this).closest('.form-group-material').addClass('on');
    }).focusout(function () {
        if (!$(this).val()) {
            $(this).closest('.form-group-material').removeClass('on');
        }
    });
}
updateMaterial();

/*-----------------------------------------------------------------
    CURSOR
-------------------------------------------------------------------*/
$(document).mousemove(function(e) {
    const cursor = $('.cursor');
    const target = $(event.target);    
    cursor.css('left', e.clientX).css('top', e.clientY);
    $('a, .zoom-cursor').on('mouseenter', function() {
        cursor.addClass('cursor--zoom');
    });
    $('a, .zoom-cursor').on('mouseleave', function() {
        cursor.removeClass('cursor--zoom');
    });
});

/*-----------------------------------------------------------------
    SWIPER SLIDER INIT
-------------------------------------------------------------------*/
if ($('.recent-projects').length > 0) {   
    var swiper = new Swiper('.recent-projects', {
        direction: 'horizontal',
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            0: {
                pagination: false
            },
            768: {
                pagination: false
            },
            1024: {
                direction: 'vertical',
            },
        }
    });
}

if ($('.testimonial-slider').length > 0) {   
    var swiper = new Swiper('.testimonial-slider', {
        slidesPerView: 1,
        spaceBetween: 10,
        watchActiveIndex: true,
        loop: true,
        autoplay: false,
        parallax: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            0: {
                
            },
            991: {
                
            }
        }
       /*  pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }, */
        
    });
}

if ($('.mobile-dev-slider').length > 0) {   
    var swiper = new Swiper('.mobile-dev-slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: false,
        autoplay: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        
    });
}
$('.mobile-dev-slider-slides .slides-item').on('click', function (e) {
    $('.mobile-dev-slider-slides .slides-item').removeClass('active');
    var i = $(this).attr("data-id");
    $(".mobile-dev-slider .swiper-pagination-bullet:nth-child(" + i + ")").click();
    $(this).addClass('active');
});

 

if ($('.our-partner-slider').length > 0) {   
    var swiper = new Swiper('.our-partner-slider', {
        spaceBetween: 0,
        slidesPerView: 8,
        loop: true,
        freeMode: true,
        parallax: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
          },
        breakpoints: {
            0: {
                slidesPerView: 2
            },
            575: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 4
            },
            991: {
                slidesPerView: 5
            },
            1100: {
                slidesPerView: 8
            }
        }
    });
}
if ($('.team-slider').length > 0) {   
    var swiper = new Swiper('.team-slider', {
        spaceBetween: 0,
        slidesPerView: 5,
        loop: true,
        freeMode: true,
        parallax: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
          },
        breakpoints: {
            0: {
                slidesPerView: 1
            },
            575: {
                slidesPerView: 2
            },
            768: {
                slidesPerView: 3
            },
            991: {
                slidesPerView: 4
            },
            1100: {
                slidesPerView: 5
            }
        }
    });
}



/*-----------------------------------------------------------------
    CATEGORY FILTER
-------------------------------------------------------------------*/
/* (function ( $ ) {
	$.fn.categoryFilter=function(selector){
		this.click( function() {
			var categoryValue = $(this).attr('data-filter');
			$(this).addClass('active').siblings().removeClass('active');
			if(categoryValue=="all") {
				$('.filter').show(1000);
			} else {
				$(".filter").not('.'+categoryValue).hide('3000');
            	$('.filter').filter('.'+categoryValue).show('3000');
			}
		});
	}
}(jQuery));
$('.category-filter .category-button').categoryFilter(); */

if ($('#category-list').length > 0) {   
    $('#category-list').lingGalleryFilter({
        tagContainer: $('.category-filter'),
    });
    $(".category-filter .active").click();
    $(".category-filter button").on('click', function () {
        $('.category-filter button').removeClass('active');
        $(this).addClass('active');
    });
}

/*-----------------------------------------------------------------
    TILT EFFECT
-------------------------------------------------------------------*/
if ($('.tool-tilt').length > 0) {   
    $('.tool-tilt').tilt();
}

/*-----------------------------------------------------------------
    VALUE COUNTER
-------------------------------------------------------------------*/
(function($) {
	$.fn.jQuerySimpleCounter = function( options ) {
	    let settings = $.extend({
	        start:  0,
	        end:    100,
	        easing: 'swing',
	        duration: 400,
	        complete: ''
	    }, options );

	    const thisElement = $(this);

	    $({count: settings.start}).animate({count: settings.end}, {
			duration: settings.duration,
			easing: settings.easing,
			step: function() {
				let mathCount = Math.ceil(this.count);
				thisElement.text(mathCount);
			},
			complete: settings.complete
		});
	};

}(jQuery));


/*-----------------------------------------------------------------
    PARALLAX EFFECT
-------------------------------------------------------------------*/
if ($('.thumbnail').length > 0) {
    const images = document.querySelectorAll('.thumbnail');
    var parallax = new simpleParallax(images, {
        orientation: 'right',
        scale: 1.2,
        overflow: true,
        delay: .6,
        transition: 'cubic-bezier(0,0,0,1)'
    });
}

/*-----------------------------------------------------------------
    LOCAL STORAGE FOR CONTACT PAGE DROPDOWN SELECTION
-------------------------------------------------------------------*/
if (typeof(Storage) !== "undefined") {
    $('#webDevelopment').click(function () {
        localStorage.setItem('clickEventName', 'webdevelopment');
    });
    $('#mobileDevelopment').click(function () {
        localStorage.setItem('clickEventName', 'mobiledevelopment');
    });
} else {
    $(document).text('No web storage Support.');
}