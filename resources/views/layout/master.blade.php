<!DOCTYPE html>
<html lang="en">
<html>

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta_description')">
    <link rel="canonical" href={{URL::current()}} />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta name=”twitter:card” content=”summary” />
    <meta name=”twitter:title” content="@yield('title')" />
    <meta name=”twitter:description” content="@yield('meta_description')" />
    <meta name=”twitter:url” content="{{URL::current()}}" />

    @yield('image')
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&family=Poppins:wght@400;500;700;800;900&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('public/img/favicon.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" media="screen" >
    <link rel="stylesheet" href="{{asset('public/css/aos.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('public/css/swiper.min.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('public/css/grid.css')}}" media="screen" >
    <link rel="stylesheet" href="{{asset('public/css/main.css')}}" media="screen" >

    <script type="application/ld+json">
        {
        "@context": "https://schema.org",
        "@type": "LocalBusiness",
        "name": "Multipz Technology",
        "image": "https://www.multipz.com/public/img/logo-dark.svg",
        "@id": "",
        "url": "https://www.multipz.com/",
        "telephone": "079-2518537",
        "priceRange": "$",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "415, 4th Floor, Maruti Plaza",
            "addressLocality": "Ahmedabad",
            "postalCode": "382346",
            "addressCountry": "IN"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 23.051185,
            "longitude": 72.6438052
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
            "Monday",
            "Tuesday",
            "Thursday",
            "Wednesday",
            "Friday",
            "Saturday"
            ],
            "opens": "09:00",
            "closes": "20:00"
        },
        "sameAs": [
            "https://www.facebook.com/multipzservices/",
            "https://twitter.com/multipztech",
            "https://www.instagram.com/multipztechnology/",
            "https://www.linkedin.com/company/multipztechnology/",
            "https://in.pinterest.com/multipztechnology/"
        ] 
        }
    </script>
</head>

</html>

<body>
    <!--Header-->
    <div id="loader">
        <span class="loading"></span>
    </div>



    @if ( Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" id="emailmsg" role="alert">
        <strong>Dear {{ Session::get('name') }},</strong> <i class="fa fa-heart text-danger"></i> {{ Session::get('success') }}
        &nbsp;Please check your email.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif


    <header class="header nav-down bg-secondary">
        <div class="container-fluid d-flex align-items-center">
            <div class="row align-items-center flex-grow-1">
                <div class="col-auto">
                    <a class="logo" href="{{Route('index')}}">
                        <img class="img-fluid logo-dark" src="{{asset('public/img/logo-dark.svg')}}" alt="multipz">
                    </a>
                </div>
                <div class="ml-lg-auto">
                    <div class="rxt-travel" data-rxt-travel-max="991" data-rxt-travel-appendTo="#rxtNavTravel">
                        <div id="rxtNavWrap" class="rxt-navigation-wrap">
                            <span class="mobOnly-menu-close-link">
                                <i class="mobOnly-menu-close fa fa-times" aria-hidden="true"></i>
                            </span>
                            <div id="listQuickLinks"></div>
                            <div class="d-flex flex-wrap">
                                <ul class="rxt-navigation px-lg-3">
                                    <li class="{{ Route::currentRouteNamed('about') ? 'active' : '' }}">
                                        <a href="{{Route('about')}}">About us</a>
                                    </li>
                                    <li class="{{ Route::currentRouteNamed('service')||Route::currentRouteNamed('hire-dedicated-developer') ||Route::currentRouteNamed('artificial-intelligence-development')||Route::currentRouteNamed('mobile-app-development')||Route::currentRouteNamed('web-development')||Route::currentRouteNamed('custom-solutions') ? 'active' : '' }} has-sub">
                                        <a href="{{Route('service')}}">Services</a>
                                        <ul>
                                            <li>
                                                <a href="{{Route('web-development')}}">Website Development</a>
                                            </li>
                                            <li>
                                                <a href="{{Route('mobile-app-development')}}">Mobile App Development</a>
                                            </li>
                                            <li>
                                                <a href="{{Route('artificial-intelligence-development')}}">Artificial Intelligence</a>
                                            </li>
                                            <li>
                                                <a href="{{Route('custom-solutions')}}">Custom Solutions</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="{{ Route::currentRouteNamed('work') ||Route::currentRouteNamed('presenta')||Route::currentRouteNamed('simpilli')||Route::currentRouteNamed('holdeed')||Route::currentRouteNamed('gpbo')||Route::currentRouteNamed('holdeed')||Route::currentRouteNamed('callhippo')||Route::currentRouteNamed('joykick') ? 'active' : '' }}">
                                        <a href="{{Route('work')}}">Work</a>
                                    </li>
                                    <li class="{{ Route::currentRouteNamed('blog')||Route::currentRouteNamed('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality')||Route::currentRouteNamed('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App')||Route::currentRouteNamed('Which-is-the-Best-Flutter-or-React-Native')||Route::currentRouteNamed('What-to-choose-Open-Source-or-Custom-solutions') ? 'active' : '' }}">
                                        <a href="{{Route('blog')}}">Blog</a>
                                    </li>
                                    <li>
                                        <a class="btn btn-accent" href="{{Route('contact')}}" id="contat_us"><i class="fa fa-phone fa-rotate-90 mr-1"></i>Contact Us</a>
                                        <!-- Route::currentRouteNamed('blog') || -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <span class="rxt-nav-overlay"></span>
                    </div>
                </div>

                <div class="col-auto align-self-center mobOnly-rxt-nav-trigger-wrap ml-auto">
                    <span class="rxt-nav-trigger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>
            </div>
        </div>
    </header>

    <div class="d-block header-spacer">
    </div>
    <div id="rxtNavTravel" class="headerOnTravel"></div>


    <!--End Header -->

    <!--Section-->


    @yield('content')


    <!--EndSection-->

    <!--Footer-->
    <footer>

        @if (\Request::is('contact-us'))
        <div class="bg-primary pt-lg-5">
            <div class="container-fluid px-lg-5 pt-5 pb-2 text-white">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xl-3 mb-3 mb-md-0 text-center text-md-left">
                        <img src="{{asset('public/img/logo-light.png')}}" alt="Multipz Logo" class="img-fluid mb-3">
                        <address>
                            <div class="h3 font-weight-bold">India</div>
                            <div class="media align-items-baseline d-flex justify-content-center">
                                <i class="d-none d-sm-block fa fa-map-marker mr-2"></i>
                                <div>
                                    415, 4th Floor, Maruti Plaza, Krishnanagar, Ahmedabad - 382346, India
                                </div>
                            </div>
                        </address>
                    </div>
                    <div class="d-none d-md-block col-md-2 col-lg-2 col-xl-2 offset-xl-1">
                        <div class="h4 font-weight-bold mb-3">Quick Links</div>
                        <ul class="list-unstyled">
                            <li class="my-3"><a class="text-white" href="{{Route('about')}}">About Us</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('service')}}">Services</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('work')}}">Work</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('blog')}}">Blog</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="text-center text-sm-left col-md-2 mb-3 mb-md-0 col-lg-2 col-xl-2 offset-xl-1">
                        <div class="h4 font-weight-bold mb-3">Services</div>
                        <ul class="list-unstyled">
                            <li class="my-3"><a class="text-white" href="{{Route('web-development')}}">Website Development</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('mobile-app-development')}}">Mobile App Development</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('artificial-intelligence-development')}}">Artificial Intelligence</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('custom-solutions')}}">Custom Solutions</a></li>
                        </ul>
                    </div>
                    <!-- <div class="d-none d-md-block col-md-3 col-lg-3 col-xl-3">
                         <div class="h4 font-weight-bold mb-3">Address</div>
                         <div class="media align-items-baseline mb-3">
                             <i class="fa fa-map-pin mr-2"></i>
                             <div class="media-body">
                                 415, 4th Flor, Maruti Plaza, Krishnanagar, Ahmedabad 382346, India
                             </div>
                         </div>
                         <div class="media align-items-baseline">
                             <i class="fa fa-map-pin mr-2"></i>
                             <div class="media-body">
                                 2nd Floor Vaibhav Chamber, Opp Mira Gems, Minibazar, Surat 395006, India
                             </div>
                         </div>
                     </div> -->
                    <div class="col-md-3 col-lg-3 col-xl-2 offset-xl-1 text-center text-md-left">
                        <div class="h4 font-weight-bold mb-3">Contact Info</div>
                        <ul class="list-unstyled">
                            <li class="mb-3"><a class="text-white" role="button" href="mailto:info@multipz.com"><i class="fa fa-envelope mr-2"></i>info@multipz.com</a></li>
                            <li class="mb-3"><a class="text-white" role="button" href="tel:+918200554263"><i class="fa fa-mobile-alt mr-2"></i>+91-820-055-4263</a></li>
                            <!-- <li><a class="text-white" role="button" href="tel:0792518537"><i class="fa fa-phone mr-2 fa-rotate-90"></i>079-2518537</a></li> -->
                        </ul>
                    </div>

                    <div class="col-12 text-center my-3">
                        <nav class="social-icon">
                            <a href="https://www.facebook.com/multipzservices/" class="facebook align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.linkedin.com/company/multipztechnology/" class="linkedin align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                          <!--  <a href="skype:hitesh.devit?call" class="skype align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-skype"></i></a> -->
                            <a href="https://www.youtube.com/channel/UC7k0wfx0-poLZbfunciCDZw/videos?view_as=subscriber" class="youtube align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://twitter.com/multipztech" class="twitter align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.behance.net/MultipzTechnology" class="behance align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-behance"></i></a>
                            <a href="https://in.pinterest.com/multipztechnology/" class="pinterest align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-pinterest"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="text-center copyright py-2 py-lg-3">
                <p class="text-secondary small mb-0">&copy;<?php echo date("Y"); ?> - All Rights Reserved By Multipz Technology with <i class="fa fa-heart text-danger"></i></p>
            </div>
        </div>

        @else
        <div class="bg-light py-3 py-lg-0">
            <div class="overflow-hidden">
                <div class="d-none d-lg-block animate-building slow" style="height: 300px; background: url('public/img/bg-contact-form.png') repeat-x center bottom / 20%; width: 5076px;"></div>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <form class="card py-lg-4 shadow-lg footer-contact-form needs-validation" action="{{Route('discuss')}}" method="post" novalidate>
                            {{csrf_field()}}
                            <div class="card-body">
                                <div class="h4 font-weight-bold mb-3">Let's Discuss Your Project</div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <input class="form-control" type="text" placeholder="Name" name="name" required>
                                        <div class="invalid-feedback">
                                            Please Enter Your Name.
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" type="email" placeholder="Email" name="email" required>
                                        <div class="invalid-feedback">
                                            Please Enter a Valid Email Address.
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="number" class="form-control" pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==18) return false;" placeholder="Phone No" name="phone" required>
                                        <div class="invalid-feedback">
                                            Please Enter a Mobile Number.
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <!-- <input class="form-control" type="text" placeholder="Country" name="country" required> -->
                                        <select required class="custom-select" id="looking-for" name="country">
                                            <option selected disabled value="">Select Country</option>
                                            <option value="Afganistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bonaire">Bonaire</option>
                                            <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Canary Islands">Canary Islands</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Channel Islands">Channel Islands</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos Island">Cocos Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote DIvoire">Cote DIvoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curaco">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Ter">French Southern Ter</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Great Britain">Great Britain</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="India">India</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle of Man">Isle of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea North">Korea North</option>
                                            <option value="Korea Sout">Korea South</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Midway Islands">Midway Islands</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Nambia">Nambia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                            <option value="Nevis">Nevis</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau Island">Palau Island</option>
                                            <option value="Palestine">Palestine</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Phillipines">Philippines</option>
                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="St Barthelemy">St Barthelemy</option>
                                            <option value="St Eustatius">St Eustatius</option>
                                            <option value="St Helena">St Helena</option>
                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St Maarten">St Maarten</option>
                                            <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                            <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                            <option value="Saipan">Saipan</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="Samoa American">Samoa American</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Tahiti">Tahiti</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                            <option value="United States of America">United States of America</option>
                                            <option value="Uraguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City State">Vatican City State</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                            <option value="Wake Island">Wake Island</option>
                                            <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zaire">Zaire</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please Select a Country.
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <textarea placeholder="Write Your Message" class="form-control" name="messagess" rows="3"></textarea>
                                    </div>

                                    <div class="form-group col-12 mb-lg-4">
                                        <!-- <label for="captcha" class="small">Math Captcha <sup class="text-danger">*</sup></label><br /> -->
                                        <div class="form-row align-items-center">
                                            <div class="col-auto">
                                                <strong id="question" class="mb-0"></strong> <strong>=</strong>
                                            </div>
                                            <div class="col-auto">
                                                <input id="ans" type="text" style="max-width: 70px;height: calc(1.5em + 0.75rem + 2px);padding: 0.375rem 0.75rem;border: 1px solid #ced4da;border-radius: 0.25rem;" required>
                                            </div>
                                            <div class="col-auto">
                                                <a type="reset" id="reset" value="reset"><i class="fas fa-sync-alt"></i></a>
                                            </div>
                                            <div class="col-auto">
                                                <div id="message" style="font-size: 14px">Please Enter a Captcha <i class="fa fa-exclamation-circle" style="color: #B8860B;" aria-hidden="true"></i></div>
                                                <div id="success" style="font-size: 14px">Verify complete <i class="fas fa-check-circle" style="color: green;"></i></div>
                                                <div id="fail" style="font-size: 14px">Verify failed <i class="fas fa-exclamation-triangle" style="color: red;"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" class="bg-light form-control" id="year" row="5" value="" name="year">
                                    <input type="hidden" class="bg-light form-control" id="greeting" row="5" value="" name="greeting">
                                    <input type="hidden" class="bg-light form-control" id="indianTime" row="5" value="" name="indianTime">
                                    <div class="col-12 text-center">
                                        <button type="submit" onclick="myFunction()" class="btn btn-accent btn-lg zoom-cursor">Let's Talk</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="bg-primary pt-lg-5">
            <div class="container-fluid px-lg-5 pt-5 pb-2 text-white">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xl-3 mb-3 mb-md-0 text-center text-md-left">
                        <img src="{{asset('public/img/logo-light.png')}}" alt="Multipz Logo" class="img-fluid mb-3">
                        <address>
                            <div class="h3 font-weight-bold">India</div>
                            <div class="media align-items-baseline d-flex justify-content-center">
                                <i class="d-none d-sm-block fa fa-map-marker mr-2"></i>
                                <div>
                                    415, 4th Floor, Maruti Plaza, Krishnanagar, Ahmedabad - 382346, India
                                </div>
                            </div>
                        </address>
                    </div>
                    <div class="d-none d-md-block col-md-2 col-lg-2 col-xl-2 offset-xl-1">
                        <div class="h4 font-weight-bold mb-3">Quick Links</div>
                        <ul class="list-unstyled">
                            <li class="my-3"><a class="text-white" href="{{Route('about')}}">About us</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('service')}}">Services</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('work')}}">Work</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('blog')}}">Blog</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="text-center text-sm-center text-md-left mb-3 mb-md-0 col-md-2 col-lg-2 col-xl-2 offset-xl-1">
                        <div class="h4 font-weight-bold mb-3">Services</div>
                        <ul class="list-unstyled">
                            <li class="my-3"><a class="text-white" href="{{Route('web-development')}}">Website Development</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('mobile-app-development')}}">Mobile App Development</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('artificial-intelligence-development')}}">Artificial Intelligence</a></li>
                            <li class="my-3"><a class="text-white" href="{{Route('custom-solutions')}}">Custom Solutions</a></li>
                        </ul>
                    </div>
                    <!-- <div class="d-none d-md-block col-md-3 col-lg-3 col-xl-3">
                         <div class="h4 font-weight-bold mb-3">Address</div>
                         <div class="media align-items-baseline mb-3">
                             <i class="fa fa-map-pin mr-2"></i>
                             <div class="media-body">
                                 415, 4th Flor, Maruti Plaza, Krishnanagar, Ahmedabad 382346, India
                             </div>
                         </div>
                         <div class="media align-items-baseline">
                             <i class="fa fa-map-pin mr-2"></i>
                             <div class="media-body">
                                 2nd Floor Vaibhav Chamber, Opp Mira Gems, Minibazar, Surat 395006, India
                             </div>
                         </div>
                     </div> -->
                    <div class="col-md-3 col-lg-3 col-xl-2 offset-xl-1 text-center text-md-left">
                        <div class="h4 font-weight-bold mb-3">Contact Info</div>
                        <ul class="list-unstyled">
                            <li class="mb-3"><a class="text-white" role="button" href="mailto:info@multipz.com"><i class="fa fa-envelope mr-2"></i>info@multipz.com</a></li>
                            <li class="mb-3"><a class="text-white" role="button" href="tel:+918200554263"><i class="fa fa-mobile-alt mr-2"></i>+91-820-055-4263</a></li>
                            <!-- <li><a class="text-white" role="button" href="tel:0792518537"><i class="fa fa-phone mr-2 fa-rotate-90"></i>079-2518537</a></li> -->
                        </ul>
                    </div>

                    <div class="col-12 text-center my-3">
                        <nav class="social-icon">
                            <a href="https://www.facebook.com/multipzservices/" class="facebook align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.linkedin.com/company/multipztechnology/" class="linkedin align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                         <!--   <a href="skype:hitesh.devit?call" class="skype align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-skype"></i></a> -->
                            <a href="https://www.youtube.com/channel/UC7k0wfx0-poLZbfunciCDZw/videos?view_as=subscriber" class="youtube align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://twitter.com/multipztech" class="twitter align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.instagram.com/multipztechnology/" class="instagram align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-instagram"></i></a>
                            <a href="https://www.behance.net/MultipzTechnology" class="behance align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-behance"></i></a>
                            <a href="https://in.pinterest.com/multipztechnology/" class="pinterest align-items-center avatar-sm d-inline-flex justify-content-center" target="_blank"><i class="fab fa-pinterest"></i></a>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="text-center copyright py-2 py-lg-3">
                <p class="text-secondary small mb-0">&copy; 
                    <?php echo date("Y"); ?> - All Rights Reserved By Multipz Technology with <i class="fa fa-heart text-danger"></i></p>
            </div>
        </div>
        @endif






    </footer>
    <div class="d-none d-lg-block cursor"></div>
    <!--End Footer-->

    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="sr-only" style="enable-background:new 0 0 512 512;">
        <defs>
            <svg id="webDevelopment">
                <g>
                    <path d="M502,0H343.13c-4.045,0-7.691,2.437-9.239,6.173c-1.547,3.737-0.691,8.038,2.168,10.898l37.42,37.42L247.533,180.437
                        L131.215,64.119c-3.906-3.904-10.236-3.904-14.143,0L2.955,178.236c-3.905,3.905-3.905,10.237,0,14.143
                        c1.953,1.952,4.512,2.929,7.071,2.929s5.118-0.977,7.071-2.929L124.144,85.333l139.309,139.309
                        c1.953,1.952,4.512,2.929,7.071,2.929s5.118-0.977,7.071-2.929c3.905-3.905,3.905-10.237,0-14.143l-15.919-15.919L387.621,68.634
                        l55.839,55.839L341.967,225.967c-3.905,3.905-3.905,10.237,0,14.143c1.953,1.952,4.512,2.929,7.071,2.929s5.118-0.977,7.071-2.929
                        l101.493-101.494l37.326,37.326c1.913,1.913,4.471,2.929,7.073,2.929c1.288,0,2.588-0.249,3.825-0.761
                        c3.736-1.548,6.173-5.194,6.173-9.239V10C512,4.478,507.522,0,502,0z M492,144.728L367.272,20H492V144.728z" />
                    <path d="M331.359,250.72c-1.859-1.87-4.439-2.93-7.069-2.93s-5.21,1.06-7.07,2.93c-1.87,1.86-2.93,4.44-2.93,7.07
                        s1.06,5.21,2.93,7.069c1.86,1.86,4.44,2.931,7.07,2.931s5.21-1.07,7.069-2.931c1.86-1.859,2.931-4.439,2.931-7.069
                        S333.22,252.58,331.359,250.72z" />
                    <path d="M192.487,265.355l-54.202-54.201l16.139-16.139c3.906-3.905,3.906-10.237,0.001-14.142
                        c-3.906-3.904-10.236-3.904-14.143,0L2.951,318.204c-3.905,3.905-3.905,10.237,0,14.143c1.953,1.952,4.512,2.929,7.071,2.929
                        c2.559,0,5.118-0.977,7.071-2.929l107.05-107.05l54.202,54.201c1.953,1.952,4.512,2.929,7.071,2.929s5.118-0.977,7.071-2.929
                        C196.392,275.593,196.392,269.261,192.487,265.355z" />
                    <path d="M256.467,366.902L256,366.901c-5.522,0-10,4.478-10,10c0,5.522,4.478,10,10,10l0.319,0.001c0.025,0,0.051,0,0.076,0
                        c5.487,0,9.957-4.429,9.998-9.926C266.434,371.454,261.99,366.943,256.467,366.902z" />
                    <path d="M502,492h-17.54v-44.813c0-4.592-3.127-8.594-7.583-9.703l-46.479-11.58L413.736,385.7l24.674-41.047
                        c2.366-3.936,1.748-8.977-1.499-12.224l-38.754-38.754c-3.248-3.247-8.289-3.864-12.224-1.499l-41.048,24.674l-40.203-16.661
                        l-11.58-46.479c-1.109-4.456-5.111-7.583-9.703-7.583H228.6c-4.592,0-8.594,3.127-9.703,7.583l-11.58,46.479l-40.203,16.661
                        l-41.048-24.674c-3.936-2.367-8.976-1.748-12.224,1.499L75.089,332.43c-3.247,3.247-3.865,8.288-1.499,12.224L98.264,385.7
                        l-16.661,40.204l-46.479,11.58c-4.456,1.109-7.583,5.111-7.583,9.703V492H10c-5.522,0-10,4.478-10,10c0,5.522,4.478,10,10,10h492
                        c5.522,0,10-4.478,10-10C512,496.478,507.522,492,502,492z M464.458,492h-102.2c5.554-33.969-5.425-68.867-30.116-93.559
                        c-10.827-10.827-23.519-19.062-37.72-24.477c-5.16-1.968-10.938,0.62-12.906,5.781c-1.968,5.16,0.621,10.938,5.781,12.906
                        c11.551,4.404,21.882,11.11,30.703,19.932c20.887,20.888,29.724,50.749,23.931,79.416h-171.87
                        c-5.792-28.668,3.045-58.529,23.933-79.416c8.908-8.908,19.35-15.656,31.035-20.058c5.169-1.946,7.78-7.715,5.834-12.883
                        c-1.947-5.168-7.715-7.778-12.883-5.834c-14.366,5.411-27.195,13.698-38.129,24.632c-24.69,24.691-35.67,59.588-30.116,93.559
                        H47.54v-36.997l43.788-10.909c3.077-0.767,5.607-2.946,6.821-5.875l20.521-49.52c1.214-2.93,0.967-6.262-0.668-8.98
                        l-23.246-38.671l27.703-27.703l38.672,23.246c2.718,1.635,6.053,1.883,8.98,0.668l49.519-20.521
                        c2.929-1.214,5.109-3.744,5.875-6.821l10.909-43.788h39.17l10.909,43.788c0.766,3.077,2.946,5.607,5.875,6.821l49.519,20.521
                        c2.929,1.215,6.263,0.968,8.98-0.668l38.672-23.246l27.703,27.703l-23.246,38.671c-1.635,2.719-1.882,6.051-0.668,8.98
                        l20.521,49.52c1.214,2.929,3.744,5.108,6.821,5.875l43.788,10.909V492z" />
                </g>
            </svg>

            <svg id="applicationDevelopment">
                <g>
                    <path d="m30 11h-6v2h4v2h-4v2h4v2h-4v2h6z" />
                    <path d="m40 16c0-2.757-2.243-5-5-5h-3v10h3c2.757 0 5-2.243 5-5zm-6-3h1c1.654 0 3 1.346 3 3s-1.346 3-3 3h-1z" />
                    <path d="m55 44.142v-24.284c1.72-.447 3-1.999 3-3.858 0-2.206-1.794-4-4-4-1.2 0-2.266.542-3 1.382l-15.091-7.546c.058-.27.091-.549.091-.836 0-2.206-1.794-4-4-4s-4 1.794-4 4c0 .287.033.566.091.836l-15.091 7.546c-.734-.84-1.8-1.382-3-1.382-2.206 0-4 1.794-4 4 0 1.859 1.28 3.411 3 3.858v24.284c-1.72.447-3 1.999-3 3.858 0 2.206 1.794 4 4 4 1.2 0 2.266-.542 3-1.382l15.091 7.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4c0-.287-.033-.566-.091-.836l15.091-7.546c.734.84 1.8 1.382 3 1.382 2.206 0 4-1.794 4-4 0-1.859-1.28-3.411-3-3.858zm-1-30.142c1.103 0 2 .897 2 2s-.897 2-2 2-2-.897-2-2 .897-2 2-2zm-22-11c1.103 0 2 .897 2 2s-.897 2-2 2-2-.897-2-2 .897-2 2-2zm-3 4.618c.734.84 1.8 1.382 3 1.382s2.266-.542 3-1.382l15.091 7.546c-.058.27-.091.549-.091.836s.033.566.091.836l-15.091 7.546c-.734-.84-1.8-1.382-3-1.382s-2.266.542-3 1.382l-15.091-7.546c.058-.27.091-.549.091-.836s-.033-.566-.091-.836zm6.714 20.845c.964 1.193 1.874 2.694 2.492 4.537h-5.206v-2.142c1.241-.323 2.248-1.217 2.714-2.395zm3.286 9.537c0 1.076-.105 2.07-.277 3h-5.723v-6h5.727c.174.929.273 1.927.273 3zm-14 0c0-1.073.099-2.071.273-3h5.727v6h-5.717c-.175-.931-.283-1.925-.283-3zm6 5v7.155c-1.552-1.169-3.95-3.474-5.193-7.155zm2 7.173v-7.173h5.205c-1.237 3.716-3.644 6.013-5.205 7.173zm-1-21.173c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm-1 1.858v2.142h-5.206c.618-1.842 1.528-3.344 2.492-4.537.466 1.178 1.473 2.072 2.714 2.395zm-3.21-4.845c-1.569 1.61-3.199 3.903-4.095 6.987h-3.695c1.355-3.254 4.009-5.88 7.433-7.166zm-8.79 11.987c0-1.027.122-2.031.351-3h3.893c-.153.939-.244 1.935-.244 3 0 1.07.103 2.058.257 3h-3.893c-.23-.965-.364-1.966-.364-3zm4.694 5c.986 3.417 2.858 5.842 4.537 7.438-3.722-1.13-6.742-3.885-8.228-7.438zm16.612 0h3.692c-1.487 3.553-4.506 6.308-8.228 7.438 1.677-1.596 3.55-4.021 4.536-7.438zm4.694-5c0 1.034-.134 2.035-.363 3h-3.893c.153-.942.256-1.93.256-3 0-1.065-.091-2.061-.245-3h3.893c.23.969.352 1.973.352 3zm-4.695-5c-.896-3.084-2.525-5.377-4.095-6.987l.357-.178c3.424 1.285 6.078 3.912 7.433 7.166h-3.695zm-32.305-17c0-1.103.897-2 2-2s2 .897 2 2-.897 2-2 2-2-.897-2-2zm2 34c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm22 11c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2zm3-4.618c-.734-.84-1.8-1.382-3-1.382s-2.266.542-3 1.382l-15.091-7.546c.058-.27.091-.549.091-.836 0-1.859-1.28-3.411-3-3.858v-24.284c.79-.205 1.478-.643 2-1.24l12.114 6.057c-4.928 2.544-8.114 7.652-8.114 13.325 0 8.271 6.729 15 15 15s15-6.729 15-15c0-5.673-3.186-10.781-8.113-13.325l12.113-6.057c.521.597 1.21 1.035 2 1.24v24.284c-1.72.447-3 1.999-3 3.858 0 .287.033.566.091.836zm19-6.382c-1.103 0-2-.897-2-2s.897-2 2-2 2 .897 2 2-.897 2-2 2z" />
                </g>
            </svg>

            <svg id="realityDevelopment">
                <path d="m262.261719 126.28125c-.066407-.257812-.128907-.515625-.128907-.644531 0-2.058594 3.027344-3.476563 5.277344-3.476563 1.417969 0 2.511719.449219 2.832032 1.675782l9.851562 34.890624 9.785156-34.890624c.320313-1.226563 1.417969-1.675782 2.832032-1.675782 2.253906 0 5.28125 1.480469 5.28125 3.476563 0 .195312-.066407.386719-.132813.644531l-12.359375 40.492188c-.578125 1.933593-2.960938 2.832031-5.40625 2.832031s-4.765625-.898438-5.40625-2.832031zm0 0" />
                <path d="m302.433594 124.738281c0-1.351562 1.09375-2.574219 2.703125-2.574219h12.746093c8.433594 0 15.257813 3.21875 15.257813 13.453126 0 7.402343-3.605469 11.394531-8.433594 12.875l8.433594 15.644531c.257813.320312.320313.773437.320313 1.03125 0 2.125-2.765626 4.375-5.214844 4.375-1.15625 0-2.25-.707031-2.894532-1.996094l-9.140624-17.769531h-5.410157v16.804687c0 1.738281-2.058593 2.640625-4.183593 2.640625-2.0625 0-4.1875-.902344-4.1875-2.640625v-41.84375zm8.367187 4.761719v13.710938h7.082031c4.183594 0 6.886719-1.738282 6.886719-6.824219 0-5.085938-2.703125-6.886719-6.886719-6.886719zm0 0" />
                <path d="m481.066406 484.472656-4.277344-25.03125c-.695312-4.082031-4.574218-6.828125-8.65625-6.128906-4.082031.699219-6.824218 4.574219-6.128906 8.65625l4.277344 25.027344c.429688 2.53125-.242188 5.003906-1.898438 6.964844-1.652343 1.960937-3.976562 3.039062-6.542968 3.039062h-52.570313l-9.390625-57.699219c-.664062-4.085937-4.507812-6.859375-8.605468-6.195312-4.089844.664062-6.863282 4.515625-6.199219 8.605469l9 55.289062h-298.742188l8.996094-55.289062c.667969-4.089844-2.109375-7.941407-6.195313-8.605469-4.082031-.664063-7.941406 2.105469-8.609374 6.195312l-9.386719 57.699219h-52.574219c-2.566406 0-4.890625-1.078125-6.542969-3.039062-1.652343-1.960938-2.328125-4.433594-1.894531-6.964844l10.246094-59.988282c3.503906-20.519531 17.121094-37.886718 35.71875-46.421874.980468-.207032 1.890625-.605469 2.6875-1.15625 2.046875-.824219 4.152344-1.546876 6.300781-2.152344l90.007813-19.125c23.925781 16.054687 51.746093 24.527344 80.617187 24.527344s56.6875-8.472657 80.617187-24.527344l90.003907 19.125c23.125 6.535156 40.660156 26.03125 44.707031 49.730468.699219 4.082032 4.574219 6.824219 8.65625 6.128907 4.082031-.695313 6.828125-4.574219 6.128906-8.65625-5.03125-29.464844-26.890625-53.691407-55.683594-61.714844-.148437-.042969-.300781-.078125-.457031-.109375l-93.386719-19.847656-9.042968-1.921875c-5.820313-1.167969-12.328125-9.140625-12.328125-18.054688v-25.980469c13.839843-12.414062 28.265625-26.988281 34.355469-35.9375 10.335937-15.171874 16.34375-32.917968 17.375-51.3125l.265624-4.761718h3.5c6.140626 0 11.382813-3.925782 13.347657-9.402344h7.703125c8.515625 0 15.445312-6.925781 15.445312-15.441406v-64.976563c0-8.515625-6.929687-15.445312-15.445312-15.445312h-7.703125c-1.851563-5.160157-6.613281-8.941407-12.296875-9.363281-2.746094-20.097657-11.445313-38.914063-24.878906-53.589844-33.042969-36.082032-81.609376-36.621094-84.339844-36.621094-.019532 0-.039063 0-.054688 0h-12.960937c-2.109375.0195312-51.117188.265625-84.40625 36.613281-13.4375 14.671875-22.132813 33.488281-24.882813 53.59375-5.699218.40625-10.480468 4.195313-12.335937 9.367188h-7.703125c-8.515625 0-15.445313 6.929687-15.445313 15.445312v24.988281h-11.683593c-14.199219 0-25.746094 11.546876-25.746094 25.746094v202.5625c-21.597656 10.996094-37.277344 31.789063-41.441406 56.164063l-10.246094 59.992187c-1.171875 6.859375.730468 13.84375 5.214844 19.160156 4.488281 5.316407 11.050781 8.367188 18.007812 8.367188h434.277344c6.957031 0 13.523437-3.050781 18.007812-8.367188 4.488282-5.316406 6.390625-12.300781 5.21875-19.160156zm-111.476562-369.898437h6.871094c.246093 0 .445312.199219.445312.445312v64.972657c0 .246093-.199219.445312-.445312.445312h-6.871094zm-68.160156 238.027343c-18.644532 9.902344-39.363282 15.078126-60.726563 15.078126-21.371094 0-42.097656-5.179688-60.746094-15.085938 10.382813-6.050781 16.53125-18.875 16.53125-29.761719v-13.222656c6.148438 4.902344 11.277344 8.484375 14.222657 9.742187 9.53125 4.0625 19.761718 6.097657 29.992187 6.097657s20.460937-2.03125 29.988281-6.097657c2.949219-1.257812 8.078125-4.84375 14.222656-9.742187v13.222656c0 10.894531 6.164063 23.734375 16.515626 29.769531zm-140.542969-305.855468c28.71875-31.359375 72.871093-31.746094 73.324219-31.746094h13.003906.003906c.652344 0 44.511719.308594 73.300781 31.75 10.917969 11.925781 18.144531 27.113281 20.773438 43.421875h-201.183594c2.632813-16.316406 9.859375-31.503906 20.777344-43.425781zm-34.109375 58.425781h4.917968.125 222.769532v84.667969h-83.445313c-4.003906 0-7.929687-1.09375-11.355469-3.164063-11.785156-7.117187-26.425781-7.117187-38.210937 0-3.425781 2.070313-7.351563 3.164063-11.355469 3.164063h-13.542968c-4.140626 0-7.5 3.359375-7.5 7.5s3.359374 7.5 7.5 7.5h13.542968c6.734375 0 13.34375-1.839844 19.109375-5.324219 7.003907-4.226563 15.703125-4.226563 22.703125 0 5.765625 3.484375 12.371094 5.324219 19.105469 5.324219h65.75l-.222656 3.917968c-.878907 15.675782-5.992188 30.789063-14.792969 43.710938-10.828125 15.902344-50.191406 50.152344-57.066406 53.082031-15.324219 6.535157-32.894532 6.535157-48.214844 0-2.257812-.960937-10.507812-6.976562-22.542969-17.605469-.0625-.058593-.128906-.117187-.195312-.171874-1.480469-1.308594-3.015625-2.6875-4.605469-4.132813-13.777344-12.53125-25.164062-24.476563-29.722656-31.171875-8.800782-12.921875-13.917969-28.039062-14.796875-43.71875l-.21875-3.910156h17.238281c4.144531 0 7.5-3.359375 7.5-7.5 0-4.144532-3.355469-7.5-7.5-7.5h-34.972656zm-22.316406 9.847656c0-.246093.199218-.445312.445312-.445312h6.871094v65.863281h-6.871094c-.246094 0-.445312-.199219-.445312-.445312zm-37.433594 50.734375c0-5.925781 4.820312-10.746094 10.746094-10.746094h11.6875v24.984376c0 8.515624 6.929687 15.445312 15.445312 15.445312h7.699219c1.964843 5.476562 7.207031 9.402344 13.351562 9.402344h3.53125l.269531 4.753906c1.03125 18.398438 7.039063 36.148438 17.375 51.320312 6.089844 8.949219 20.515626 23.523438 34.355469 35.9375v25.980469c0 8.914063-6.507812 16.886719-12.410156 18.074219l-8.960937 1.902344-93.089844 19.78125zm0 0" />
            </svg>

            <svg id="customSolution">
                <g>
                    <path d="m63 22v-18c0-1.654-1.346-3-3-3h-22c-1.654 0-3 1.346-3 3h-7.774l-.928 3.589c-3.457.714-6.697 2.056-9.648 3.996l-3.193-1.882-6.753 6.752 1.883 3.193c-1.939 2.95-3.282 6.19-3.996 9.649l-3.591.929v9.549l3.59.928c.714 3.459 2.057 6.698 3.996 9.648l-1.883 3.193 1.456 1.456h-2.159c-2.757 0-5 2.243-5 5v3h62v-3c0-2.757-2.243-5-5-5h-2.159l1.456-1.456-1.883-3.193c1.939-2.95 3.282-6.189 3.996-9.648l3.59-.929v-9.549l-3.59-.928c-.303-1.463-.724-2.901-1.257-4.298h1.847c1.654.001 3-1.345 3-2.999zm-17-12c0-1.654 1.346-3 3-3s3 1.346 3 3-1.346 3-3 3-3-1.346-3-3zm1.586 13-4 4h-.586v-4h-2c0-4.411 3.589-8 8-8s8 3.589 8 8zm-6.586 2v4h3.414l4-4h.897c1.754 3.03 2.689 6.472 2.689 10 0 .717-.049 1.433-.125 2.145-.279-.085-.569-.145-.875-.145h-16c-1.654 0-3 1.346-3 3v.374c-.004-.001-.007-.002-.011-.003l-3.538-.71c-.502-.144-.914-.48-1.171-.918.548-.481.999-1.067 1.296-1.743h.424c1.654 0 3-1.346 3-3 0-.648-.211-1.244-.562-1.735.861-.775 1.404-1.764 1.527-2.785.023-.19.035-.382.035-.575 0-1.506-.719-2.944-1.924-3.847l-1.547-1.16c-.771-.579-1.727-.898-2.691-.898h-.395c-.879 0-1.722.252-2.443.723-.722-.471-1.564-.723-2.443-.723h-.395c-.964 0-1.92.319-2.691.897l-1.547 1.16c-1.205.904-1.924 2.342-1.924 3.848 0 .193.012.385.034.571.123 1.025.667 2.015 1.527 2.789-.35.491-.561 1.087-.561 1.735 0 1.654 1.346 3 3 3h.424c.297.677.749 1.263 1.297 1.744-.252.436-.649.768-1.094.897l-3.617.729c-1.101.223-2.05.81-2.753 1.611-.831-2.222-1.257-4.561-1.257-6.981 0-11.028 8.972-20 20-20 1.007 0 2.009.097 3 .246v6.754c0 1.654 1.346 3 3 3zm-27 20.272c0-1.422 1.012-2.659 2.405-2.94l1.853-.376.994 6.854 4.748-3.56 4.748 3.561.994-6.852 1.85.373c.14.029.275.071.408.118v7.55c0 1.302.839 2.402 2 2.816v2.184h-20zm5-10.272c-.552 0-1-.448-1-1s.448-1 1-1zm.78-4h-.78c-.257 0-.502.042-.74.103l-.354-.317c-.506-.452-.82-1.002-.887-1.553-.013-.108-.019-.218-.019-.328 0-.879.42-1.72 1.123-2.248l1.547-1.16c.428-.32.958-.497 1.492-.497h.395c.655 0 1.274.253 1.742.713l.701.689.701-.688c.468-.461 1.087-.714 1.742-.714h.395c.534 0 1.064.177 1.492.497l1.547 1.16c.703.528 1.123 1.368 1.123 2.248 0 .11-.006.22-.021.333-.065.547-.38 1.097-.887 1.549l-.354.316c-.237-.061-.482-.103-.738-.103h-1-1.764c-1.309 0-2.619-.309-3.789-.895l-.211-.105h-.236c-.883 0-1.67.391-2.22 1zm9.22 2c.552 0 1 .448 1 1s-.448 1-1 1zm-1.622 8.379c.13.059.267.108.405.154l-.531 3.656-1.826-1.37zm-6.378-6.379v-2c0-.486.349-.893.81-.982 1.379.643 2.904.982 4.426.982h.764v2c0 1.654-1.346 3-3 3s-3-1.346-3-3zm3 5c.541 0 1.053-.108 1.541-.268.082.14.17.276.268.405l-1.809 2.261-1.807-2.259c.098-.13.186-.266.267-.407.488.16.999.268 1.54.268zm-3.37 1.388 1.945 2.431-1.826 1.37-.53-3.649c.139-.045.278-.092.411-.152zm15.37 11.612h3.367l-.25 2h-3.117zm9.883-6h-5.766l-.5 4h-4.617c-.552 0-1-.448-1-1v-10c0-.552.448-1 1-1h16c.552 0 1 .448 1 1v10c0 .552-.448 1-1 1h-4.617zm-1.016 8h-3.735l.75-6h2.234zm-35.654-1.774 1.74-2.952-.362-.526c-2.094-3.033-3.492-6.409-4.157-10.036l-.115-.628-3.319-.858v-6.451l3.318-.857.115-.628c.665-3.627 2.063-7.003 4.157-10.036l.362-.526-1.74-2.953 4.562-4.561 2.952 1.739.526-.362c3.035-2.094 6.411-3.493 10.035-4.156l.629-.115.858-3.32h6.226v7.228c-.993-.135-1.995-.228-3-.228-12.131 0-22 9.869-22 22 0 3.276.708 6.419 2.09 9.354-.057.299-.09.606-.09.918v9.728h-1.013zm51.787 6.774v1h-58v-1c0-1.654 1.346-3 3-3h52c1.654 0 3 1.346 3 3zm-14.117-5-.25-2h4.367c1.172 0 2.179-.682 2.673-1.665l1.115 1.891-1.775 1.774zm10.683-24.711.114.628 3.32.857v6.451l-3.318.857-.115.628c-.6 3.271-1.802 6.335-3.566 9.131v-8.841c0-.477-.122-.921-.32-1.322.202-1.211.319-2.445.319-3.678 0-3.496-.832-6.917-2.407-10h4.395c.71 1.703 1.247 3.476 1.578 5.289zm-5.135-16.67c.961-.912 1.569-2.192 1.569-3.619 0-2.757-2.243-5-5-5s-5 2.243-5 5c0 1.427.608 2.707 1.57 3.619-3.827 1.404-6.57 5.074-6.57 9.381h-1c-.552 0-1-.449-1-1v-18c0-.551.448-1 1-1h22c.552 0 1 .449 1 1v18c0 .551-.448 1-1 1h-1c0-4.308-2.743-7.977-6.569-9.381z" />
                    <path d="m23 51h2v2h-2z" />
                    <path d="m23 47h2v2h-2z" />
                </g>
            </svg>
        </defs>
    </svg>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="sr-only" style="enable-background:new 0 0 512 512;">
        <defs>
            <svg id="multipz-Managed" viewBox="0 0 480 480">
                <g>
                    <g>

                        <g>

                            <path d="M429.688,90.304l-40-40c-2.678-2.681-6.869-3.115-10.04-1.04L335.2,78.4l-47.928-20L279.92,6.864    C279.355,2.924,275.98-0.001,272,0h-64c-3.98-0.001-7.355,2.924-7.92,6.864L192.72,58.4l-47.88,20l-44.456-29.12    c-3.171-2.075-7.362-1.641-10.04,1.04l-40,40c-2.684,2.685-3.112,6.886-1.024,10.056l29.6,44.96L73.96,156.8l14.688,6.352    l6.68-15.424c1.073-2.478,0.823-5.33-0.664-7.584l-28.416-43.12L97.04,66.232l42.576,27.888c2.229,1.443,5.025,1.701,7.48,0.688    l56-23.448c2.586-1.096,4.408-3.467,4.8-6.248L214.936,16h50.128l7.016,49.136c0.392,2.781,2.214,5.152,4.8,6.248l56,23.432    c2.452,1.013,5.246,0.755,7.472-0.688l42.592-27.872l31.008,31.008l-20.8,35.2c-1.238,2.087-1.462,4.624-0.608,6.896l8.872,23.432    l14.968-5.664l-7.512-19.832l22.04-37.264C432.772,96.89,432.268,92.887,429.688,90.304z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M325.656,306.344l-24-24c-1.5-1.5-3.534-2.344-5.656-2.344H184c-2.122,0-4.156,0.844-5.656,2.344l-24,24    c-1.5,1.5-2.344,3.534-2.344,5.656v160c0,4.418,3.582,8,8,8h160c4.418,0,8-3.582,8-8V312    C328,309.878,327.156,307.844,325.656,306.344z M267.312,296h25.376L264,324.688L251.312,312L267.312,296z M252.688,336    l-10.344,10.344c-1.505,1.497-2.349,3.533-2.344,5.656c-0.015-2.102-0.857-4.114-2.344-5.6L227.312,336L240,323.312L252.688,336z     M244.688,296L240,300.688L235.312,296H244.688z M212.688,296l16,16L216,324.688L187.312,296H212.688z M209,464h-41V315.312l8-8    l34.344,34.344l13.2,13.2L209,464z M225.144,464l14.784-110.944c0.048-0.337,0.072-0.676,0.072-1.016    c0.002,0.513,0.055,1.025,0.16,1.528L262.24,464H225.144z M312,464h-33.44l-21.872-109.368l12.968-12.976L304,307.312l8,8V464z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M296,104h-72c-26.499,0.026-47.974,21.501-48,48v48c0.04,35.33,28.67,63.96,64,64c35.33-0.04,63.96-28.67,64-64v-88    C304,107.582,300.418,104,296,104z M288,200c0,26.51-21.49,48-48,48c-26.51,0-48-21.49-48-48v-32h64    c11.824-0.003,23.227-4.393,32-12.32V200z M256,152h-64c0-17.673,14.327-32,32-32h64C288,137.673,273.673,152,256,152z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M477.656,338.344l-24-24c-1.5-1.5-3.534-2.344-5.656-2.344h-64c-2.122,0-4.156,0.844-5.656,2.344l-24,24    c-1.5,1.5-2.344,3.534-2.344,5.656v128c0,4.418,3.582,8,8,8h112c4.418,0,8-3.582,8-8V344    C480,341.878,479.156,339.844,477.656,338.344z M464,464h-40V344h-16v120h-40V347.312l16-16l18.344,18.344L408,344l5.656-5.656    L403.312,328h26.008l-10.808,10.168L424,344l5.488,5.832l19.072-17.96l15.44,15.44V464z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M424,176h-15.736c-22.02,0.044-39.881,17.844-40,39.864l-0.128,40.056c-0.088,26.465,21.294,47.991,47.76,48.08    c0.053,0,0.107,0,0.16,0c26.457-0.031,47.9-21.463,47.944-47.92V216C463.974,193.92,446.08,176.026,424,176z M384.264,215.92    C384.307,202.799,394.88,192.144,408,192c0.001,13.152-10.584,23.855-23.736,24V215.92z M448,256.056    c-0.031,17.62-14.3,31.9-31.92,31.944c-17.629,0-31.92-14.291-31.92-31.92c0-0.027,0-0.053,0-0.08l0.08-24    c12.506-0.075,24.257-5.995,31.76-16c7.554,10.072,19.41,16,32,16V256.056z M424,192c13.255,0,24,10.745,24,24    C434.745,216,424,205.255,424,192z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M125.656,338.344l-24-24c-1.5-1.5-3.534-2.344-5.656-2.344H32c-2.122,0-4.156,0.844-5.656,2.344l-24,24    C0.844,339.844,0,341.878,0,344v128c0,4.418,3.582,8,8,8h112c4.418,0,8-3.582,8-8V344    C128,341.878,127.156,339.844,125.656,338.344z M112,464H72V344H56v120H16V347.312l16-16l18.344,18.344L56,344l5.656-5.656    L51.312,328H77.32l-10.808,10.168L72,344l5.488,5.832l19.072-17.96l15.44,15.44V464z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                    <g>

                        <g>

                            <path d="M72,176H56.264c-22.02,0.044-39.881,17.844-40,39.864l-0.128,40.056c-0.088,26.465,21.294,47.991,47.76,48.08    c0.053,0,0.107,0,0.16,0c26.457-0.031,47.9-21.463,47.944-47.92V216C111.974,193.92,94.08,176.026,72,176z M72,192    c10.134,0.042,19.15,6.445,22.528,16H80c-10.134-0.042-19.15-6.445-22.528-16H72z M40,198.464v1.464    c0.01,2.129-0.841,4.172-2.36,5.664c-1.04,1.012-2.347,1.706-3.768,2C35.181,204.116,37.279,200.992,40,198.464z M96,256.08    c-0.044,17.611-14.309,31.876-31.92,31.92c-17.629,0-31.92-14.291-31.92-31.92c0-0.027,0-0.053,0-0.08l0.104-32.064    c6.263-0.035,12.26-2.539,16.688-6.968c1.265-1.281,2.383-2.697,3.336-4.224C59.711,219.955,69.651,223.992,80,224h16V256.08z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#F60334"></path>

                        </g>

                    </g>
                </g>

            </svg>

            <svg id="Client-Managed" viewBox="0 0 496 496">

                <g>

                    <path d="m496 208h-17.976562l-26.792969 14.097656c-3.015625-1.664062-6.152344-3.074218-9.535157-3.921875l-33.703124-8.433593v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-31.078125-24.351562-57.085937-54.28125-57.992187-1.519531-.039063-3.015625.015624-4.519531.089843l8.738281-20.457031-42.363281-42.351562-24.972657 16.253906c-9.488281-5.34375-19.523437-9.503906-29.945312-12.421875l-6.167969-29.136719h-60.96875l-6.167969 29.144531c-10.421874 2.917969-20.464843 7.078125-29.945312 12.421875l-24.972656-16.253906-42.328125 42.328125 8.589843 20.472656c-.328124-.015625-.640624-.074219-.96875-.082031-15.320312-.445312-29.75 5.144531-40.710937 15.785156-10.976563 10.632813-17.015625 24.902344-17.015625 40.183594v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-21.984375 5.496093-39.847656-23.238281h-26.167969v165.769531l78.703125 26.230469h15.328125l1.914062-6.703125 24.125 14.480469c.394532 13.117187 10.914063 23.671875 24.019532 24.132812.460937 13.050782 10.949218 23.539063 24 24 .460937 13.050782 10.949218 23.539063 24 24 .476562 13.355469 11.414062 24.089844 24.878906 24.089844 5 0 9.902344-1.558594 14.023438-4.335938l7.085937 6.257813c4.441406 3.910156 10.160156 6.078125 16.097656 6.078125h1.480469c13.3125 0 24.144531-10.761719 24.3125-24.03125 13.167969-.167969 23.832031-10.832031 24-24 13.175781-.167969 23.847656-10.847656 24-24.023438 13.296875-.3125 24.03125-11.175781 24.03125-24.546874 0-.804688-.039062-1.597657-.121094-2.398438l144.121094-72.054688zm-80-61.984375v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 39.96875 9.984375-87.976562 46.304687v-21.550781c0-10.753906-4.34375-20.601563-11.457031-27.878906l27.457031-6.859375v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.222656-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187zm-155.9375 126.230469-36.214844-11.574219c-16.503906-5.289063-34.367187-6.078125-51.261718-2.335937l-4.585938 1.015624v-2.367187c0-11.03125 7.480469-20.609375 18.175781-23.289063l45.824219-11.449218v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.289062-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 45.816407 11.449218c10.695312 2.679688 18.175781 12.257813 18.175781 23.289063v29.976563l-1.976562 1.039062h-4.902344l-49.183594-14.054688c-3.875-1.097656-7.867188-1.546874-11.875-1.699218zm-90.269531-152.511719c15.023437-29.109375 45.261719-47.734375 78.207031-47.734375 32.632812 0 62.742188 18.375 77.878906 47.089844-3.808594 7.636718-5.878906 16.09375-5.878906 24.910156v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-32 8.011718-32.007812-8.011718v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-31.078125-24.351562-57.085937-54.28125-57.992187-15.3125-.449219-29.75 5.144531-40.710938 15.785156-10.96875 10.640625-17.007812 24.910156-17.007812 40.191406v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-32 8.011718-32.007812-8.011718v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-9.414063-2.265625-18.351563-6.207031-26.28125zm-36.792969-48.382813 25.558594-25.558593 22.496094 14.640625 4.304687-2.640625c10.785156-6.609375 22.441406-11.449219 34.640625-14.367188l4.921875-1.179687 5.566406-26.246094h35.03125l5.550781 26.238281 4.921876 1.175781c12.199218 2.921876 23.855468 7.761719 34.640624 14.371094l4.304688 2.640625 22.496094-14.640625 25.527344 25.535156-9.074219 21.246094c-6.175781 2.65625-11.902344 6.394532-16.886719 11.226563-.328125.320312-.609375.664062-.929688.992187-18.847656-29.960937-52.167968-48.785156-88.070312-48.785156-36.160156 0-69.574219 19-88.375 49.265625-5.03125-5.265625-11.015625-9.53125-17.648438-12.511719zm-29 150.894532v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.246094-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 27.449219 6.859375c-7.113281 7.277343-11.457031 17.125-11.457031 27.878906v5.925781l-24.953125 5.546875.648437-2.265625.304688-14.785156-42.191406-24.613281zm-88-14.246094h5.832031l57.535157 33.558594-29.03125 116.121094-34.335938-11.449219zm65.296875 160-15.746094-5.246094 28.203125-112.800781 18.246094 10.640625v2.285156l-30.03125 105.121094zm54.703125 23.03125c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm24 24c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm24 24c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm32.96875 32.96875c-4.945312 0-8.96875-4.023438-8.96875-8.96875 0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625zm38.6875 8h-1.480469c-2.039062 0-3.992187-.742188-5.511719-2.089844l-5.984374-5.277344 6.007812-6.007812c2.144531-2.144531 3.808594-4.695312 5.039062-7.433594l7.273438 6.054688c1.902344 1.585937 3 3.921875 3 6.410156 0 4.601562-3.742188 8.34375-8.34375 8.34375zm71.742188-72h-1.382813c-2.007813 0-3.96875-.710938-5.503906-2l-43.382813-36.152344-10.25 12.296875 42.128906 35.101563c1.894532 1.585937 2.992188 3.921875 2.992188 6.410156 0 4.601562-3.742188 8.34375-8.34375 8.34375h-1.746094c-1.941406 0-3.839844-.6875-5.335937-1.9375l-43.453125-36.207031-10.25 12.296875 42.128906 35.105468c1.902344 1.574219 3 3.910157 3 6.398438 0 4.601562-3.742188 8.34375-8.34375 8.34375-3.070312 0-6.0625-1.089844-8.417969-3.046875l-17.855469-14.882813c-3.933593-8.039062-12.015624-13.644531-21.480468-13.980468-.460938-13.050782-10.949219-23.539063-24-24-.460938-13.050782-10.949219-23.539063-24-24-.46875-13.355469-11.40625-24.089844-24.871094-24.089844-6.574219 0-13.007812 2.664062-17.65625 7.3125l-14.0625 14.0625c-.839844.839844-1.535156 1.792969-2.242188 2.738281l-24.574218-14.746093 21.542968-75.398438 54.007813-12.007812c14.167969-3.167969 29.105469-2.480469 42.921875 1.941406l11.785156 3.769531-39.953125 19.976563c-9.128906 4.574218-14.800781 13.757812-14.800781 23.96875v1.582031c0 14.777343 12.023438 26.800781 26.800781 26.800781 4.855469 0 9.632813-1.320312 13.785157-3.824219l29.789062-17.863281c5.984375-3.585938 14.015625-2.800781 19.183594 1.847656l67.59375 60.832032c1.808594 1.640624 2.847656 3.96875 2.847656 6.40625 0 4.746093-3.855469 8.601562-8.601562 8.601562zm152.601562-92.945312-134.777344 67.394531c-.4375-.464844-.894531-.90625-1.375-1.335938l-67.582031-60.832031c-5.921875-5.34375-13.578125-8.28125-21.554687-8.28125-5.832032 0-11.558594 1.585938-16.574219 4.59375l-29.800781 17.871094c-1.671876 1.007812-3.582032 1.535156-5.535157 1.535156-5.960937 0-10.800781-4.839844-10.800781-10.800781v-1.582031c0-4.121094 2.289062-7.816407 5.96875-9.65625l44.625-22.3125c7.636719-3.824219 16.703125-4.679688 24.933594-2.3125l50.273437 14.359374 12.175781.304688 150.023438-78.960938zm0 0" data-original="#000000" class="hovered-path active-path" data-old_color="#000000" fill="#F60334"></path>

                </g>

            </svg>
        </defs>
    </svg>


    <script src="{{asset('public/js/jquery-3.5.1.min.js')}}" ></script>
    <script src="{{asset('public/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/js/tilt.jquery.min.js')}}" ></script>
    <script src="{{asset('public/js/ling-gallery-filter.js')}}" ></script>
    <script src="{{asset('public/js/typewriter.js')}}" ></script>

    <script src="{{asset('public/js/aos2_3_4.js')}}" ></script>
    <script src="{{asset('public/js/swiper_min.js')}}" ></script>
    <script src="{{asset('public/js/simpleParallax_min.js')}}" ></script>
    <script src="{{asset('public/js/skrollr.min.js')}}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.0/anime.min.js" integrity="sha512-LfB+BcvR3zBt7ebVskhSWiSbEUiG3p5EcCprkieldsKhBeR6wpnLi0VpWC2GNgVGWP2n/skO8Bx2oKNjUhXCkw==" crossorigin="anonymous" ></script>
    <script src="{{asset('public/js/main.js')}}" ></script>


    <script>
        function myFunction() {
            var d = new Date();
            var n = d.getFullYear();
            $('#year').val(n);

            var hourNow = d.getHours();
            var indian = new Date().toLocaleString("en-US", {
                timeZone: "Asia/Kolkata"
            });
            var q = new Date(indian);
            var indianHour = q.getHours();
            var indianTime;
            var greeting;

            if (hourNow >= 0 && hourNow < 6) {
                greeting = 'Very Early Morning';
            } else if (hourNow >= 6 && hourNow < 12) {
                greeting = 'Good Morning';
            } else if (hourNow >= 12 && hourNow < 18) {
                greeting = 'Good Afternoon!';
            } else {
                greeting = 'Good Evening!';
            }
            $('#greeting').val(greeting);

            if (indianHour >= 0 && indianHour < 6) {
                indianTime = 'Very Early Morning';
            } else if (indianHour >= 6 && indianHour < 12) {
                indianTime = 'Good Morning';
            } else if (indianHour >= 12 && indianHour < 18) {
                indianTime = 'Good Afternoon!';
            } else {
                indianTime = 'Good Evening!';
            }
            $('#indianTime').val(indianTime);

        }


        $('button[type=submit]').attr('disabled', 'disabled');
        $('button[type=submit]').removeAttr('disabled');
        $('#message').show();
        $('#success').hide();
        $('#fail').hide();

        var randomNum1;
        var randomNum2;

        //set the largeest number to display

        var maxNum = 15;
        var total;

        randomNum1 = Math.ceil(Math.random() * maxNum);
        randomNum2 = Math.ceil(Math.random() * maxNum);
        total = randomNum1 + randomNum2;

        $("#question").prepend(randomNum1 + " + " + randomNum2);

        // When users input the value

        $("#ans").keyup(function() {

            var input = $(this).val();
            var slideSpeed = 200;

            $('#message').hide();

            if (input == total) {
                $('button[type=submit]').removeAttr('disabled');
                $('#success').show();
                $('#fail').hide();

            } else {
                $('button[type=submit]').attr('disabled', 'disabled');
                $('#fail').show();
                $('#success').hide();

            }

        });

        // Wheen "reset button" click, generating new randomNum1 & randomNum2
        $("#reset").on("click", function() {
            $('#message').show();
            $('#success').hide();
            $('#fail').hide();
            randomNum1 = Math.ceil(Math.random() * maxNum);
            randomNum2 = Math.ceil(Math.random() * maxNum);
            total = randomNum1 + randomNum2;
            $("#question").empty();
            $("#ans").val('');
            $("#question").prepend(randomNum1 + " + " + randomNum2);
        });

        setTimeout(function() {
            $('#emailmsg').fadeOut('fast');
        }, 8000);

        (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();

    </script>







    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f96725a194f2c4cbeb8ed92/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l][];w[l].push({'gtm.start'
new Date().getTime(),event'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer''&l='+l'';j.async=true;j.src=
'httpswww.googletagmanager.comgtm.jsid='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFVJF5T');</script>
<!-- End Google Tag Manager -->





<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105605693-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-105605693-1');
</script>

    @yield('script')
</body>

</html>