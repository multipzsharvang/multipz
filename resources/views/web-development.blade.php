@extends('layout.master')

@section('title', 'Professional Web Development Services | Web Development Solutions')
@section('meta_description', 'Create your presence on the web with a leading web development company. We offer a one-stop solution for website design and development services. Get a quote now!')
@section('image')
<meta property="og:image" content="{{asset('public/img/icon-service.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/icon-service.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <h1 class="font-weight-bold display-3 mb-lg-4" data-aos="fade-up" data-aos-delay="200">Web Development Company</h1>
                    <div class="font-weight-bold h3  mb-lg-4" data-aos="fade-up" data-aos-delay="300">
                        <span class="letter-space-2 title-strike">Everything for a striking impact</span>
                    </div>
                    <p data-aos="fade-up" data-aos-delay="400">Multipz Technology is an emerging star in the web development field. We strive to fuel all kinds and scales of enterprises with our performance-driven, robust, and reliable web development services. Our team of dedicated developers uses different frameworks and technologies for your static or dynamic website requirements. With our unique approach to website development, we will let our clients have an advanced web identity. </p>
                        
                    <p data-aos="fade-up" data-aos-delay="500">Multipz Technology aims to grow bigger every day, and that is exactly what we want for our clients too. Irrespective of your business or industry website demand, you got our back. Hire us to empower your business. </p>
                </div>
            </div>
        </div>

        <section class="py-3 py-lg-5 bg-primary text-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                       <h2> <h5 class="display-3 font-weight-bold" style="position: sticky;top:75px;">Web <br class="d-none d-lg-block">Development Services</h5> </h2>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{asset('public/img/icon-service.png')}}" alt="" class="img-fluid mb-3">
                            <h3 class="font-weight-bold h4 mb-2">CMS Development</h3>
                            <p class="text-white">The Content Management System gives complete control and freedom to manage the content of the website with ease. With the dynamic nature of the growing market, it becomes necessary to mark your presence from time to time. Our team of deft CMS developers helps to combine the latest development technologies and their knowledge to offer you something simple but yet less ordinary.</p>
                            <a href="{{Route('contact')}}" class="btn btn-accent mt-3">Hire Developer <i class="fa fa-arrow-right ml-1"></i></a>
                        </div>
                        <div class="mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{asset('public/img/icon-service.png')}}" alt="" class="img-fluid mb-3">
                            <h3 class="font-weight-bold h4 mb-2">Frontend web development</h3>
                            <p class="text-white">Say hello to fast, highly interactive visual content, enhanced technology featured websites with our front-end web development. Multipz Technology helps to deliver only the best result. If you wish to create a fully functional and rich features website, we are your ideal front-end web development company in India.   </p>
                            <a href="{{Route('contact')}}" class="btn btn-accent mt-3">Hire Developer <i class="fa fa-arrow-right ml-1"></i></a>
                        </div>
                        <div class="mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{asset('public/img/icon-service.png')}}" alt="" class="img-fluid mb-3">
                            <h3 class="font-weight-bold h4 mb-2">Backend Development</h3>
                            <p class="text-white">Our team of experienced backend developers strives to give the best and only the best to elevate yours and your customers’ experience. With Multipz Technology, we assure you that your website will be proficient and interactive.</p>
                            <a href="{{Route('contact')}}" class="btn btn-accent mt-3">Hire Developer <i class="fa fa-arrow-right ml-1"></i></a>
                        </div>
                        <div class="mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{asset('public/img/icon-service.png')}}" alt="" class="img-fluid mb-3">
                            <h3 class="font-weight-bold h4 mb-2">Custom Web Development</h3>
                            <p class="text-white">If you wish to stand out from others and wish something that your potential clients find irresistible then, custom web development is for you. We love to paint your imagination into reality with codes and design to open new doors for opportunities and growth. With our custom web development service, build your successful website.</p>
                            <a href="{{Route('contact')}}" class="btn btn-accent mt-3">Hire Developer <i class="fa fa-arrow-right ml-1"></i></a>
                        </div>
                        <div class="mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{asset('public/img/icon-service.png')}}" alt="" class="img-fluid mb-3">
                            <h3 class="font-weight-bold h4 mb-2">E-Commerce Development</h3>
                            <p class="text-white">Say hello to the digital world with your eCommerce website. Our team of expert developers is well-versed with platforms like Zend, woo-commerce, open cart, etc. It’s the time to open digital gates for your business to reach potential customers beyond the physical boundaries with our striking web development services.</p>
                            <a href="{{Route('contact')}}" class="btn btn-accent mt-3">Hire Developer <i class="fa fa-arrow-right ml-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="pb-3 pb-lg-5">
            <div class="container">
                <img src="{{asset('public/img/web-development-01.png')}}" alt="Web Development Services" class="img-fluid mt-n5 mb-5">

                <div class="h1 font-weight-bold mb-3" data-aos="fade-up" data-aos-delay="300">Why Us</div>
                <p data-aos="fade-up" data-aos-delay="300">At the heart of Multipz Technology, we believe in continuous improvement. We don’t just make websites, but build brands. We build targeted solutions that work, create powerful user experiences to drive organic traffic, generate leads, and ultimately boost sales.</p>
                <p data-aos="fade-up" data-aos-delay="400">Our company offers a great service in the design, development, programming, and marketing of websites. We constantly investigate new technologies and recommend them to make sense and effect. </p>
                <p data-aos="fade-up" data-aos-delay="500">We seek to offer the best solution for your business and unbiased advice with a promise of an affordable price. We bring enthusiasm and commitment to every project we put our hands on. If you wish to have a partner who cares and is honest about your business, we are your ultimate choice.</p>
                <p class="font-italic" data-aos="fade-up" data-aos-delay="600">Wish to have a fantastic Website?</p>
                <a id="webDevelopment" class="btn btn-accent mt-3" href="{{Route('contact')}}" data-aos="fade-up" data-aos-delay="700">Talk to our expert TODAY!!!</a>

                <div class="py-xl-5">
                    <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">technology</h6>
                    <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Tools and Tech Stack</h4>
                    <div class="row mt-3 mt-xl-5 justify-content-center tools-tech">
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                            <div class="bg-php mb-2 tool-tilt mx-auto"></div>
                                <p>PHP</p>
                            </div>
                            <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                                <div class="bg-Yii mb-2 tool-tilt mx-auto"></div>
                                <p>Yii</p>
                            </div>
                            <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                                <div class="bg-Codeignitor mb-2 tool-tilt mx-auto"></div>
                                <p>Codeignitor</p>
                            </div>
                            <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                                <div class="bg-Laravel mb-2 tool-tilt mx-auto"></div>
                                <p>Laravel</p>
                            </div>
                            <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-lg-flex flex-column">
                                <div class="bg-Html mb-auto tool-tilt mx-auto"></div>
                                <p>HTML</p>
                            </div>
                            <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-lg-flex flex-column">
                                <div class="bg-CSS mb-auto tool-tilt mx-auto"></div>
                                <p>CSS</p>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    
        <div class="py-3 py-lg-5 bg-primary text-white mb-lg-5">
            <div class="container">
                <div class="h1 font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="300">FAQs</div>
                <div class="accordion" id="faq">
                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq1" class="text-white d-block h4 pr-4" data-toggle="collapse" aria-expanded="false" aria-controls="faq1">
                        What type of web development services do you provide?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq1" class="collapse show" data-parent="#faq">
                            <p class="text-white py-3">
                            We offer web design, development, testing, deployment, and maintenance. You can hire us for e-commerce, CMS, and Custom web development services.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq2" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq2">
                        How long does it take to develop a website?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq2" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            The time will vary from project to project. We don’t want to make fake promises without discussing project details.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq3" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq3">
                        What are the preferred technologies for web development?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq3" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            For the backend, we prefer Laravel, PHP, and CodeIgniter, while for the front-end, we use HTML, Bootstrap, CSS, and JS.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq4" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq4">
                        Can you help to update my existing website?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq4" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            Yes, we would love to update your existing website, even if it is developed by another firm.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div> 
        <div class="mb-5"></div>
    </main>
@endsection

@section('script')
<script type="application/ld+json">
            {
              "@context": "https://schema.org",
              "@type": "FAQPage",
              "mainEntity": [{
                "@type": "Question",
                "name": "What type of web development services do you provide?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "We offer web design, development, testing, deployment, and maintenance.You can hire us for e-commerce, CMS, and Custom web development services."
                }
              },{
                "@type": "Question",
                "name": "How long does it take to develop a website?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "The time will vary from project to project. We don’t want to make fake promises without discussing project details. "
                }
              },{
                "@type": "Question",
                "name": "What are the preferred technologies for web development?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "For the backend, we prefer Laravel, PHP, and CodeIgniter, while for the front-end, we use HTML, Bootstrap, CSS, and JS."
                }
              },{
                "@type": "Question",
                "name": "Can you help to update my existing website?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "Yes, we would love to update your existing website, even if it is developed by another firm. "
                }
              }]
            }
            </script>
@endsection



