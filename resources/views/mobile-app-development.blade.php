@extends('layout.master')

@section('title', 'Mobile Application Development Company | iOS & Android')
@section('meta_description', 'Being a prominent mobile application development company, we offer innovative custom mobile app development solutions for Android, iOS, and cross-platform.')
@section('image')
<meta property="og:image" content="{{asset('public/img/mobile-app-01.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/mobile-app-01.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <h1 class="font-weight-bold display-3 mb-lg-4" data-aos="fade-up" data-aos-delay="600">Mobile App Development</h1>
                    <p data-aos="fade-up" data-aos-delay="1200">We are your destination for creative, secure, and reliable mobile app development. You and your dream deserve a better place, and that is why we are here. Our mobile app development company is capable enough to convert your vision into a beautiful reality.</p>
                </div>
            </div>
        </div>

        <div class="position-relative app-dev-category">
            <div class="bg-primary text-white overflow-hidden">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="swiper-container mobile-dev-slider-slides mb-xs-0 py-3 py-lg-5 ">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <span class="align-items-center avatar-md d-inline-flex h3 justify-content-center rounded-circle slides-item active"> 1 </span>
                                    </div>
                                    <div class="swiper-slide">
                                        <span class="align-items-center avatar-md d-inline-flex h3 justify-content-center rounded-circle slides-item"> 2 </span>
                                    </div>
                                    <div class="swiper-slide">
                                        <span class="align-items-center avatar-md d-inline-flex h3 justify-content-center rounded-circle slides-item"> 3 </span>
                                    </div>
                                    <div class="swiper-slide">
                                        <span class="align-items-center avatar-md d-inline-flex h3 justify-content-center rounded-circle slides-item"> 4 </span>
                                    </div>
                                    <div class="swiper-slide">
                                        <span class="align-items-center avatar-md d-inline-flex h3 justify-content-center rounded-circle slides-item"> 5 </span>
                                    </div>
                                </div>
                            </div>            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container py-3 py-lg-5 mb-lg-5">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="swiper-container mobile-dev-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <h3 class="h2 mb-3">Native iOS App Development</h3>
                                    <p>With years of experience in delivering highly optimized iOS apps for different enterprises and firms, we are your ultimate destination. Multipz Technology is a one-stop solution for the entire iOS platform, including the latest versions of iOS. We have delivered hundreds of apps with stunning UI, consistent performance, and above all fast loading time.</p>
                                </div>
                                <div class="swiper-slide">
                                 <h3 class="h2 mb-3">Native Android App Development</h3>
                                    <p>Multipz Technology is a reliable and top choice for the android app development to prosper your business. Our developers have expertise in developing remarkable and compatible apps. We develop apps in the android for various industry verticals. We are an ideal choice because we have Expertise in Angular, Java, Kotlin, and other technologies to deliver an excellent android app.</p>
                                </div>
                                <div class="swiper-slide">
                                 <h3 class="h2 mb-3">Hybrid App Development</h3>
                                    <p>Hybrid apps are a hot choice for offering a plethora of benefits such as low development cost, easy access to data devices, etc. We combine HTML5, JavaScript, and CSS3 to develop hybrid apps with all the latest frameworks. At Multipz Technology, we offer apps that open gates of opportunities without compromising on quality and affordability.</p>
                                </div>
                                <div class="swiper-slide">
                                 <h3 class="h2 mb-3">Cross-platform App Development</h3>
                                    <p>Multipz Technology is a name you can trust for cross-platform app development. If you wish to cover a wider audience, you must choose cross-platform as it covers multiple OS. Our team of developers has proven experience in developing dynamic solutions with the help of technologies and frameworks like Appcelerator’s Titanium, React Native, PhoneGap, and Xamarin.</p>
                                </div>
                                <div class="swiper-slide">
                                 <h3 class="h2 mb-3">Progressive Web App Development</h3>
                                    <p>If Progressive web app development is your choice, then we are your ultimate choice. We have a team of experts who have experience in developing apps that offer seamless performance, high quality, and versatile apps. With minimum efforts, it can run faster on different platforms.</p>
                                </div>
                            </div>
                            <div class="swiper-pagination sr-only"></div>
                        </div>
                    </div>
                </div>
            </div>

            <img src="{{asset('public/img/mobile-app-01.png')}}" alt="Mobile App Development" class="d-none d-lg-block img-absolute-50 pl-lg-5 right mb-3 mb-lg-0 thumbnail">
        </div>
    
        <section class="py-3 py-lg-5 bg-light">
            <div class="container">
                <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">what we do</h6>
                <h4 class="h1 text-center font-weight-bold mt-n5 mb-5" data-aos="fade-up" data-aos-delay="100">Our process</h4>

                <div class="row mt-lg-5 justify-content-lg-center">
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Discussion</div>
                                <p>The first step is the discussion where we sit with the client and discuss their requirements, expectations, target audience, etc.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">UI/UX design</div>
                                <p>After that, we create a design with the latest tools and kits.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Prototype</div>
                                <p>Once the design is ready, we’ll prepare the prototype and send it to the development team.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Development stage</div>
                                <p>After that, developers will start the development process on the preferred platform with total transparency.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Quality check</div>
                                <p>Next come is the quality test where testers test it on various parameters for 100% bug-free and user-friendly apps.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Deployment time</div>
                                <p>Once the QA team gives the green signal, the app gets ready for the deployment in the app store.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4 d-md-flex">
                        <div class="card border">
                            <div class="card-body">
                                <div class="mb-3 h5 font-weight-bold">Support and Maintenance</div>
                                <p>Our supportive team will take care of after-sale service and maintenance.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 text-center">
                        <a id="mobileDevelopment" href="{{Route('contact')}}" class="btn btn-accent btn-lg">Contact us</a>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="py-xl-5">
                <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">technology</h6>
                <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Tools and Tech Stack</h4>
                <div class="row mt-3 mt-xl-5 justify-content-center tools-tech">
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                        <div class="bg-apple mb-2 tool-tilt mx-auto"></div>
                            <p>iOS</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                            <div class="bg-Android  mb-2 tool-tilt mx-auto"></div>
                            <p>Android</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                            <div class="bg-flutter mb-2 tool-tilt mx-auto"></div>
                            <p>Flutter</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                            <div class="bg-Ionic mb-2 tool-tilt mx-auto"></div>
                            <p>Ionic</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 ">
                            <div class="bg-React mb-2 tool-tilt mx-auto"></div>
                            <p>React Native</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    
        <section class="py-3 py-lg-5 bg-primary text-white mb-lg-5">
            <div class="container">
                <div class="h1 font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="300">FAQs</div>
                <div class="accordion" id="faq">
                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq1" class="text-white d-block h4 pr-4" data-toggle="collapse" aria-expanded="false" aria-controls="faq1">
                            How much does an app cost?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq1" class="collapse show" data-parent="#faq">
                            <p class="text-white py-3">
                            It is difficult to tell the exact cost of building an app. On discussing your ideas, platform, and other important things, our expert can let you know the cost of mobile app development.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq2" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq2">
                        What different programming languages are used in mobile app development?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq2" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            Use Swift and Objective C for iOS and Kotlin and Java for android app, Dart for Flutter, JavaScript for React Native, and C# for Xamarin.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq3" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq3">
                        Do you have any extra charges for the developed mobility solution?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq3" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            No, we believe in transparency, and that is why we discuss every charge right from our first meeting.
                            </p>
                        </div>
                    </div>

                    <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                        <a href="#faq4" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq4">
                            Is Cross-platform better than native applications?
                            <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                        </a>
                        <div id="faq4" class="collapse" data-parent="#faq">
                            <p class="text-white py-3">
                            It depends on your requirements. Based on your requirements, our expert will discuss all pros and cons of each platform for your app. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="mb-5"></div>
        
    </main>
@endsection

@section('script')
<script type="application/ld+json">
            {
              "@context": "https://schema.org",
              "@type": "FAQPage",
              "mainEntity": [{
                "@type": "Question",
                "name": "How much does an app cost?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "It is difficult to tell the exact cost of building an app. On discussing your ideas, platform, and other important things, our expert can let you know the cost of mobile app development."
                }
              },{
                "@type": "Question",
                "name": "What different programming languages are used in mobile app development?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "Use Swift and Objective C for iOS and Kotlin and Java for android app, Dart for Flutter, JavaScript for React Native, and C# for Xamarin."
                }
              },{
                "@type": "Question",
                "name": "Do you have any extra charges for the developed mobility solution?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "No, we believe in transparency, and that is why we discuss every charge right from our first meeting."
                }
              },{
                "@type": "Question",
                "name": "Is Cross-platform better than native applications?",
                "acceptedAnswer": {
                  "@type": "Answer",
                  "text": "It depends on your requirements. Based on your requirements, our expert will discuss all pros and cons of each platform for your app."
                }
              }]
            }
            </script>
@endsection

