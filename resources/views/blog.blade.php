@extends('layout.master')
@section('title', 'Latest News and Update For Mobile, Web and AI Solution')
@section('meta_description', 'Welcome to Multipz Technology Blog! This is the right place to read all the latest news and update for mobile development, Web development, and AI Solution.')
@section('image')
<meta property="og:image" content="{{asset('public/img/bg-blog-banner.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/bg-blog-banner.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
<div class="hero position-relative overflow-hidden">
   
            <div class="container-fluid">
                <div class="row align-items-center min-vh-100" style="background: url('public/img/bg-blog-banner.png') no-repeat center / cover;">
                    <div class="col-lg-6 col-xl-5 pl-xl-5 text-white">
                        <h4 class="h3 font-weight-bold mb-xl-3" data-aos="fade-up">Blog</h4>
                        <h1 class="font-weight-bold display-3 mb-xl-3" data-aos="fade-up" data-aos-delay="600">Fuel the Creativity Here!!!</h1>
                        <p class="col-lg-10 px-0 mb-xl-3 text-white" data-aos="fade-up" data-aos-delay="1200">Our opinions, thoughts, and ideas on everything that matters in technologies and beyond </p>
                    </div>
                </div>
            </div>
            <img src="{{asset('public/img/bg-element-03.png')}}" alt="" class="d-none d-lg-block img-fluid banner-ele-3 animate-rotate slow" data-aos="fade" data-aos-delay="1000" data-aos-offset="-100">
        </section>
</div>
         
        
        <section class="py-3 py-lg-5 position-relative overflow-hidden">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-4 mb-5">
                        <select class="custom-select">
                            <option selected>All Categories</option>
                            <option value="MobileAppDevelopment">Mobile App Development</option>
                            <option value="WebDevelopment">Web Development</option>
                            <option value="ArtificialIntelligence">Artificial Intelligence</option>
                            <option value="WebDesign">Web Design</option>
                            <option value="Events">Events</option>
                            <option value="News">News</option>
                        </select>
                    </div>
                </div>

                <div class="blog-list mt-4" style="clear:both;">
                    <div class="row mb-4 mb-md-5">
                        <div class="col-md-6 text-md-right">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/image_1.jpg')}}" alt="" class="img-fluid">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality')}}" class="text-white">Artificial Intelligence: Give a Beautiful Wing to Future Reality</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>Siri here is not a person, but the AI-enabled device that is making life easy for people like Jack and others. Once what was shown in sci-fi movies like driverless cars to voice automation devices, it is now a reality of the world. All this is possible due to technology called artificial intelligence or AI. Who had ever thought voice devices like Alexa, Siri will follow your countless demands from ordering food, showing temperature, to what not? Thanks to AI, for making it so easy and took the center stage like never before. The bright future of Artificial Intelligence development services is very enormously bright and better than predictions.</p>
                                <a href="{{ Route('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row flex-md-row-reverse mb-4 mb-md-5">
                        <div class="col-md-6">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/image_2.jpg')}}" alt="" class="img-fluid" loading="lazy">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App')}}" class="text-white">Who is the Ultimate Winner: Hybrid vs Native App</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>Many studies show how we are becoming dependent on mobile apps, from shopping, travel, movies, and health appointments, to online classes, in short, from anything to everything. So, you get it, how much it is important to have a business app. The moment you decide to invest in mobile app development, the biggest dilemma is the barrage of platforms. Most of the time, people get stuck between the choice of hybrid or native app. If you are the one who got tucked between these two, you must read this post to settle the dust of confusion. </p>
                                <a href="{{ Route('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>


                    <div class="row mb-4 mb-md-5">
                        <div class="col-md-6 text-md-right">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/image_3.jpg')}}" alt="" class="img-fluid" loading="lazy">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('Which-is-the-Best-Flutter-or-React-Native')}}" class="text-white">Which is the Best: Flutter or React Native</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>Flutter is a pretty young framework in the cross-platform community when compared to React Native. Created by Google; it is an open-source UI software development kit used for developing iOS, Windows, Linux, Web, and Android apps with a single code base. Despite being a young technology, it managed to gain 94.9k GitHub. You can hire a reliable and experienced mobile app development company to develop the best flutter app. Some fantastic apps developed using Flutter include <b>eBay, Hamilton Broadway Musical app, Alibaba, etc</b>. </p>
                                <a href="{{ Route('Which-is-the-Best-Flutter-or-React-Native')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row flex-md-row-reverse mb-4 mb-md-5">
                        <div class="col-md-6">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/image_4.jpg')}}" alt="" class="img-fluid" loading="lazy">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('What-to-choose-Open-Source-or-Custom-Development')}}" class="text-white">What to choose: Open Source or Custom Development</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>In this digital era, the website is the face of the business. To grow beyond the geographical limit, you have to have a website. But when it comes to website development, there is always confusion between two options: custom development and open source development. There are countless questions like Which is the best option? Is the option relevant for my business?. The answers to all these questions lie in the below reading.</p>
                                <a href="{{ Route('What-to-choose-Open-Source-or-Custom-Development')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4 mb-md-5">
                        <div class="col-md-6 text-md-right">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/design-trend.webp')}}" alt="" class="img-fluid" loading="lazy">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('Do-you-know-the-Top-Web-Design-Trends-of-the-Year')}}" class="text-white">Do you know the Top Web Design Trends of the Year</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>Think back to some fifteen years ago, where owners used to decorate their shops with various beautiful elements to attract customers. But now in the digital age where mortar and brick stores are replaced with websites. Web design helps to catch the attention of the users, if it's attractive and user-friendly.</p>
                                <a href="{{ Route('Do-you-know-the-Top-Web-Design-Trends-of-the-Year')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row flex-md-row-reverse mb-4 mb-md-5">
                        <div class="col-md-6">
                            <figure class="blog-image">
                                <img src="{{asset('public/img/blog/image_5.webp')}}" alt="" class="img-fluid" loading="lazy">
                            </figure>
                        </div>    
                        <div class="col-md-6 pt-md-5">
                            <div class="blog-title h5 bg-primary p-3 text-white mb-md-5">
                                <a href="{{ Route('Zip-Zap-Zoom-Its-Picnic-Time')}}" class="text-white">Zip...Zap. . .Zoom - It's Picnic Time</a>
                            </div>
                            <div class="bg-light p-3 p-md-4">
                                <p>A job fills your pocket, adventure fills your soul, and with Multipz Technology you can fill both pocket and soul. You do not believe me? Well... in that case, I would love to take you to the Parvati Hills, our picnic location of 2020. The new year of the new decade is doing no good to the world, and that is why this picnic acts as a positivity booster.</p>
                                <a href="{{ Route('Zip-Zap-Zoom-Its-Picnic-Time')}}" class="btn btn-accent">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <div class="mb-5"></div>         
    </main>
    
@endsection

