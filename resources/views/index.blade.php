@extends('layout.master')

@section('title', 'Multipz Technology: Web and Mobile App Development Company')
@section('meta_description', 'Multipz Technology is a one-stop-solution for Web development, Mobile App Development, and Artificial intelligence Development services. Get in touch to discuss your project!')
@section('image')
<meta property="og:image" content="{{asset('public/img/laptop.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/laptop.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <section class="hero position-relative overflow-hidden">
        <div class="container-fluid">
            <div class="swiper-container main-slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row flex-lg-row-reverse align-items-center min-vh-100">
                            <div class="col-xl-7">
                                <!-- <img src="img/laptop.png" alt="Web Development Company" class="img-fluid" data-aos="fade-up" data-aos-delay="800" > -->
                                <img src="{{ asset('public/img/laptop.webp')}}" alt="Web Development Company" class="img-fluid mx-auto d-block" >
                            </div>
                            <div class="col-xl-5 pl-xl-5">
                                <h4 class="h5 mb-xl-3" data-aos="fade-up">Let’s make it simple</h4>
                                <!-- <h1 class="typing-string-01 display-3 mb-xl-3 font-weight-bold"></h1> -->
                                <h1 class="font-weight-bold display-3 mb-xl-3" data-aos-delay="600">Web Development Company</h1>
                                <p class="h4 col-lg-10 px-0 mb-xl-3 typing-sub-strings" data-aos="fade-up" data-aos-delay="1200">Your smoother, faster and awesome website is just a click away </p>
                                <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4" data-aos="fade-up" data-aos-delay="1500">Hire us <i class="fas fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row flex-lg-row-reverse align-items-center min-vh-100">
                            <div class="col-xl-7">
                                <img src="{{ asset('public/img/banner-mobile.webp')}}" alt="Mobile App Development" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="col-xl-5 pl-xl-5">
                                    <!-- <h1 class="typing-string-02 display-3 mb-xl-3 font-weight-bold"></h1> -->
                                <h2 class="font-weight-bold display-3 mb-xl-3" data-aos-delay="600">Mobile App Development Company</h2>
                                <p class="h4 col-lg-10 px-0 mb-xl-3 typing-sub-strings" data-aos="fade-up" data-aos-delay="1200">We build effortless and impactful apps</p>
                                <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4" data-aos="fade-up" data-aos-delay="1500">Hire us <i class="fas fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row flex-lg-row-reverse align-items-center min-vh-100">
                            <div class="col-xl-7">
                                <img src="{{ asset('public/img/banner-ai.webp')}}" alt="Artificial Intelligence Development Company" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="col-xl-5 pl-xl-5">
                                    <!-- <h1 class="typing-string-03 display-3 mb-xl-3 font-weight-bold"></h1> -->
                                <h2 class="font-weight-bold display-3 mb-xl-3" data-aos-delay="600">Artificial Intelligence Development Company</h2>
                                <p class="h4 col-lg-10 px-0 mb-xl-3 typing-sub-strings" data-aos="fade-up" data-aos-delay="1200">Stay ahead in the game with custom-build AI solutions</p>
                                <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4" data-aos="fade-up" data-aos-delay="1500">Hire us <i class="fas fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="row flex-lg-row-reverse align-items-center min-vh-100">
                            <div class="col-xl-7">
                                <img src="{{ asset('public/img/banner-custom.webp')}}" alt="Custom Solutions Company" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="col-xl-5 pl-xl-5">
                                    <!-- <h1 class="typing-string-04 display-3 mb-xl-3 font-weight-bold"></h1> -->
                                <h2 class="font-weight-bold display-3 mb-xl-3" data-aos-delay="600">Custom Solutions Company</h2>
                                <p class="h4 col-lg-10 px-0 mb-xl-3 typing-sub-strings" data-aos="fade-up" data-aos-delay="1200">We understand your love for tailor-made then ready-made</p>
                                <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4" data-aos="fade-up" data-aos-delay="1500">Hire us <i class="fas fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('public/img/bg-element-03.png')}}" alt="" class="d-none d-lg-block img-fluid banner-ele-3 animate-rotate slow" data-aos="fade" data-aos-delay="1000" data-aos-offset="-100">
        <a href="#" class="d-none d-xl-block scroll-down text-center" data-aos="fade" data-aos-delay="1500" data-aos-offset="0">
            <i class="fas fa-circle font-size-10 animate-btn"></i>
        </a>
    </section>  


    <section class="py-3 py-lg-5">
        <div class="container-fluid py-xl-5">
            <div class="row">
                <div class="col-lg-6 col-xl-3 d-lg-flex" data-aos="fade-up" data-aos-delay="150" data-aos-offset="0">
                    <a href="{{Route('web-development')}}">
                        <div class="media shadow p-3 card-style-1 mb-3 align-items-stretch d-lg-flex">
                            <div class="avatar-md mr-3 p-3 rounded-circle">
                                <svg class="icon" viewBox="0 0 512 512">
                                    <use xlink:href="#webDevelopment"></use>
                                </svg>
                            </div>
                            <div class="media-body d-lg-flex flex-column">
                                <div>
                                    <h3 class="h4 font-weight-bold">Web Development</h3>
                                    <p>Revolutionize your business with our rich web development services. Talk to us about your requirements.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="{{Route('web-development')}}" class="btn btn-link">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-xl-3 d-lg-flex" data-aos="fade-up" data-aos-delay="300">
                    <a href="{{Route('mobile-app-development')}}">
                        <div class="media shadow p-3 card-style-1 mb-3 align-items-stretch d-lg-flex">
                            <div class="avatar-md mr-3 p-3 rounded-circle">
                                <svg class="icon" viewBox="0 0 64 64">
                                    <use xlink:href="#applicationDevelopment"></use>
                                </svg>
                            </div>
                            <div class="media-body d-lg-flex flex-column">
                                <div>
                                    <h3 class="h4 font-weight-bold">Mobile App Development</h3>
                                    <p>We develop impeccable mobile apps to generate higher conversion, stunning interface, and amazing user experience. </p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="{{Route('mobile-app-development')}}" class="btn btn-link">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-6 col-xl-3 d-lg-flex" data-aos="fade-up" data-aos-delay="450">
                    <a href="{{Route('artificial-intelligence-development')}}">
                        <div class="media shadow p-3 card-style-1 mb-3 align-items-stretch d-lg-flex">
                            <div class="avatar-md mr-3 p-3 rounded-circle">
                                <svg class="icon" viewBox="-16 0 512 512.00104">
                                    <use xlink:href="#realityDevelopment"></use>
                                </svg>
                            </div>
                            <div class="media-body d-lg-flex flex-column">
                                <div>
                                    <h3 class="h4 font-weight-bold">Artificial Intelligence </h3>
                                    <p>Invade the market place with artificial intelligence solutions. We believe in a holistic approach for smart, and reliable AI services.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="{{Route('artificial-intelligence-development')}}" class="btn btn-link">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-6 col-xl-3 d-lg-flex" data-aos="fade-up" data-aos-delay="600">
                    <a href="{{Route('custom-solutions')}}">
                        <div class="media shadow p-3 card-style-1 mb-3 align-items-stretch d-lg-flex">
                            <div class="avatar-md mr-3 p-3 rounded-circle">
                                <svg class="icon" viewBox="0 0 64 64">
                                    <use xlink:href="#customSolution"></use>
                                </svg>
                            </div>
                            <div class="media-body d-lg-flex flex-column">
                                <div>
                                    <h3 class="h4 font-weight-bold">Custom Development</h3>
                                    <p>For your unique business, we have beautiful and unique solutions. Talk to us about your requirement and we will be ready with a custom package.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="{{Route('custom-solutions')}}" class="btn btn-link">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-3 pb-lg-5">
        <div class="container pb-xl-5">
            <div class="row align-items-lg-center">
                <div class="col-lg-5">
                    <h6 class="display-3 text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">Our Work</h6>
                    <h3 class="h1 font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Recent Projects</h3>
                </div>
                <!--  <div class="col-lg-7" data-aos="fade-up" data-aos-delay="300">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit quis corporis eligendi fugiat? Velit exercitationem labore unde praesentium id explicabo beatae quas corporis reiciendis tenetur, natus, quod totam, ullam earum.</p>
                    </div> -->
            </div>
        </div>
    </section>

    <section class="py-3 py-lg-5 recent-projects-wrapper" style="background-image: url('public/img/bg-home-slider.png');">
    <div class="container-fluid pt-xl-5">
                <div class="row justify-content-center">
                    <div class="col-11">
                        <div class="swiper-container recent-projects">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="row flex-lg-row-reverse align-items-lg-center">
                                        <div class="col-lg-6 col-xl-6 offset-xl-2">
                                            <img src="{{asset('public/img/portfolio/presenta/presenta-app.webp')}}" alt="presenta" class="img-fluid">
                                        </div>
                                        <div class="col-lg-6 col-xl-4 text-white">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <img src="{{asset('public/img/portfolio/presenta/logo.png')}}" alt="presenta" class="img-fluid" style="max-width: 150px;">
                                                </div>
                                                <div class="col">
                                                    <h4 class="h3 font-weight-bold">Presenta App</h4>
                                                </div>
                                            </div>
                                            <p class="text-white">Presenta is an Android and iPhone based app to deliver the ultimate accounting solution. In this application, it becomes easy to do some tough tasks of accounting, taxation, and payroll. With the help of AI, we have built an app that can help you get your financial reports with a few clicks.This app is something that is meant to reduce human effort and save valuable time.</p>
                                            
                                            <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Let's Talk <i class="fas fa-arrow-right ml-1"></i></a>
                                            <a href="{{Route('presenta')}}" class="ml-3 btn btn-outline-light mt-lg-3 btn-lg px-lg-4">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="row flex-lg-row-reverse align-items-lg-center">
                                        <div class="col-lg-6 col-xl-6 offset-xl-2">
                                            <img src="{{asset('public/img/portfolio/simpilli/01.webp')}}" alt="simpilli" class="img-fluid" loading="lazy">
                                        </div>
                                        <div class="col-lg-6 col-xl-4 text-white">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <img src="{{asset('public/img/portfolio/simpilli/logo.png')}}" alt="simpilli" class="img-fluid" style="max-width: 150px;">
                                                </div>
                                                <div class="col">
                                                    <h4 class="h3 font-weight-bold">Simpilli App</h4>
                                                </div>
                                            </div>
                                            <p class="text-white">Say goodbye to long queues at the pharmacy store. Simpilli is a mobile and web app, helping to deliver medicine at your doorstep with no contact delivery. The platform connects consumers and nearby pharmacies to deliver the prescribed medicines. Its interaction features i.e. real-time chat, audio, and video call can help in removing all queries related to medication.</p>
                                            
                                            <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Let's Talk <i class="fas fa-arrow-right ml-1"></i></a>
                                            <a href="{{Route('simpilli')}}" class="ml-3 btn btn-outline-light mt-lg-3 btn-lg px-lg-4">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="row flex-lg-row-reverse align-items-lg-center">
                                        <div class="col-lg-6 col-xl-6 offset-xl-2">
                                            <img src="{{asset('public/img/portfolio/holdeed/01.webp')}}" alt="holdeed" class="img-fluid" loading="lazy">
                                        </div>
                                        <div class="col-lg-6 col-xl-4 text-white">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <img src="{{asset('public/img/portfolio/holdeed/logo.png')}}" alt="holdeed" class="img-fluid" style="max-width: 150px;">
                                                </div>
                                                <div class="col">
                                                    <h4 class="h3 font-weight-bold">Holdeed App</h4>
                                                </div>
                                            </div>
                                            <p class="text-white">Holdeed is an administrative software exclusively created to simplify project management, order processing, time management, leave management, and construction documentation. This platform unifies the needs of SMEs, construction companies, and helps them in saving millions of money. It helps to connect directly to employees even when they are working outside the office. It features web as well as app-based access to project manager, supervisor, and company backend</p>
                                            
                                            <a href="{{Route('contact')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Let's Talk <i class="fas fa-arrow-right ml-1"></i></a>
                                            <a href="{{Route('holdeed')}}" class="ml-3 btn btn-outline-light mt-lg-3 btn-lg px-lg-4">Read more <i class="fas fa-arrow-right ml-1"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="py-3 py-lg-5 position-relative overflow-hidden">
        <div class="container py-xl-5">
            <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">technology</h6>
            <h3 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Tools and Tech Stack</h3>
            <div class="row mt-3 mt-xl-5">
                <div class="col-12">
                    <div class="category-filter mb-3 mb-lg-5 text-center">
                        <button class="btn btn-light btn-lg active mr-1 mr-lg-3 mb-2" data-tag="designing">Designing</button>
                        <button class="btn btn-light btn-lg mr-1 mr-lg-3 mb-2" data-tag="frontend">Frontend</button>
                        <button class="btn btn-light btn-lg mr-1 mr-lg-3 mb-2" data-tag="backend">Backend</button>
                        <button class="btn btn-light btn-lg mr-1 mr-lg-3 mb-2" data-tag="database">Database</button>
                        <button class="btn btn-light btn-lg mr-1 mr-lg-3 mb-2" data-tag="mobile">Mobile</button>
                        <button class="btn btn-light btn-lg mb-2" data-tag="ai">AI</button>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row justify-content-center" id="category-list">
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-XD mb-2 tool-tilt mx-auto"></div>
                            <p>Adobe XD</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-PS mb-2 tool-tilt mx-auto"></div>
                            <p>Photoshop</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-AI mb-2 tool-tilt mx-auto"></div>
                            <p>Illustrator</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-ID mb-2 tool-tilt mx-auto"></div>
                            <p>InDesign</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-AE mb-2 tool-tilt mx-auto"></div>
                            <p>After Effects</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-flashbuilder mb-2 tool-tilt mx-auto"></div>
                            <p>Adobe Flash</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-BR mb-2 tool-tilt mx-auto"></div>
                            <p>Adobe Bridge</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="designing">
                            <div class="bg-acrobat mb-2 tool-tilt mx-auto"></div>
                            <p>Adobe Acrobat</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="frontend">
                            <div class="bg-Html mb-2 tool-tilt mx-auto"></div>
                            <p>HTML</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="frontend">
                            <div class="bg-CSS mb-2 tool-tilt mx-auto"></div>
                            <p>CSS</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="frontend">
                            <div class="bg-js mb-2 tool-tilt mx-auto"></div>
                            <p>JavaScript</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="backend">
                            <div class="bg-php mb-2 tool-tilt mx-auto"></div>
                            <p>PHP</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="backend">
                            <div class="bg-Yii mb-2 tool-tilt mx-auto"></div>
                            <p>Yii</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="backend">
                            <div class="bg-Codeignitor mb-2 tool-tilt mx-auto"></div>
                            <p>Codeignitor</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="backend">
                            <div class="bg-Laravel mb-2 tool-tilt mx-auto"></div>
                            <p>Laravel</p>
                        </div>
                        <div class="col-6 col-sm-4 col-lg-2 text-center mb-2 mb-lg-5 " data-category="database">
                            <div class="bg-mysql mb-2 tool-tilt mx-auto"></div>
                            <p>MySql</p>
                        </div>
                        <div class="col-6 col-sm-4 col-lg-2 text-center mb-2 mb-lg-5 " data-category="database">
                            <div class="bg-postgreesql mb-2 tool-tilt mx-auto"></div>
                            <p>PostgreSQL</p>
                        </div>
                        <div class="col-6 col-sm-4 col-lg-2 text-center mb-2 mb-lg-5 " data-category="database">
                            <div class="bg-mongodb mb-2 tool-tilt mx-auto"></div>
                            <p>MongoDB</p>
                        </div>
                        <div class="col-6 col-sm-4 col-lg-2 text-center mb-2 mb-lg-5 " data-category="database">
                            <div class="bg-Firebase mb-2 tool-tilt mx-auto"></div>
                            <p>Firebase</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="mobile">
                            <div class="bg-apple mb-2 tool-tilt mx-auto"></div>
                            <p>Apple</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="mobile">
                            <div class="bg-Android mb-2 tool-tilt mx-auto"></div>
                            <p>Android</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="mobile">
                            <div class="bg-Ionic mb-2 tool-tilt mx-auto"></div>
                            <p>Ionic</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="mobile">
                            <div class="bg-flutter mb-2 tool-tilt mx-auto"></div>
                            <p>Flutter</p>
                        </div>
                        <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="mobile">
                            <div class="bg-React mb-2 tool-tilt mx-auto"></div>
                            <p>React Native</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="ai">
                            <div class="bg-python mb-2 tool-tilt mx-auto"></div>
                            <p>Python</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="ai">
                            <div class="bg-Django mb-2 tool-tilt mx-auto"></div>
                            <p>DJango</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="ai">
                            <div class="bg-Flask mb-2 tool-tilt mx-auto"></div>
                            <p>Flask</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="ai">
                            <div class="bg-Zope mb-2 tool-tilt mx-auto"></div>
                            <p>Zope</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5" data-category="ai">
                            <div class="bg-Pyramid mb-2 tool-tilt mx-auto"></div>
                            <p>Pyramid</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="ai">
                            <div class="bg-Web2py mb-2 tool-tilt mx-auto"></div>
                            <p>Web2py</p>
                        </div>
                        <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 " data-category="ai">
                            <div class="bg-Tornado mb-2 tool-tilt mx-auto"></div>
                            <p>Tornado</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('public/img/dots-circle.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow" style="position: absolute;top: 28%;right: -270px;user-select: none;pointer-events: none;">
        <div class="py-3 py-lg-5 position-relative">
            <img src="{{asset('public/img/svg/welcome.svg')}}" alt="welcome to Multipz Technology" class="img-absolute-50 pl-lg-5 left mb-3 mb-lg-0" loading="eager">
            <div class="container py-xl-5">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <h6 class="display-3 text-uppercase font-weight-bold" style="opacity: 0.1;">welcome to</h6>
                        <h3 class="h1 font-weight-bold mt-n5 mb-4">Multipz Technology</h3>
                        <p class="mb-3">At Multipz Technology, challenge and innovation are not just words, but core parts of the organization. We strongly believe in the value of trust, honesty, value-centricity, and all of these are fueled with the passion for giving less ordinary. We help our clients to reimagine their business for the future by powering the future with everyday innovations and effort. </p>

                        <ul class="nav nav-pills mt-5 mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Our Mission</a>
                            </li>
                            <li class="nav-item ml-3" role="presentation">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Our Vision</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <p>At Multipz Technology, our mission is to be the partner of choice for our customers by delivering high performance, scalable and innovative services & solutions to different industries. </p>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <p>Our philosophy of Less-Ordinary ensures the pursuit of the client’s best interest. Aimed at offering holistic services to our clients to meet present and make them future-ready.</p>
                            </div>
                        </div>

                        <a href="{{Route('about')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Discover <i class="fas fa-arrow-right ml-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-lg-5 position-relative overflow-hidden">
        <div class="container py-xl-5 position-relative">
            <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">process</h6>
            <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">How our projects come ALIVE!</h4>
            <p class="text-center mt-3" data-aos="fade-up" data-aos-delay="150">Our projects grow from initial to final stage with the following process.</p>
            <img src="{{asset('public/img/svg/tir-arrow.svg')}}" alt="arrow" data-aos="fade-up" class="img-fluid d-none d-lg-block" style="position: absolute;left:50%;height: 140px;transform: translateX(-50%);">
            <div class="row justify-content-xl-center mb-5">
                <div class="col-12 col-xl-10 pt-4">
                    <div class="project-process-wrap mt-3 mt-xl-5 pt-5">
                        <div class="row no-gutters">
                            <div class="col-md-6 offset-md-6">
                                <div class="pp-item" data-aos="fade-up">
                                    <h3 class="h3 mb-2">Brainstorming</h3>
                                    <p class="mb-0">This is the first step for every project that we undertake. Our expert team sits down with our clients for a thorough discussion to understand the exact needs of the clients.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <div class="pp-item text-lg-right" data-aos="fade-up">
                                    <h3 class="h3 mb-2">Project Sketching</h3>
                                    <p class="mb-0">Client requirements are thereafter analyzed. Based on the analysis, project timeline and milestones are setup and finalized after approval from the clients.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-6 offset-md-6">
                                <div class="pp-item" data-aos="fade-up">
                                    <h3 class="h3 mb-2">Design & Development</h3>
                                    <p class="mb-0">The designing team takes over from here and creates an elegant and user friendly UI design. Once the design is approved, the developing team steps in implementing the functionalities and processess.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <div class="pp-item text-lg-right" data-aos="fade-up">
                                    <h3 class="h3 mb-2">Testing</h3>
                                    <p class="mb-0">Once the application is developed, it goes through multiple rigorous testing scenarios. This makes sure that the application is entirely bug free with a smooth working flow.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-md-6 offset-md-6">
                                <div class="pp-item" data-aos="fade-up">
                                    <h3 class="h3 mb-2">Project Submission</h3>
                                    <p class="mb-0">After getting a green signal from our testing team, the application is then submitted to the clients.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 57px;" class="d-none d-lg-block"></div>
            <img src="{{asset('public/img/svg/tir-under.svg')}}" data-aos="fade-up" alt="arrow" class="img-fluid d-none d-lg-block" style="position: absolute; left: 50%; height: 153px; transform: translateX(-50%); bottom: 0px;user-select: none;pointer-events: none;">
        </div>

        <div class="bg-primary text-white py-3 py-lg-5 mt-5">
            <div class="container text-center">
                <div class="h1">Give wings to your <b class="text-accent">IDEAS</b>.<br /> We love to discuss real business</div>
                <a href="{{Route('contact')}}" class="btn btn-accent btn-lg px-lg-5 mt-lg-3 py-2 py-lg-3 font-weight-bold font-size-20">Let's Talk</a>
            </div>
        </div>

        <div class="py-3 py-lg-5">
            <div class="container py-xl-5">
                <div class="row align-items-lg-center">
                    <div class="col-lg-7">
                        <h6 class="display-3 text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">testimonials</h6>
                        <h4 class="h1 font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Customer Reviews</h4>
                    </div>
                    <!-- <div class="col-lg-5" data-aos="fade-up" data-aos-delay="300">
                            <p>Give wings to your IDEAS. We love to discuss real business.</p>
                        </div> -->
                </div>
            </div>
        </div>
        <div class="container">
            <div class="swiper-container testimonial-slider py-lg-5" style="background-image: url('public/img/bg-black.png');">
                <div class="swiper-wrapper pb-lg-5">
                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-01.jpg')}}" alt="" class="img-fluid mb-3 mb-lg-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold">Nikunj Patel , India</h4>
                                <p class="text-white"> Great job done by Multipz team. They have built customised software for our business and we are completely driven by the Software.</p>
                                <p class="text-white">They have guided me with extra functionality which in return turned out to be really good. Kudos to the team! </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-02.jpg')}}" alt="" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold">Dhanjiv Pandey , Mumbai , India</h4>
                                <p class="text-white"> Excellent Work Done by Hitesh and his team, First time dealing with Multipz. All I can say is, they are by far the best freelancer I've dealt with. If you're looking for a yes-man for Mobile Apps, go for Hitesh. He has given&nbsp;me advice and recommendations on making the app run smoother. They will straight up tell you no if it's a bad idea and give you a better way of doing it. </p>
                                <p class="text-white"> If you're looking for a team of great developers, stop looking and give Apps-maven a try, they will not disappoint! </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-05.jpg')}}" alt="" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold"> Hitesh Chandwani , Pune , India</h4>
                                <p class="text-white"> Hitesh is a bright, talented programmer and problem solver. His curiosity motivates him to continually learn and improve his projects and his own skills. He is committed to wringing elegant code and making sure his projects are built the “right” way. </p>
                                <p class="text-white">  </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide p-2">
                            <div class="row">
                                <div class="col-4 col-md-4 offset-md-1">
                                    <div class="avatar-testimonial">    
                                        <img src="{{asset('public/img/avatar-03.jpg')}}" alt="Chetan Kachhadiya" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-6 text-white">
                                    <h4 class="h3 font-weight-bold">Chetan Kachhadiya</h4>
                                    <p class="text-white">Multipz team is extremely skilled and has got great critical thinking skills. They give us very earnest suggestion which helped make the software better. They have delivered the software on time with amazing look and feel. Good Job Hitesh and team!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide p-2">
                            <div class="row">
                                <div class="col-4 col-md-4 offset-md-1">
                                    <div class="avatar-testimonial">    
                                        <img src="{{asset('public/img/avatar-02.jpg')}}" alt="Hossam" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-6 text-white">
                                    <h4 class="h3 font-weight-bold">Hossam</h4>
                                    <p class="text-white">Hitesh and his team is very talented and has got very creative ideas. Someone who is looking for attractive and creative work I would recommend Multipz team. They have worked amazingly well for my Application and have got many compliments for the same!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide p-2">
                            <div class="row">
                                <div class="col-4 col-md-4 offset-md-1">
                                    <div class="avatar-testimonial">    
                                        <img src="{{asset('public/img/avatar-04.jpg')}}" alt="Brain & Franco" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-6 text-white">
                                    <h4 class="h3 font-weight-bold">Brain & Franco</h4>
                                    <p class="text-white">Hitesh is very much focused and committed towards his work and has given us completely satisfactory product. If you are looking for person who can give your ideas wings then he is the one! He and his team has been very cooperative and has delivered it on time!</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide p-2">
                            <div class="row">
                                <div class="col-4 col-md-4 offset-md-1">
                                    <div class="avatar-testimonial">    
                                        <img src="{{asset('public/img/avatar-01.jpg')}}" alt="Jeet Diyora" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                    </div>
                                </div>
                                <div class="col-md-7 col-lg-6 text-white">
                                    <h4 class="h3 font-weight-bold">Jeet Diyora</h4>
                                    <p class="text-white">Great job done by Multipz team. They have built customised software for our business and we are completely driven by the Software. They have guided me with extra functionality which in return turned out to be really good. Kudos to the team!</p>
                                </div>
                            </div>
                        </div>

                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-03.jpg')}}" alt="" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold"> David Vargas </h4>
                                <p class="text-white"> I use Multipz for all of my offshore projects currently. Team is well qualified and have incredible level of professionalism. I'd recommend them to anyone looking to expand in house capabilities with their very capable staff. </p>
                                <p class="text-white"> </p>
                            </div>
                        </div>
                    </div>
                     
                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-02.jpg')}}" alt="" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold"> Netphone Co. Group </h4>
                                <p class="text-white"> Hitesh good job! We are completely satisfied with the Integration of SIP, VOIP service in our system. He has got great developer’s team who are very skilled and cooperative. The system is working absolutely fine and is far better than we expected. </p>
                                <p class="text-white"> Hitesh has been the man of his words and has delivered the finished product on time. </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide p-2">
                        <div class="row">
                            <div class="col-4 col-md-4 offset-md-1">
                                <div class="avatar-testimonial">
                                    <img src="{{asset('public/img/avatar-03.jpg')}}" alt="" class="img-fluid mb-3 mb-md-0 shadow-lg" loading="lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-6 text-white">
                                <h4 class="h3 font-weight-bold"> Ankit Dudhwewala  , Ahmedabad , India</h4>
                                <p class="text-white">Commandable work done by Hitesh Khunt, </p>
                                <p class="text-white">Hitesh has incredible programming abilities and great bunch of developers. He has additionally given numerous suggestions about what is right now going in the market and has helped me improve my application (CallHippo). On the off chance that you are searching for individual who can convey the quality item with less measure of time then Hitesh is the one. He conveys finish attractive application.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination sr-only"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next d-none d-lg-block"></div>
                <div class="swiper-button-prev d-none d-lg-block"></div>
            </div>
        </div>
        <img src="{{asset('public/img/dots-square.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow" style="position: absolute;top: 28%;left: -270px;user-select: none;pointer-events: none;">
        <img src="{{asset('public/img/dots-curve.png')}}" alt="" class="d-none d-xl-block img-fluid animate-updown" style="position: absolute;top: 28%;right: -270px;user-select: none;pointer-events: none;">
    </section>

    <section class="py-3 py-lg-5">
        <div class="container py-xl-5">
            <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">blog</h6>
            <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Recent Blog</h4>
            <div class="row mt-5">
                <div class="col-md-4">
                    <div class="card card-style-2">
                        <div class="text-uppercase align-items-center align-self-start avatar-md bg-accent d-flex font-weight-bold h5 justify-content-center mb-n4 ml-3 p-1 text-secondary" style="z-index: 1;">
                            13<br>July
                        </div>
                        <a href="{{Route('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality')}}">
                            <img src="{{asset('public/img/blog-01.png')}}" class="img-fluid" alt="" loading="lazy">
                            <div class="card-body px-0">
                                <h5 class="card-title">Artificial Intelligence: Give a Beautiful Wing to Future Reality</h5>
                                <!-- <div class="border-top border-bottom py-2 text-muted small mb-2">
                                        <i class="fa fa-user-alt mr-1"></i>Chris Evan
                                    </div> -->
                                <p class="card-text">Siri here is not a person, but the AI-enabled device that is making life easy for people like Jack and others. Once what was shown in sci-fi movies like driverless cars to voice automation devices, it is now a reality of the world.</p>
                                <div class="text-right">
                                    <a href="{{Route('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality')}}" class="btn btn-link">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-style-2">
                        <div class="text-uppercase align-items-center align-self-start avatar-md bg-accent d-flex font-weight-bold h5 justify-content-center mb-n4 ml-3 p-1 text-secondary" style="z-index: 1;">
                            13<br>July
                        </div>
                        <a href="{{Route('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App')}}">
                            <img src="{{asset('public/img/blog-02.png')}}" class="img-fluid" alt="" loading="lazy">
                            <div class="card-body px-0">
                                <h5 class="card-title">Who is the Ultimate Winner: Hybrid vs Native App</h5>
                                <!-- <div class="border-top border-bottom py-2 text-muted small mb-2">
                                        <i class="fa fa-user-alt mr-1"></i>Chris Evan
                                    </div> -->
                                <p class="card-text">App development is no longer a luxury to enjoy, but has become a prime necessity in today’s time. With the rise in use of smartphones, it becomes vital to have a business app to remain competitive.</p>
                                <div class="text-right">
                                    <a href="{{Route('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App')}}" class="btn btn-link">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-style-2">
                        <div class="text-uppercase align-items-center align-self-start avatar-md bg-accent d-flex font-weight-bold h5 justify-content-center mb-n4 ml-3 p-1 text-secondary" style="z-index: 1;">
                            13<br>July
                        </div>
                        <a href="{{Route('Which-is-the-Best-Flutter-or-React-Native')}}">
                            <img src="{{asset('public/img/blog-03.png')}}" class="img-fluid" alt="" loading="lazy">
                            <div class="card-body px-0">
                                <h5 class="card-title">Which is the Best: Flutter or React Native</h5>
                                <!-- <div class="border-top border-bottom py-2 text-muted small mb-2">
                                        <i class="fa fa-user-alt mr-1"></i>Chris Evan
                                    </div> -->
                                <p class="card-text">Who will be the ultimate ruler in future marketing? Which cross-platform app development framework is best for my business?</p>
                                <div class="text-right">
                                    <a href="{{Route('Which-is-the-Best-Flutter-or-React-Native')}}" class="btn btn-link">Read more <i class="fa fa-arrow-right ml-1"></i></a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="py-3 py-lg-5">
        <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">partners</h6>
        <h4 class="h1 text-center font-weight-bold mt-n5 mb-5" data-aos="fade-up" data-aos-delay="100">Our Valuable Clients</h4>

        <div class="bg-light py-3 py-lg-5">
            <div class="container-fluid">
                <div class="swiper-container our-partner-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide text-center">
                            <img src="{{asset('public/img/partner/GPBO.png')}}" alt="GPBO" class="img-fluid" loading="lazy">
                            <h6 class="mt-1">GPBO</h6>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="{{asset('public/img/partner/Holdeed.png')}}" alt="Holdeed" class="img-fluid" loading="lazy">
                            <h6 class="mt-1">Holdeed</h6>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="{{asset('public/img/partner/Joykicks.png')}}" alt="Joykicks" class="img-fluid" loading="lazy">
                            <h6 class="mt-1">Joykicks</h6>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="{{asset('public/img/partner/presenta.png')}}" alt="presenta" class="img-fluid" loading="lazy">
                            <h6 class="mt-1">Presenta</h6>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="{{asset('public/img/partner/Simpilli.png')}}" alt="Simpilli" class="img-fluid" loading="lazy">
                            <h6 class="mt-1">Simpilli</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container py-4">
            <div class="swiper-container badge-carousel">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide">
                        <script type="text/javascript" src="https://widget.clutch.co/static/js/widget.js"></script>
                        <div class="clutch-widget" data-url="https://widget.clutch.co" data-widget-type="2" data-height="50" data-clutchcompany-id="333148"></div>
                    </div>
                    <div class="swiper-slide">
                        <div><a target="_blank" href="https://www.goodfirms.co/company/multipz-technology"><img style="max-width:160px" class="img-fluid" src="https://goodfirms.s3.amazonaws.com/badges/color-badge/top-software-development-companies.svg" alt="GoodFirms Badge"></a></div>
                    </div>
                    <div class="swiper-slide">
                        <div><a href=https://www.itfirms.co/top-mobile-app-development-companies/ target="_blank" ><img class="img-fluid" src=https://www.itfirms.co/wp-content/uploads/2020/01/mobile-app-developer-2020.png></a></div>
                    </div>
                    <div class="swiper-slide">
                        <a href="https://www.topmobileappdevelopmentcompany.com/top-mobile-app-development-company-california/" target="_blank"><img class="img-fluid" src="https://www.topmobileappdevelopmentcompany.com/wp-content/uploads/2018/12/logo.png"></a> 
                    </div>
                    <div class="swiper-slide">
                        <a href="https://topwebdevelopmentcompanies.com/in/top-10/web-development/india" target="_blank"> <img class="img-fluid" src="https://topwebdevelopmentcompanies.com/badges/top-web-development-companies.png" alt="web development companies india" title="Web Development Companies India" style="height: 150px; width: 150px;" /></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="https://wadline.com/mobile/us" target="_blank" rel="noopener noreferrer"><img style="max-width:125px" class="img-fluid" src="https://wadline.com/badges/june/badge/Badge_Monocolor_Mobile.svg" alt="badge"></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="mb-5 d-none d-md-block"></div>

        
    </section>

    <!--  <section class="py-3 py-lg-5 call-to-action">
            <div class="container text-center">
                <p class="text-secondary">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <a href="#" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Contat us <i class="fas fa-arrow-right ml-1"></i></a>
            </div>
        </section> -->
</main>
@endsection