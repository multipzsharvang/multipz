@extends('layout.master')

@section('title', 'Hire Dedicated Developer')
@section('meta_description', 'Create your presence on the web with a leading web development company. We offer a one-stop solution for website design and development services. Get a quote now!')
@section('image')
<meta property="og:image" content="{{asset('public/img/favicon.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/favicon.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">

    <div class="container py-5">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h1 class="font-weight-bold display-3 mb-lg-4" data-aos="fade-up" data-aos-delay="600">Hire Dedicated Developer In India</h1>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="font-weight-bold h3  mb-lg-4" data-aos="fade-up" data-aos-delay="800">
                            <span class="letter-space-2 title-strike">Hire our Dedicated Developers and effectuate ideas into reality</span>
                        </div>
                        <p data-aos="fade-up" data-aos-delay="1200">Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt sunt sapiente iure numquam corrupti tempore, minus quas labore. At ad dicta quod, accusamus veritatis aspernatur nostrum reprehenderit numquam autem.</p>

                        <p data-aos="fade-up" data-aos-delay="1400">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis sunt saepe tenetur perferendis cumque quos facere autem error laborum culpa sed, maxime laudantium maiores tempora molestiae, quisquam in voluptas! Saepe?</p>
                    </div>
                    <div class="col-lg-5" data-aos="fade-up" data-aos-delay="1200">
                        <div class="h4 mb-lg-4" data-aos="fade-up" data-aos-delay="800">Hire our Dedicated Developers and effectuate ideas into reality</div>
                        <ul class="list-unstyled">
                            <li class="mb-2"><i class="fas fa-wallet mr-3 text-accent align-middle"></i>Reduction in Unplanned Cost</li>
                            <li class="mb-2"><i class="fas fa-shield-alt mr-3 text-accent align-middle"></i>Saves A lot of Time & Cost</li>
                            <li class="mb-2"><i class="fas fa-laptop-code mr-3 text-accent align-middle"></i>Meet the Deadlines</li>
                            <li class="mb-2"><i class="fas fa-chart-line mr-3 text-accent align-middle"></i>Goes Above & Beyond For The Project</li>
                            <li class="mb-2"><i class="fas fa-handshake mr-3 text-accent align-middle"></i>Flexible Resources</li>
                            <li class="mb-2"><i class="fas fa-newspaper mr-3 text-accent align-middle"></i>Doesn't Reserve A Place In Your Premises</li>
                            <li><i class="fas fa-clock mr-3 text-accent align-middle"></i>Round The Clock Service Provider</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="py-3 py-lg-5 bg-primary text-white">
        <div class="container">
            <h4 class="h1 text-center font-weight-bold" data-aos="fade-up" data-aos-delay="100">Hire Dedicated Developers</h4>
            <p class="text-white text-center" data-aos="fade-up" data-aos-delay="100">We have dedicated team players who work very efficiently and help you build a robust application. Also, we divide our team in two different ways, i.e., Multipz-Managed team and Client-Managed team.</p>
            <div class="row mt-5">
                <div class="col-md-6 text-center p-md-5">
                    <div class="avatar-lg mx-auto mb-3">
                        <svg class="icon" viewBox="0 0 480 480">
                            <use xlink:href="#multipz-Managed"></use>
                        </svg>
                    </div>
                    <div class="h3 font-weight-bold mb-3">Multipz-Managed<br />Teams</div>
                    <p class="text-white">We at Multipz manages more than 100+ employees, out of which 30+ are developers, who are profound and efficient in developing end-to-end customer-oriented website or web-apps. We have a team that is assigned for a project and a project manager who looks after the project and regularly monitors and updates the project status to the clients.</p>
                </div>
                <div class="col-md-6 text-center p-md-5 border-left-dash">
                    <div class="avatar-lg mx-auto mb-3">
                        <svg class="icon" viewBox="0 0 496 496">
                            <use xlink:href="#Client-Managed"></use>
                        </svg>
                    </div>
                    <div class="h3 font-weight-bold mb-3">Client-Managed<br />Teams</div>
                    <p class="text-white">We also provide a team that will be completely managed by the client to provide an all-round solution to your projects. We provide you a project manager who will be in constant coordination with the client through different communication mediums. Also, we are open for client’s preferred communication channel to coordinate through their preferred channel.</p>
                </div>
            </div>
            <div class="text-center mb-4">
                <a href="{{Route('contact')}}" class="btn btn-accent btn-lg">Hire Dedicated Developer</a>
            </div>
        </div>
    </section>
    <div class="mb-5"></div>

</main>

@endsection

@section('script')

@endsection