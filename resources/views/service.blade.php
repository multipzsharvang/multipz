@extends('layout.master')

@section('title', 'Best Custom Web and App Development Company | AI Development Company')
@section('meta_description', 'We are one of the leading custom development companies, offer the best design and development solution for Web, Mobile App, and AI Development. Get a quote now!')
@section('image')
<meta property="og:image" content="{{asset('public/img/bg-service-banner.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/bg-service-banner.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="container-fluid">
        <div class="row align-items-center min-vh-100" style="background: url('public/img/bg-service-banner.png')no-repeat center / cover;">
            <div class="col-lg-6 col-xl-5 pl-xl-5 text-white">
                <h1 class="font-weight-bold display-3 mb-xl-3" data-aos="fade-up" data-aos-delay="600">Our Services  Towards a Better Tomorrow</h1>
                <p class="col-lg-10 px-0 mb-xl-3 text-white" data-aos="fade-up" data-aos-delay="1200">Today, if you are not online, your identity becomes questionable. The era now demands innovation to empower the business. Multipz Technology has perfectly honed the art of impeccable IT solutions with rich technical expertise and proven experience in several industries.</p>
            </div>
        </div>
    </div>
    <section class="py-3 py-lg-5 position-relative overflow-hidden">
        <div class="container-fluid">
            <div class="flex-md-row-reverse row align-items-center position-relative">
                <div class="col-md-6 mb-3 mb-lg-0 text-center">
                    <img src="{{asset('public/img/service-web.png')}}" alt="web development" class="img-fluid animate-floating">
                </div>
                <div class="col-md-6 col-lg-4 offset-lg-2 mb-3 mb-lg-0 bg-secondary">
                    <h3 data-depth="0.5" class="h3 font-weight-bold mb-3">Web Development</h3>
                    <p>Tap the power of the digital-enabled age with our impactful web solutions to elevate your real business potential. Multipz Technology as a web development company committed to providing full-fledged development services across the globe. With us, you will find a perfect blend of skills, quality structure, and affordable solution. Let’s get together to build a beautiful looking front-end and secure back-end for the complete satiate experience.</p>
                    <a href="{{Route('web-development')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Know More <i class="fas fa-arrow-right ml-1"></i></a>
                </div>
                <div class="svg-draw d-none d-lg-block" data-900-top="height:0%;" data--500-bottom="height:100%;" style=" position: absolute; top: 68%; height: 73.4615%; background: url('public/img/svg/line.svg') center top / 100% no-repeat; width: 410px; left: 35%; z-index: -1; ">
                </div>
            </div>
            <div class="row align-items-center position-relative">
                <div class="col-md-6 mb-3 mb-lg-0 text-center">
                    <img src="{{asset('public/img/service-mobile.png')}}" alt="Mobile App development" class="img-fluid animate-floating">
                </div>
                <div class="col-md-6 col-lg-4 mb-3 mb-lg-0 bg-secondary py-3">
                    <h3 class="h3 font-weight-bold mb-3">Mobile App Development</h3>
                    <p>Our young and energetic, mobile app development team understands the importance of powerful apps for greater benefits of the business. We are here to serve the needs of iOS, Android, and Windows platforms for your small and large business houses. We understand the importance of the business app for you, and that is why we appoint experts. Multipz Technology is the destination for every business, as we understand the importance and beauty of different business models. Get in touch with us for all the custom mobile app requirements. </p>
                    <a href="{{Route('mobile-app-development')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Know More <i class="fas fa-arrow-right ml-1"></i></a>
                </div>
                <div class="svg-draw d-none d-lg-block" data-900-top="height:0%;" data--500-bottom="height:100%;" style=" position: absolute; top: 66%; height: 73.4615%; background: url('public/img/svg/line-1.svg') center top / 100% no-repeat; width: 410px; right: 42%; z-index: -1; "></div>
            </div>
            <div class="flex-md-row-reverse row align-items-center position-relative">
                <div class="col-md-6 mb-3 mb-lg-0 text-center">
                    <img src="{{asset('public/img/service-ai.png')}}" alt="Artificial Intelligence " class="img-fluid animate-floating">
                </div>
                <div class="col-md-6 col-lg-4 offset-lg-2 mb-3 mb-lg-0 bg-secondary">
                    <h3 data-depth="0.5" class="h3 font-weight-bold mb-3">Artificial Intelligence </h3>
                    <p>Build a smarter world with artificial intelligence. Robust your business performance with our machine learning, chatbot development, speech recognition, deep learning, natural language processing, and data analysis services. Get in touch today to get cogent solutions that combine seamlessly with the client’s business model for everyday growth.</p>
                    <a href="{{Route('artificial-intelligence-development')}}" data-depth="1" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Know More <i class="fas fa-arrow-right ml-1"></i></a>
                </div>
                <div class="svg-draw d-none d-lg-block" data-900-top="height:0%;" data--500-bottom="height:100%;" style=" position: absolute; top: 68%; height: 73.4615%; background: url('public/img/svg/line.svg') center top / 100% no-repeat; width: 410px; left: 35%; z-index: -1; ">
                </div>
            </div>
            <div class="row align-items-center position-relative">
                <div class="col-md-6 mb-3 mb-lg-0 text-center">
                    <img src="{{asset('public/img/service-custom.png')}}" alt="custom development" class="img-fluid animate-floating">
                </div>
                <div class="col-md-6 col-lg-4 mb-3 mb-lg-0 bg-secondary">
                    <h3 class="h3 font-weight-bold mb-3">Custom Solutions</h3>
                    <p>Build bespoke solutions that can work for your business. Our team of experienced developers helps to build custom development solutions including mobile payment app development, health web app, finance app, e-Commerce website, etc, at affordable prices.</p>
                    <a href="{{Route('custom-solutions')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Know More <i class="fas fa-arrow-right ml-1"></i></a>
                </div>
            </div>
        </div>
        <img src="{{asset('public/img/dots-circle.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow" style="position: absolute; bottom: 0; right: -180px; user-select: none; pointer-events: none; max-width: 360px;">
        <img src="{{asset('public/img/dots-square.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow" style="position: absolute;top: 18%;left: -270px;user-select: none;pointer-events: none; ">
    </section>
    <div class="mb-5"></div>

</main>

@endsection

@section('script')
<script type="text/javascript">
    var s = skrollr.init();
    if (s.isMobile()) {
        s.destroy();
    }
</script>
@endsection