<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Feedback Response</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            min-width: 100% !important;
        }

        img {
            height: auto;
        }

        .content {
            width: 100%;
            max-width: 600px;
        }

        .header {
            padding: 40px 30px 20px 30px;
        }

        .innerpadding {
            padding: 30px 30px 30px 30px;
        }

        .borderbottom {
            border-bottom: 1px solid #f2eeed;
        }

        .subhead {
            font-size: 15px;
            color: #D35400;
            font-family: sans-serif;
            letter-spacing: 10px;
        }

        .h1,
        .h2,
        .bodycopy {
            color: #153643;
            font-family: sans-serif;
        }

        .h1 {
            font-size: 22px;
            line-height: 38px;
            font-weight: bold;
        }

        .h2 {
            padding: 0 0 15px 0;
            font-size: 20px;
            line-height: 28px;
            font-weight: bold;
        }



        .bodycopy {
            font-size: 16px;
            line-height: 22px;
        }

        .button {
            text-align: center;
            font-size: 18px;
            font-family: sans-serif;
            font-weight: bold;
            padding: 0 30px 0 30px;
        }

        .button a {
            color: #ffffff;
            text-decoration: none;
        }

        .footer {
            padding: 20px 30px 15px 30px;
        }

        .footercopy {
            font-family: sans-serif;
            font-size: 14px;
            color: #ffffff;
        }

        .footercopy a {
            color: #ffffff;
            text-decoration: underline;
        }

        @media only screen and (max-width: 550px),
        screen and (max-device-width: 550px) {
            body[yahoo] .hide {
                display: none !important;
            }

            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0px !important;
            }

            body[yahoo] .button a {
                background-color: #e05443;
                padding: 15px 15px 13px !important;
            }

            body[yahoo] .unsubscribe {
                display: block;
                margin-top: 20px;
                padding: 10px 50px;
                background: #2f3942;
                border-radius: 5px;
                text-decoration: none !important;
                font-weight: bold;
            }
        }

        /*@media only screen and (min-device-width: 601px) {
    .content {width: 600px !important;}
    .col425 {width: 425px!important;}
    .col380 {width: 380px!important;}
    }*/
    </style>
</head>

<body yahoo bgcolor="#f6f8f1">
    <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" style="margin: 3rem 0 0;">
        <tr>
            <td>
                <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->
                <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td bgcolor="#c7d8a7" class="header">
                            <table width="100" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="100" style="padding: 0 10px 10px 0;">
                                        <img class="fix" src="{{$message->embed(asset('public/img/loog.png'))}}" alt="multipz" width="100" height="100" border="0" />
                                    </td>
                                </tr>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
                            <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;">
                                <tr>
                                    <td height="70">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="subhead" style="padding: 0 0 0 3px;">
                                                    Dear,
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="h1" style="padding: 5px 0 0 0;">
                                                    {{$name}}
                                                </td>


                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td class="innerpadding borderbottom">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="h2">
                                        {{$greeting}} <br />
                                        Thanks for being awesome!
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodycopy">
                                        Hey there,<br />

                                        You are important to us. Thank you for writing to us. We will get back to you at the earliest convenience. <br />
                                        In case of immediate assistance requirements, <a href="tel:+919016464035,+910792518537">dial</a> our contact number. Otherwise, we will respond as soon as possible. <br />
                                        Talk to you soon.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="footer" bgcolor="#44525f">
                            <table width="100%" height="50%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" class="footercopy">

                                        © {{$year}} - All Rights Reserved By Multipz Technology
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" style="padding: 15px 0 0 0;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://www.facebook.com/multipzservices/">
                                                        <img src="{{$message->embed(asset('public/img/facebook.png'))}}" width="37" height="37" alt="Facebook" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://twitter.com/multipztech">
                                                        <img src="{{$message->embed(asset('public/img/twitter.png'))}}" width="37" height="37" alt="Twitter" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://www.instagram.com/multipztechnology/">
                                                        <img src="{{$message->embed(asset('public/img/instagram.png'))}}" width="37" height="37" alt="instagram" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://www.youtube.com/channel/UC7k0wfx0-poLZbfunciCDZw/videos?view_as=subscriber">
                                                        <img src="{{$message->embed(asset('public/img/youtube.png'))}}" width="37" height="37" alt="Youtube" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://is.gd/iymuH2">
                                                        <img src="{{$message->embed(asset('public/img/skype.png'))}}" width="37" height="37" alt="Skype" border="0" />
                                                    </a>
                                                </td>

                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://www.linkedin.com/company/multipztechnology/">
                                                        <img src="{{$message->embed(asset('public/img/linkedin.png'))}}" width="37" height="37" alt="Linkedin" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://www.behance.net/MultipzTechnology">
                                                        <img src="{{$message->embed(asset('public/img/behance.png'))}}" width="37" height="37" alt="Behance" border="0" />
                                                    </a>
                                                </td>
                                                <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                                                    <a href="https://in.pinterest.com/multipztechnology/">
                                                        <img src="{{$message->embed(asset('public/img/pinterest.png'))}}" width="37" height="37" alt="Pinterest" border="0" />
                                                    </a>
                                                </td>

                                            </tr>

                                        </table>
                                    </td>
                                </tr><br/>
                                <tr>
                                    <td align="center" class="footercopy" style="font-size: small;">
                                        <b>Note:</b> This mail is auto generated by the system.
                                    </td>
                                </tr>
                                

                            </table>
                        </td>

                    </tr>

                </table>
                <!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]-->
            </td>
        </tr>
    </table>
</body>

</html>