@extends('layout.master')

@section('title', 'Artificial Intelligence Development Company | AI Developer')
@section('meta_description', 'Get the expert artificial intelligence development solution of Machine Learning, Image Processing, Natural Language Processing, Data Analysis, etc. Contact us now!')
@section('image')
<meta property="og:image" content="{{asset('public/img/icon-ml.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/icon-ml.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="hero position-relative overflow-hidden">
        <div class="embed-responsive embed-responsive-16by9">
            <video src="{{asset('public/img/ai-video.mp4')}}" loop muted autoplay></video>
            <div style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;background-color: rgb(0, 0, 0); mix-blend-mode: saturation;"></div>
        </div>

        <div class="position-absolute d-md-flex align-items-center" style="top: 0;bottom: 0;left: 0;right: 0;">
            <div class="container py-5">
                <div class="row align-items-center">
                    <div class="col-lg-12 text-white">
                        <h1 class="font-weight-bold display-3 mb-lg-4" data-aos="fade-up" data-aos-delay="600">Artificial Intelligence</h1>
                        <p class="text-white" data-aos="fade-up" data-aos-delay="1200">Can machines think like humans? Well, yes. Unleash the potential of AI to turn your business smarter, profitable, and future-ready with us. Find out how our artificial intelligence development services can empower your business and people for a better tomorrow.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="ai-features-wrap">
        <div class="container">
            <div class="row py-lg-5 align-items-lg-center position-relative" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/machine-learning.png')}}" alt="Machine Learning" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white">
                    <img src="{{asset('public/img/icon-ml.png')}}" alt="Machine Learning Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">Machine Learning</h4>
                    </h3>
                    <p>Machine learning is a study of algorithms and statistical models that help the computer system in performing particular tasks. We are masters in machine learning with a strong hands-on experience in developing solutions that act more accurate in performing tasks. With our solution, you can gather unstructured data and convert it into actionable insight to improve business growth.</p>
                </div>
            </div>

            <div class="row py-lg-5 flex-lg-row-reverse" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/image-processing.png')}}" alt="Image Processing" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white text-lg-right mr-n1 align-self-center">
                    <img src="{{asset('public/img/icon-ip.png')}}" alt="Image Processing Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">Image Processing</h4>
                    </h3>
                    <p>As a technology-driven organization, we take the help of technology advancement for better image and image data processing. Our solution will process the image, manipulate it, then analyze deep insights from it. With this, you can make better decisions to flourish your business.</p>
                </div>
            </div>

            <div class="row py-lg-5 align-items-lg-center position-relative" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/nlp.png')}}" alt="Natural Language Processing" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white">
                    <img src="{{asset('public/img/icon-nlp.png')}}" alt="Natural Language Processing Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">Natural Language Processing</h4>
                    </h3>
                    <p>NLP helps you to understand unstructured data, phrases, grammar, and language to drive insights. We take the help of NLP to build smart software and applications that will keep you ready for an insightful business strategy.</p>
                </div>
            </div>
            <div class="row py-lg-5 flex-lg-row-reverse" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/data-analysis.png')}}" alt="Data Analysis" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white text-lg-right mr-n1 align-self-center">
                    <img src="{{asset('public/img/icon-data-analysis.png')}}" alt="Data Analysis Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">Data Analysis</h4>
                    </h3>
                    <p>Hire our AI-powered data-analysis services to find insights and patterns in an extensive database. It helps to improve business by offering predictions about customer choice, ideal marketing channels, and product development.</p>
                </div>
            </div>

            <div class="row py-lg-5 align-items-lg-center position-relative" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/data-science.png')}}" alt="Data Science" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white">
                    <img src="{{asset('public/img/icon-data-science.png')}}" alt="Data Science Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">Data Science</h4>
                    </h3>
                    <p>Data is a goldmine, and with the help of data science, you can strengthen your business. Our data science uses tools, applications, algorithms to make sense from large data clusters to reach your organizational goal and business process.</p>
                </div>
            </div>

            <div class="row py-lg-5 flex-lg-row-reverse" data-aos="fade-up" data-aos-delay="500">
                <span class="d-none d-lg-block" style="display: inline-block;width: 4px;position: absolute;top: 0;bottom: 0;left: 50%; background-color: #000;"></span>
                <div class="col-lg-6 py-lg-5">
                    <div class="img-wrap">
                        <img src="{{asset('public/img/ai-algorithm.png')}}" alt="AI - Based Algorithm" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 px-lg-0 bg-white text-lg-right mr-n1 align-self-center">
                    <img src="{{asset('public/img/icon-ai.png')}}" alt="AI - Based Algorithm Icon" class="img-fluid icon d-none d-lg-inline-block">
                    <h3>
                        <h4 class="h1 font-weight-bold">AI - Based Algorithm</h4>
                    </h3>
                    <p>Our service assists you in providing tremendous ROI. Using AI-Based algorithms, you can find some helpful solutions to accomplish the next level of success. Enhance your brand recognition and boost productivity with our AI solutions.</p>
                </div>
            </div>
        </div>
    </section>

    <div class="bg-primary text-white py-3 py-lg-5 mt-5">
        <div class="container text-center">
            <div class="h1">Uncover the best <span class="text-accent font-weight-bold">AI solution</span> for your business model. Discover possibilities with us</div>
            <a href="{{Route('contact')}}" class="btn btn-accent btn-lg px-lg-5 mt-lg-3 py-2 py-lg-3 font-weight-bold font-size-16">Let's Talk</a>
        </div>
    </div>

    <section class="py-3 py-lg-5">
        <div class="container pt-xl-5">
            <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">technology</h6>
            <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Tools and Tech Stack</h4>
            <div class="row mt-3 mt-xl-5 justify-content-center align-items-top">
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-python mb-2 tool-tilt mx-auto"></div>
                    <p>Python</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Django mb-2 tool-tilt mx-auto"></div>
                    <p>DJango</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Flask mb-2 tool-tilt mx-auto"></div>
                    <p>Flask</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Zope mb-2 tool-tilt mx-auto"></div>
                    <p>Zope</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Pyramid mb-2 tool-tilt mx-auto"></div>
                    <p>Pyramid</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Web2py mb-2 tool-tilt mx-auto"></div>
                    <p>Web2py</p>
                </div>
                <div class="col-4 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5">
                    <div class="bg-Tornado mb-2 tool-tilt mx-auto"></div>
                    <p>Tornado</p>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3 py-lg-5 bg-primary text-white mb-lg-5">
        <div class="container">
            <div class="h1 font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="300">FAQs</div>
            <div class="accordion" id="faq">
                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq1" class="text-white d-block h4 pr-4" data-toggle="collapse" aria-expanded="false" aria-controls="faq1">
                    What is Artificial Intelligence?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq1" class="collapse show" data-parent="#faq">
                        <p class="text-white py-3">
                        Also known as machine intelligence, artificial intelligence builds smart machines capable of performing tasks that require human intelligence. In short, here, machines can think and provide a solution.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq2" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq2">
                    How can AI be beneficial for my business?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq2" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                        Since AI is in the early development stage, we sure AI will help you in making your business smoother and profitable. We fit in the pocket of a perfect artificial intelligence development company.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq3" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq3">
                    How can I assess the progress or updates on my AI project?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq3" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                        Our artificial intelligence developers regularly keep our clients updated about the project. Our client can keep track of what precisely is going on with the effective communication cycle.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq4" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq4">
                    How long does it take to implement an AI-driven solution?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq4" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                        It depends on the scope, may take a few weeks, or a few months maybe. We follow an agile development process to deliver projects on time with no compromise on quality standards.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div class="mb-5"></div>
</main>
@endsection

@section('script')
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity": [{
            "@type": "Question",
            "name": "What is Artificial Intelligence?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Also known as machine intelligence, artificial intelligence builds smart machines capable of performing tasks that require human intelligence. In short, here, machines can think and provide a solution."
            }
        }, {
            "@type": "Question",
            "name": "How can AI be beneficial for my business?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Since AI is in the early development stage, we sure AI will help you in making your business smoother and profitable. We fit in the pocket of a perfect artificial intelligence development company."
            }
        }, {
            "@type": "Question",
            "name": "How can I assess the progress or updates on my AI project?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Our artificial intelligence developers regularly keep our clients updated about the project. Our client can keep track of what precisely is going on with the effective communication cycle."
            }
        }, {
            "@type": "Question",
            "name": "How long does it take to implement an AI-driven solution?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "It depends on the scope, may take a few weeks, or a few months maybe. We follow an agile development process to deliver projects on time with no compromise on quality standards."
            }
        }]
    }
</script>
@endsection