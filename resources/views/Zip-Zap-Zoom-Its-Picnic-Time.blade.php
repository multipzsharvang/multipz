@extends('layout.master')

@section('title', 'Zip...Zap. . .Zoom - It’s Picnic Time')
@section('meta_description', 'So finally we get some time to spread the joy. An awesome trip ends with some awesome memories. Thanks, Multipz to make this memorable day for us.')
@section('image')
<meta property="og:image" content="{{asset('public/img/blog/picnic-banner.webp')}}" />
<meta name="twitter:image" content="{{asset('public/img/blog/picnic-banner.webp')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <img src="{{asset('public/img/blog/picnic-banner.webp')}}" alt="Zip...Zap. . .Zoom - It’s Picnic Time" class="w-100">

    <div class="container py-3 py-lg-5 blog-details">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="font-weight-bold display-3 mb-lg-4">Zip...Zap. . .Zoom - It’s Picnic Time</h1>

                <p>A job fills your pocket, adventure fills your soul, and with Multipz Technology you can fill both pocket and soul. You do not believe me? Well... in that case, I would love to take you to the Parvati Hills, our picnic location of 2020. The new year of the new decade is doing no good to the world, and that is why this picnic acts as a positivity booster.</p>

                <p>The day reminds our peeps of the beautiful life we are blessed with, without worrying about any deadly virus, soul-thrashing news, and all horrific things happening around the world.</p>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <img src="{{asset('public/img/blog/breakfast.webp')}}" alt="breakfast" class="img-fluid mb-3" loading="lazy">
                    </div>
                    <div class="col-lg-6">
                        <p>The beautiful day begins with the yummy breakfast on the top of the hill. Imagine that your day begins on the top of a hill with all your favorite food to eat, stunning scenery stealing your heart, and the veil of mist, making it a picturesque experience for eyes and soul. Yes, it's the actual view from the breakfast table. The chirping birds and incredible green were perfect for a great start.</p>
                    </div>
                </div>


                <p>After breakfast, we were ready for the real fun. Oh! Man, it was something that I had experienced for the first time in life. The crazy gypsy rides took us to the adventure setup that was beyond any words description. The adventure setup begins with some warm activities such as Burma bridge, balance walk, parallel rope, suspension bridge, and commando net.</p>
                
                <p>Once we were all pumped up, the next task rope was the real commando challenge. We all were ready to sky the touch (not literally) but with the safety harness kit. The three-level setup was full of various settings including skywalk, Bungee swing, tire bridge, rope ladder, log to log, and zig-zag plank. </p>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <img src="{{asset('public/img/blog/adventure-01.webp')}}" alt="adventure-01" class="img-fluid mb-3" loading="lazy">
                    </div>
                    <div class="col-lg-6">
                        <img src="{{asset('public/img/blog/adventure-02.webp')}}" alt="adventure-02" class="img-fluid mb-3" loading="lazy">
                    </div>
                </div>
                
                <p>Half of the people were focused on completing different levels, a bunch were motivators, while others were giving tips. It was like a complete military-like experience. The joy of completing all three levels has its own swag. Our team never learns to back off from taking any risk being it professional front or activities front. </p>

                <p>On coming back, yummy lunch was awaiting for us. On refueling the empty stomach with mouth-watering food, our techy was ready for the swimming pool. It was literally a crazy experience with all office buddies bonding over swimming lessons, video making, and crazy poses.</p>

                <p>The next was the jungle safari, a real safari to experience dense forest, surreal view, and of course fun. Our desi Vin Diesel giving a tough competition to Formula1 racers. The gypsy driver was equipped with great driving skills to make the whole safari experience thrilling. On reaching the sunset point, the view was spectacular with a beautiful lake surrounded by dense forest, making us realize what actual peace is?</p>

                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <p>The beautiful day begins with the yummy breakfast on the top of the hill. Imagine that your day begins on the top of a hill with all your favorite food to eat, stunning scenery stealing your heart, and the veil of mist, making it a picturesque experience for eyes and soul. Yes, it's the actual view from the breakfast table. The chirping birds and incredible green were perfect for a great start.</p>
                    </div>
                    <div class="col-lg-4 offset-lg-1">
                        <img src="{{asset('public/img/blog/safari.webp')}}" alt="safari" class="img-fluid mb-3" loading="lazy">
                    </div>
                </div>

                <p>The evening time was filled with soothing music, games, and snacks. We all wished that the day should not get over. The bewitching beauty of the surrounding, glorious sunset and twinkling stars illuminated our end of Day.</p>

            </div>
        </div>
    </div>
    <div class="mb-5"></div>
</main>
@endsection