@extends('layout.master')
@section('title', 'About Us | Who We Are - Multipz Technology')
@section('meta_description', 'With the mission of 100% client satisfaction, Multipz Technology provides the best client-oriented high performing, innovative service, and solutions.')
@section('image')
<meta property="og:image" content="{{asset('public/img/bg-about-banner.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/bg-about-banner.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="hero position-relative overflow-hidden">
        <div class="container-fluid position-relative">
            <div class="row align-items-center min-vh-100" style="background: url('public/img/bg-about-banner.png')no-repeat center / cover;">
                <div class="col-lg-6 col-xl-5 pl-xl-5 text-white">
                    <h4 class="h3 font-weight-bold mb-xl-3" data-aos="fade-up">About Us</h4>
                    <h1 class="font-weight-bold display-3 mb-xl-3" data-aos="fade-up" data-aos-delay="600">Reimagine business for the digital age</h1>
                    <p class="col-lg-10 px-0 mb-xl-3 text-white" data-aos="fade-up" data-aos-delay="1200">Multipz Technology is a multi-service company with a focus to provide innovative and value-based services. We have been helping businesses through our web development, artificial intelligence, custom development, and mobile app development services. Our core competency lies in providing in-house and offshore solutions.</p>
                </div>
            </div>
        </div>
    </div>
    <section class="py-3 py-lg-5 position-relative overflow-hidden">
        <div class="container py-xl-5">
            <h6 class="display-4 text-center text-uppercase font-weight-bold" data-aos="fade-up" data-aos-delay="300">we grow brands</h6>
            <h4 class="h1 text-center font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="100">"Multipz Technology"</h4>
            <p class="text-center">At Multipz Technology, challenges and innovations are not just words, but core parts of the organization. Our strong faith in the value of trust, honesty, value-centric, fuels our passion to offer less ordinary. We help our clients to reimagine their business for the better future with our everyday innovations and effort.</p>
            <p class="text-center">We cater our services to B2B and B2C business models, irrespective of its business size. Flexible with our work approach, we can help you in areas such as consulting, strategy, roadmaps, testing, technologies, and lots more. Whether you need to speed up your revenue growth or run your business smoothly, Multipz technology is the ultimate choice. </p>

            <div class="align-items-stretch mt-5 row">
                <div class="col-md-6 px-xl-5 d-flex mb-3 mb-lg-0">
                    <div class="p-3 p-lg-5 card-about text-center">
                        <div class="h4 font-weight-bold mb-3">Our Mission</div>
                        <p>At Multipz Technology, our mission is to be the partner of choice for delivering high performance, scalable, and innovative services & solutions. Our mission to develop contemporary IT solutions for the sustainable growth of our clients.</p>
                    </div>
                </div>
                <div class="col-md-6 px-xl-5 d-flex mb-3 mb-lg-0">
                    <div class="p-3 p-lg-5 card-about text-center">
                        <div class="h4 font-weight-bold mb-3">Our Vision</div>
                        <p>Established to make startups and large firms future-ready, Multipz Technology wants to be a top choice for all IT solutions. Founded with the idea to make a difference in the digital age by catering to trusted, quality, and supportive services. Our philosophy of Less-Ordinary ensures the pursuit of the client’s best interest. Aimed at offering holistic services to our clients to meet present and make them future-ready.</p>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('public/img/dots-circle.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow " style="position: absolute; bottom: 0; right: -180px; user-select: none; pointer-events: none; max-width: 360px;">
        <img src="{{asset('public/img/dots-square.png')}}" alt="" class="d-none d-xl-block img-fluid animate-rotate slow" style="position: absolute;top: 18%;left: -270px;user-select: none;pointer-events: none; ">
    </section>

    <section class="py-3 py-lg-5">
        <div class="container">
            <h6 class="display-3 text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">quick facts</h6>
            <h4 class="h1 font-weight-bold mt-n5 mb-5" data-aos="fade-up" data-aos-delay="100">Quick Facts</h4>
        </div>
        <div class="bg-light py-3 py-lg-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6 col-md-3 text-center mb-3 mb-md-0">
                        <div id="count-locations" class="display-4 font-weight-bold"></div>
                        <div class="text-uppercase font-weight-bold">locations</div>
                    </div>
                    <div class="col-6 col-md-3 text-center mb-3 mb-md-0">
                        <div class="display-4 font-weight-bold"><span class="d-none d-md-inline-block">+</span> <span id="count-projects"></span></div>
                        <div class="text-uppercase font-weight-bold">projects</div>
                    </div>
                    <div class="col-6 col-md-3 text-center mb-3 mb-md-0">
                        <div class="display-4 font-weight-bold"><span class="d-none d-md-inline-block">+</span> <span id="count-years"></span></div>
                        <div class="text-uppercase font-weight-bold">years</div>
                    </div>
                    <div class="col-6 col-md-3 text-center mb-3 mb-md-0">
                        <div class="display-4 font-weight-bold"><span class="d-none d-md-inline-block">+</span> <span id="count-employees"></span></div>
                        <div class="text-uppercase font-weight-bold">employees</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-lg-5 position-relative overflow-hidden">
        <div class="container">
            <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">succeed</h6>
            <h4 class="h1 text-center font-weight-bold mt-n5 mb-5" data-aos="fade-up" data-aos-delay="100">Why work with us?</h4>

            <div class="row">
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0 pb-md-4">
                    <object data="{{asset('public/img/svg/expert-team.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">

                    </object>
                    <div class="font-weight-boldmb-3 h5">Expert & Experienced Team</div>
                    <p>Multipz Technology has an experienced team that is ready to walk the extra mile to serve extraordinary. We keep our employees technically ahead with regular training, which further helps in providing the most efficient business solutions.</p>
                </div>
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0 pb-md-4 border-left-dash border-right-dash">
                    <object data="{{asset('public/img/svg/transparency.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">

                    </object>
                    <div class="font-weight-boldmb-3 h5">Transparency</div>
                    <p>We are not good at the hide and seek game & that is why we keep transparency. Multipz Technology believes that the base of a strong and unshakable foundation is a total transparency with clients and employees.</p>
                </div>
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0 pb-md-4">
                    <object data="{{asset('public/img/svg/piggy-bank.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">

                    </object>
                    <div class="font-weight-boldmb-3 h5">Cost-saving solutions ​</div>
                    <p>Multipz Technology believes that with innovation and dedication, the cost-effective solution is not impossible. You can rely on our expertise for effective plus cost-saving solutions.</p>
                </div>
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0 ">
                    <object data="{{asset('public/img/svg/idea.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">

                    </object>
                    <div class="font-weight-boldmb-3 h5">Deep focus on Innovation​</div>
                    <p>Our deep focus is innovation. We weave different methodologies and strategies so that our clients can attain their goals with minimal spending.</p>
                </div>
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0  border-left-dash border-right-dash">
                    <object data="{{asset('public/img/svg/customer.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">

                    </object>
                    <div class="font-weight-boldmb-3 h5">Strong client relationships</div>
                    <p>We believe in keeping our clients updated with the ongoing progress. Our sincere efforts and remarkable services help to withhold our clients. We ensure transparency about the ongoing progress and working methodologies via email or a defined communication channel. </p>
                </div>
                <div class="col-sm-6 col-md-4 px-lg-4 mb-3 mb-md-0 ">
                    <object data="{{asset('public/img/svg/results.svg')}}" type="image/svg+xml" style="position: absolute; z-index: -1; opacity: 0.08; right: 10px; top: 0; width: 80px;">
                    </object>
                    <div class="font-weight-boldmb-3 h5">Result Oriented approach​</div>
                    <p>Our team ensures to include only the result-oriented approach in every project. We guarantee you the quality and best results. </p>
                </div>
            </div>
        </div>
        <div class="container py-3 py-lg-5">
            <h6 class="display-3 text-right text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">industry</h6>
            <h4 class="h1 text-right font-weight-bold mt-n5 mb-5" data-aos="fade-up" data-aos-delay="100">Industry We Serve</h4>
        </div>
        <div class="container">
            <div class="industry-wrap">
                <div class="industry-detail">
                    <div>
                        <div class="h4 font-weight-bold mb-3">Industries we serve</div>
                        <p class="text-white">Your search for a reliable IT partner ends here. We have served various industries, including manufacturing, education, healthcare & fitness, eCommerce, and many more.</p>
                        <a href="{{Route('service')}}" class="btn btn-accent mt-lg-3 btn-lg px-lg-4">Our Expertise <i class="fas fa-arrow-right ml-1"></i></a>
                    </div>
                </div>
                <div class="industry industry-ecommerce">
                    <img src="{{asset('public/img/ecommerce.png')}}" alt="ecommerce">
                    <div class="box">eCommerce</div>
                </div>
                <div class="industry industry-education">
                    <img src="{{asset('public/img/education.png')}}" alt="education">
                    <div class="box">Education</div>
                </div>
                <div class="industry industry-manufacturing">
                    <img src="{{asset('public/img/manufacturing.png')}}" alt="manufacturing">
                    <div class="box">Manufacturing</div>
                </div>
                <div class="industry industry-food">
                    <img src="{{asset('public/img/food-restaurent.png')}}" alt="food">
                    <div class="box">Food & Restaurant</div>
                </div>
                <div class="industry industry-healthcare">
                    <img src="{{asset('public/img/healthcare.png')}}" alt="healthcare">
                    <div class="box">Healthcare & Fitness</div>
                </div>
                <div class="industry industry-solution">
                    <img src="{{asset('public/img/ondemand.png')}}" alt="solution">
                    <div class="box">On-Demand Solutions</div>
                </div>
            </div>
        </div>

        <!-- <div class="container py-3 py-lg-5">
            <h6 class="display-3 text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">leadership</h6>
            <h4 class="h1 font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Our Team</h4>

        </div>

        <div class="container-fluid bg-light">
            <div class="row">
                <div class="col-lg-3 py-lg-5">
                    <figure class="text-center text-capitalize img-hover-style-1">
                        <img src="{{asset('public/img/team/chirag.png')}}" alt="Chirag" class="img-fluid">
                        <figcaption>
                            <div class="font-weight-bold h4  mb-1">Chirag Patel</div>
                            <p>Director</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-9">
                    <div class="swiper-container team-slider py-lg-5">
                        <div class="swiper-wrapper">
                        <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/nikunj_k.png')}}" alt="Nikunj" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Nikunj Kothiya</div>
                                        <p>Back-End Developer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/taruna.png')}}" alt="Taruna" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Taruna Valiya</div>
                                        <p>Back-End Developer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/devang.png')}}" alt="Devang" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Devang Lakhani</div>
                                        <p>iOS Developer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/bhavesh.png')}}" alt="Bhavesh" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Bhavesh Makwana</div>
                                        <p>Android Developer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/ketan.png')}}" alt="ketan" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">ketan amode</div>
                                        <p>graphics designer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/Hitendra.png')}}" alt="Hitendra" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Hitendra Soni</div>
                                        <p>SEO - Team Lead</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/meenakshi.png')}}" alt="meenakshi" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Meenakshi Herbha</div>
                                        <p>Content Writer</p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="text-center text-capitalize img-hover-style-1">
                                    <img src="{{asset('public/img/team/viral.png')}}" alt="Viral" class="img-fluid">
                                    <figcaption>
                                        <div class="font-weight-bold h4  mb-1">Viral Dave</div>
                                        <p>Business Development Manager</p>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <img class="thumbnail" src="{{asset('public/img/dots-multiple.png')}}" alt="" style="position: absolute;top:20%;left: -60px;z-index: -1;user-select: none;">
        <img class="thumbnail" src="{{asset('public/img/dots-shape.png')}}" alt="" style="position: absolute;bottom:24%;max-width:250px;right: -60px;z-index: -1;user-select: none;">
    </section>



</main>

@endsection

@section('script')
<script>
(function($) {
	$.fn.jQuerySimpleCounter = function( options ) {
	    let settings = $.extend({
	        start:  0,
	        end:    100,
	        easing: 'swing',
	        duration: 400,
	        complete: ''
	    }, options );

	    const thisElement = $(this);

	    $({count: settings.start}).animate({count: settings.end}, {
			duration: settings.duration,
			easing: settings.easing,
			step: function() {
				let mathCount = Math.ceil(this.count);
				thisElement.text(mathCount);
			},
			complete: settings.complete
		});
	};

}(jQuery));

    $('#count-locations').jQuerySimpleCounter({
        start: 0,
        end: 4,
        duration: 5000
    });
    $('#count-projects').jQuerySimpleCounter({
        start: 0,
        end: 250,
        duration: 20000
    });
    $('#count-years').jQuerySimpleCounter({
        start: 0,
        end: 7,
        duration: 10000
    });
    $('#count-employees').jQuerySimpleCounter({
        start: 0,
        end: 50,
        duration: 10000
    });
</script>
@endsection