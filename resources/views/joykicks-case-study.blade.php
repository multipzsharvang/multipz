@extends('layout.master')

@section('title', 'Joykicks: E-commerce Application for Android and iOS')
@section('meta_description', 'JoyKicks is an eCommerce application that connects the consumer to service providers for the need of party packages, catering, market, and inflatable bouncers.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
<div class="position-relative overflow-hidden">
    <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-4 col-sm-auto">
                    <img src="{{asset('public/img/portfolio/joykiks/logo.png')}}" alt="joykicks" class="img-fluid">
                </div>
            </div>
        </div>
    </div>   
</div>     
    
    <section class="py-3 py-md-5">
        <div class="container">
           <h1> <h2 class="display-3 text-uppercase font-weight-bold">Joykicks</h2></h1>
            <p>JoyKicks is an e-commerce app based on Android and iOS. It is a platform where consumers can get connected to the service provider for the need of party packages, catering, rental products, and inflatable bouncers goods.</p>
            <div class="row mt-lg-3">
                <div class="col-12">
                    <h3 class="h4 font-weight-bold mb-3">Technology Stack</h3>
                </div>
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>iOS: Flutter (dart)</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Android: Flutter (dart)</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Backend: MySQL, Laravel</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model : B2B, B2C</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language Option : English and Arabic</li>
                    </ul>
                </div>
            </div>
            <!-- <div class="row justify-content-center pt-3">
                <div class="col-auto mb-2 mb-md-0">
                    <a href="https://" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                <div class="col-auto">
                    <a href="https://" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div> -->
        </div>
    </section>



    <div class="py-3 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{asset('public/img/portfolio/joykiks/KNOW.png')}}" alt="Know More" class="img-fluid thumbnail-left">
                </div>
                <div class="col-md-7">
                    <h2 class="h1 font-weight-bold">Know Our Client</h2> 
                    <p>The client is from Qatar, who wished to bridge the gap between consumer and business owners through his ecommerce platform. He came to us with an idea where he wants to connect the dots between different products & services like birthday packages, rental products, and the food sector to the ultimate customers.</p>

                    <h3 class="h1 font-weight-bold">What was the client's demand?</h3>
                    <p>The client wished to create a platform to connect the end-users and service providers based on retail, rental, and bulk purchase. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="py-3 py-lg-5" style="background: url('public/img/portfolio/joykiks/problem.png')no-repeat left center / contain;">
        <div class="container">
           <h3> <h4 class="display-4 mb-3 font-weight-bold">Problems before the app</h4> </h3>
            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>The absence of a platform to connect different offline products and services in Qatar.</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Hassle to assemble all party itineraries to organize the party.</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>It was difficult to find rental products in nearby locations.</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Organizing parties, get-together, and other events were a big deal for consumers.</li>
            </ul>
        </div>
    </div>

    <div class="py-3 py-lg-5 position-relative">
        <img src="{{asset('public/img/portfolio/joykiks/hurdle.png')}}" alt="eCommerce application" class="img-absolute-25 left mb-3 mb-lg-0 thumbnail">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 offset-lg-4">
                   <h3> <h4 class="display-4 mb-3 font-weight-bold">Hurdles with the project</h4> </h3>
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Difficult to integrate different genres of service providers on a single platform.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Client being a non-technical person was not sure about the platform he wanted.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Non-clarity about the exact requirements</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Arabic Language Module - It was a challenging yet learning experience (as UI moves from Left to Right).</li>
                    </ul>
                </div>
            </div>
            <img src="{{asset('public/img/portfolio/joykiks/mobiles.png')}}" alt="eCommerce app for Android and iOS" class="img-fluid mx-auto d-block mt-md-5 thumbnail-left">
        </div>
    </div>


    <div class="pb-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <h2 class="display-4 mb-3 font-weight-bold"><br class="d-none d-md-block">Let us talk about our Plan</h2> 
                    <p>Considering the target and challenges of the client, our team was providing a full-fledge solution. They started closely analyzing the buyer’s behavior to evacuate the exact reason behind it. Once the doubts were clear, the team started building a strategy to build an impeccable application for a marvelous experience.</p>
                </div>
            </div>
        </div>
    </div>
     

    <div class="py-3 py-lg-5 position-relative overflow-hidden">
        <img src="{{asset('public/img/portfolio/joykiks/feature.png')}}" alt="eCommerce app" class="img-absolute-25 right mb-3 mb-lg-0 mt-n5 thumbnail">
        <div class="container">
            <h3 class="display-4 font-weight-bold mb-4">Remarkable features of the app </h3>
            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>A platform to connect nearby stores</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Facilitate rental and bulk orders</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Date and day options available to book the rental order</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>An option to write special requirement (if any)</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Cart and wishlist options are available</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Show the current status of the order</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy order gateway (for both parties)</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to give feedback on the service</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Management can add or disable categories and subcategories</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Great UX and easy navigation</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Management can approve, hide & remove store</li>
                <li class="mb-lg-3"><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to track the current location of the order</li>
            </ul>
        </div>
        
    </div>

    <div class="py-3 py-lg-5 bg-tablet-none" style="background: url('public/img/portfolio/gpbo/waves.png')no-repeat 98% center / contain;">
        <div class="container">
            <h3 class="display-4 font-weight-bold">What is before and after the app?</h3> 
            <p>Before the app, arranging parties demand blood and sweat as they have to contact different vendors, discuss proposals, and have to crack the right deal. The worst part was that sometimes people could not find rental products like cars, inflatable bouncers, etc. at a single platform which has its own pain.</p>
            <p>But now, thanks to JoyKicks for adding more joy in any party organization and celebrations as the whole arrangement is just a click away. Here you will get the complete arrangements at your destination without worrying much about anything. Even if you wish to get rental products for certain days, JoyKicks is the right destination.  </p>
        </div>
    </div>

    <div class="work-nav container-fluid py-5">
        <div class="row py-md-5 justify-content-around">
            <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                <a href="{{Route('gpbo')}}">
                    <div class="h1 mb-0">GPBO</div>
                    <span class="text-black-50">Prev Project</span>
                </a>
            </div>
            <div class="col-sm-auto text-center text-sm-right">
                <a href="{{Route('simpilli')}}">
                    <div class="h1 mb-0">Simpilli</div>
                    <span class="text-black-50">Next Project</span>
                </a>
            </div>
        </div>
        <div class="list">
            <a href="{{Route('work')}}">
                <i class="fas fa-th fa-3x"></i>
            </a>
        </div>
    </div>

    <div class="mb-sm-5"></div>
</main>
@endsection

@section('script')
<script>
    $('#count-locations').jQuerySimpleCounter({ start: 0, end: 4, duration: 5000 });
    $('#count-projects').jQuerySimpleCounter({ start: 0, end: 250, duration: 20000 });
    $('#count-years').jQuerySimpleCounter({ start: 0, end: 7, duration: 10000 });
    $('#count-employees').jQuerySimpleCounter({ start: 0, end: 50, duration: 10000 });
</script>
@endsection

