@extends('layout.master')

@section('title', 'Simpilli: Pharmacy App For Mobile and Web')
@section('meta_description', 'Multipz Technology has developed Simpilli, a digital medication delivery solution that can be easy to use for all being it a 10 years kid or 80 years old guy.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio/simpilli/logo.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio/simpilli/logo.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="position-relative overflow-hidden"></div>
    <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-4 col-sm-auto">
                    <img src="{{asset('public/img/portfolio/simpilli/logo.png')}}" alt="simpilli" class="img-fluid">
                </div>
            </div>
        </div>
    </div>  
</div>      
    
    <section class="py-3 py-md-5">
        <div class="container">
           <h1> <h2 class="display-3 text-uppercase font-weight-bold text-center">simpilli</h2> </h1>
            <p>Time to say goodbye to long queues of the pharmacy store with simpilli app. It is a mobile and web app, helping deliver medicine at your doorstep with no contact delivery. The platform connects consumers and nearby pharmacies to deliver the prescribed medicines. Its interaction features i.e. real-time chat, audio, and video call can help to remove all queries related to the medication.</p>
            <div class="row mt-lg-3">
                <div class="col-12">
                    <h3 class="h4 font-weight-bold mb-3">Technology Stack</h3>
                </div>
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>iOS: Swift</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Android: Java</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Backend: MySQL, Laravel</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Chat: Node (Socket)</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Audio & Video call : Agora.io</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Graphic Design : Adobe XD</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model : B2B, B2C</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language Option : English</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-6 col-sm-auto mb-2 mb-md-0">
                    <a href="https://play.google.com/store/apps/details?id=com.simpilli.client&amp;hl=en_IN" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                <div class="col-6 col-sm-auto">
                    <a href="https://apps.apple.com/us/app/simpilli-send-prescriptions/id1525174514" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </section>



    <div class="py-3 py-lg-5 bg-sm-img-auto mb-lg-5" style="background: url('public/img/portfolio/simpilli/challanges.png')no-repeat left center / cover;">
        <div class="d-none d-md-block" style="height:380px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                  <h3>  <h4 class="display-4 font-weight-bold text-white">Challenges<br class="d-none d-lg-inline-block"/> Before The App</h4> </h3>
                    <ul class="pl-0 list-unstyled line-height-lg mb-0 text-white">
                        <li><i class="fa fa-dot-circle mr-2 text-white"></i>Higher chances of contamination during the pandemic time.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-white"></i>The Lockdown situation made people stay inside their homes.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-white"></i>Difficult to find a nearby pharmacy.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-white"></i>Difficult for the senior citizen to collect medicines from the pharmacy store.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <picture>
        <source media="(min-width:768px)" srcset="{{asset('public/img/portfolio/simpilli/about.png')}}" class="img-fluid">
        <img src="{{asset('public/img/portfolio/simpilli/about.png')}}" alt="Challanges" class="w-100">
    </picture>

    <div class="py-3 pt-lg-5 bg-mobile-none" style="background: #fff url('public/img/portfolio/simpilli/circle.png')no-repeat center / contain;">
        <div class="container">
            <h3 class="h1 font-weight-bold" style="color: #0C9BAC;">What was the client's requirement?</h3>
            <p>The client asked for a Digital Medication Delivery solution that can be easy to use for all; be it a 10-year kid or 80 years old guy. He wants a platform that connects nearby pharmacies and consumers with the option of the video call and chat. It should also enable prescription to scan & upload to get the medicines at the doorstep.</p>

            <h3 class="mt-lg-5 h1 font-weight-bold" style="color: #0C9BAC;">Challenges with the project</h3> 
            <ul class="pl-0 list-unstyled line-height-lg mb-0" style="color: #666666;">
                <li><i class="font-size-12 fa fa-dot-circle mr-2"></i>Order management was a major challenge.</li>
                <li><i class="font-size-12 fa fa-dot-circle mr-2"></i>Difficulty in connecting different pharmacies on a common platform.</li>
                <li><i class="font-size-12 fa fa-dot-circle mr-2"></i>The limited-time was another major constraint.</li>
            </ul>

            <h2 class="mt-lg-5 h1 font-weight-bold" style="color: #0C9BAC;">Our strategy</h2> 
            <p>Keeping all requirements of the client in mind, our tech team developed a blueprint and layout of the app. Once we got a nod from the client, we were ready to start the process with a bang. Multipz technology wishes to accommodate all high-tech features without compromising on the application's navigational ease.</p>
        </div>
    </div>

    <div class="py-3 py-lg-5 position-relative overflow-hidden">
        <img src="{{asset('public/img/portfolio/simpilli/salient-feature.png')}}" alt="simpilli mobile and web app" class="img-absolute-50 left mb-3 mb-lg-0">
        <div class="container py-xl-5">
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <h3 class="display-4 font-weight-bold mt-n5 mb-4" style="color: #0C9BAC;">Salient features of the app</h3> 
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to scan and upload prescription</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Offer home delivery or pickup facility</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Get medicines delivered from a nearby pharmacy based on your current location</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Offer real-time chat option</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>The consumer can make call request for medication discussion at the pharmacy store</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Video/audio call request option: Here pharmacist will revert to customers at an appropriate time</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy for the delivery boy to track the location</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>For takeaway: no queue as the medicine kit will be ready when the consumer reaches the store</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Generate ticket option for feedback</li>
                    </ul>
                </div>
            </div>                
        </div>
    </div>
     

    <div class="py-3 py-lg-5 bg-mobile-none" style="background: url('public/img/portfolio/simpilli/BG-last.png')no-repeat center / cover;">
        <div class="container">
           <h3> <h4 class="h1 font-weight-bold" style="color: #0C9BAC;">Before and after app scenario</h4> </h3>
            <p>During the pandemic time, the fear of the virus was more contagious than the virus itself. It was difficult to find the right medication at the right time from the nearby pharmacy.</p>
            <p>However, after the app, life becomes peaceful as people do not have to leave their house to get medicines as it can be delivered at their doorstep. Even pharmacies find the app useful as it keeps the momentum of business flow. The app is a win-win solution for both parties, i.e. consumers and the pharmacy.</p>
        </div>
    </div>

    <div class="work-nav container-fluid py-5">
        <div class="row py-md-5 justify-content-around">
            <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                <a href="{{Route('joykick')}}">
                    <div class="h1 mb-0">Joykicks</div>
                    <span class="text-black-50">Prev Project</span>
                </a>
            </div>
            <div class="col-sm-auto text-center text-sm-right">
                <a href="{{Route('callhippo')}}">
                    <div class="h1 mb-0">Call Hippo</div>
                    <span class="text-black-50">Next Project</span>
                </a>
            </div>
        </div>
        <div class="list">
            <a href="{{Route('work')}}">
                <i class="fas fa-th fa-3x"></i>
            </a>
        </div>
    </div>

    <div class="mb-sm-5"></div>
</main>
@endsection

@section('script')
<script>
    $('#count-locations').jQuerySimpleCounter({ start: 0, end: 4, duration: 5000 });
    $('#count-projects').jQuerySimpleCounter({ start: 0, end: 250, duration: 20000 });
    $('#count-years').jQuerySimpleCounter({ start: 0, end: 7, duration: 10000 });
    $('#count-employees').jQuerySimpleCounter({ start: 0, end: 50, duration: 10000 });
</script>
@endsection