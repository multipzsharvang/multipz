<!DOCTYPE html>
<html lang="en">
<html>

<head>
    <meta charset="UTF-8">
    <title>Thank You!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex nofollow" />
    <link rel="shortcut icon" href="{{asset('public/img/favicon.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="{{asset('public/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/grid.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/main.css')}}">

</head>


<body>
    <!--Header-->
    <div id="loader">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" height="40px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" fill="none" stroke="#ffffff" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(282.534 50 50)">
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
            </circle>
        </svg>
    </div>






    <div class="align-items-center d-flex jumbotron justify-content-center mb-0 min-vh-100 text-center">
        <div>

            <h1 class="display-3">Thank You!</h1>
           
            <p class="lead"><strong>Dear {{ $name }},</strong> <i class="fa fa-heart text-danger"></i> 
                &nbsp;Please check your email.</p>
           
            <hr>
            <p>
                <h6>Disclaimer: We understand your privacy. We will protect your personal information and not share it with any other entities or third parties.</h6>
            </p>
            <p class="lead">
                <a class="btn btn-primary btn-sm" href="{{Route('index')}}" role="button">Continue to homepage</a>
            </p>

        </div>
    </div>

    <div class="d-none d-lg-block cursor"></div>

    <script src="{{asset('public/js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{asset('public/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/js/tilt.jquery.min.js')}}"></script>
    <script src="{{asset('public/js/ling-gallery-filter.js')}}"></script>
    <script src="{{asset('public/js/typewriter.js')}}"></script>

    <script src="{{asset('public/js/aos2_3_4.js')}}"></script>
    <script src="{{asset('public/js/swiper_min.js')}}"></script>
    <script src="{{asset('public/js/simpleParallax_min.js')}}"></script>
    <script src="{{asset('public/js/skrollr.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.0/anime.min.js" integrity="sha512-LfB+BcvR3zBt7ebVskhSWiSbEUiG3p5EcCprkieldsKhBeR6wpnLi0VpWC2GNgVGWP2n/skO8Bx2oKNjUhXCkw==" crossorigin="anonymous"></script>
    <script src="{{asset('public/js/main.js')}}"></script>
</body>

</html>