@extends('layout.master')
@section('title', 'CallHippo: Virtual Phone Application For Web, Android & iOS')
@section('meta_description', 'CallHippo is a virtual phone application created to the analysis of call through call recording, live call monitoring, routing, and other striking features.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="position-relative overflow-hidden">
        <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-4 col-sm-auto">
                        <img src="{{asset('public/img/portfolio/callhippo/logo.png')}}" alt="callhippo" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
    <section class="py-3 py-md-5">
        <div class="container py-xl-5">
            <h1 class="display-3 text-uppercase font-weight-bold text-center">Call Hippo</h1>
            <p>CallHippo is a cloud call center solution designed to meet the requirements of small, medium, large businesses, and startups. Not only that, it gives a 
complete analysis of call through call recording, live call monitoring, routing, and other striking features.</p>
            <div class="row mt-lg-3">
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>For iOS : Swift</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>For backend : Node</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model : B2B</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language option : English</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-6 col-sm-auto mb-2 mb-md-0">
                    <a href="https://play.google.com/store/apps/details?id=com.callhippo.bueno.callhippo" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                <div class="col-6 col-sm-auto">
                    <a href="https://apps.apple.com/us/app/callhippo-virtual-phone-system/id1256873576" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </section>


    <div class="pb-3 position-relative overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h1 font-weight-bold mb-4">A Brief About Client</h2>
                    <p>The client is located in Ahmedabad, Gujarat. The main aim of the client is to build a solution that helps small as well as big enterprises to grow without spending a fortune.</p>

                    <h3 class="h1 font-weight-bold mt-lg-5 mb-4">Problems before the app</h3> 
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Unsatisfied clients</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Unsatisfied call management</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Productivity was less </li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>The chances of error were high </li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Mismanagement resulted in a reduced number of customers</li>
                    </ul>
                </div>
            </div>
            <img src="{{asset('public/img/portfolio/callhippo/banner-01.webp')}}" alt="" class="img-fluid">
        </div>


        <div class="container py-3 pt-lg-5">
            <h3 class="h1 font-weight-bold">What are the client’s requirements?</h3> 
            <p>The requirement of the client was to build a call center software. The software should be able to reduce human labor plus should facilitate the buying of international numbers. </p>

            <h3 class="h1 font-weight-bold">Hurdles During The Project</h3> 
            <p>Before us, the client had already hired 12 companies, but none of them could meet his expectations. So, for us, the real challenge was to prove our worth as a technical company by providing a simple yet effective solution. During this project journey, below are some technical challenges that had bothered us for a while</p>
            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>PLIVO integration was quite a roadblock.</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Silent Push notification was another issue.</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>To create multi-screen support was a colossal task.</li>
            </ul>

            <img src="{{asset('public/img/portfolio/callhippo/banner-02.webp')}}" alt="" class="img-fluid mx-auto d-block mt-5">
        </div>

    </div>


    <div class="pb-3 pb-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="h1 font-weight-bold mb-4">Our Master Plan</h3> 
                    <p>Considering the target and challenges of the client, our team was aiming to provide a full-fledge solution. They started closely analyzing the buyer’s behavior to evacuate the exact reason behind it. Once the doubts were clear, the team started building a strategy to build an impeccable application for a marvelous experience. </p>

                    <h3 class="h1 font-weight-bold mt-lg-5 mb-4">Attributes of the app</h3> 
                    <p>The client, a former employee of a construction company in Switzerland. He was unhappy with the loopholes in the construction business. These loopholes were costing millions of dollars to business and valuable time. </p>
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Power dial: Automatically dial number from the uploaded list of a particular campaign one after one.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Analytics: With track records on call duration, call volume, routing the inbound, and outbound call, help to optimize the sales. Call Analytics helps to formulate data-driven strategies for better services.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>SmartSwitch: Give flexibility to agents to toggle between multiple telephony providers to get better call connectivity.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Smart Call Forwarding: It enables you to forward incoming calls to an alternate number. Now, no missed call even after office with conditional or unconditional smart call forwarding features.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Global Connect: Get acquainted with the time zone before dialing any international number within a fraction of a second.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Transfer: Easy intra-firm and inter-departmental live call transfers.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Queueing: No engage tone, customize messages for clients are some highlights of the call queue feature. This way, you can callback your customers if they do not want to wait in virtual queues.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Recording: For better monitoring, you can record every call to implement better strategies and boost productivity.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Conference: It allows the supervisor or manager to enter into a two-way call, making it a three-way.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Call Barging: It allows the supervisor to silently enter into the conference bridge, and even the supervisor can talk to both parties.</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Voicemail: Instead of waiting for the agent to talk to the client, the customer can send the voice message that can be reverted via reply mail.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="py-3 pb-lg-5">
        <div class="container">
            <h2 class="h1 font-weight-bold">What has changed Before and After app?</h2>
            <p>Before the app, there were difficulties in tracking and answering hundreds of calls every day. It was difficult to find loopholes in marketing strategy, as sometimes solutions or speech used by agents might not be satisfactory.</p>
            <p>But after the app, it becomes easy to handle the flow of the customers with minimum chances of errors, and greater flexibility. It helps to improve customers’ satisfaction and makes it easy to buy international numbers with no hassle.</p>
        </div>
    </div>

    <div class="work-nav container-fluid py-5">
        <div class="row py-md-5 justify-content-around">
            <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                <a href="{{Route('simpilli')}}">
                    <div class="h1 mb-0">Simpilli</div>
                    <span class="text-black-50">Prev Project</span>
                </a>
            </div>
            <div class="col-sm-auto text-center text-sm-right">
                <a href="{{Route('presenta')}}">
                    <div class="h1 mb-0">Presenta</div>
                    <span class="text-black-50">Next Project</span>
                </a>
            </div>
        </div>
        <div class="list">
            <a href="{{Route('work')}}">
                <i class="fas fa-th fa-3x"></i>
            </a>
        </div>
    </div>

    <div class="mb-sm-5"></div>
</main>
@endsection