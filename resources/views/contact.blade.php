@extends('layout.master')

@section('title', 'Contact Us For Your Project Discussion')

@section('meta_description', 'Hope is not a strategy! Contact our panel of experts who believe to give the next generation technology solutions for mobile, web, and enterprise software services.')

@section('image')

<meta property="og:image" content="{{asset('public/img/favicon.png')}}" />

<meta name="twitter:image" content="{{asset('public/img/favicon.png')}}" />

@endsection





@section('content')

<main class="pb-lg-5">



    <div class="bg-primary py-5 text-secondary">

        <div class="container pb-lg-5">

            <h1 class="font-weight-bold display-3" data-aos="fade-up">Get In Touch With Us Today</h1>

            <p class="text-white" data-aos="fade-up">Looking for a reliable partner for your next project?</p>

        </div>

    </div>



    <div class="container">

        <div class="row">

            <div class="col-lg-4 py-5">

                <p class="mb-4">Hire our optimal services for all your IT needs. We believe in building long term relationships by delivering reliable, affordable and unbeatable services.</p>

                <address class="mb-2">

                    <h6 class="h4 font-weight-bold">Office</h6>

                    <p>415, 4th Floor, Maruti Plaza, Krishnanagar, Ahmedabad - 382346, India</p>

                    <ul class="list-unstyled">

                        <li class="mb-2"><i class="fas fa-envelope mr-2"></i><a href="mailto:info@multipz.com">info@multipz.com</a></li>

                       <!-- <li><i class="fas fa-phone fa-rotate-90 mr-2"></i><a href="tel:07878055055">07-87-805-5055</a></li> -->

                    </ul>

                </address>



            </div>



            <div class="col-lg-8">

                <div class="card mt-n5 shadow-lg">

                    <div class="card-body p-lg-5">

                        <div class="row">

                            <div class="d-none d-sm-block col-sm-auto">

                                <i class="fa fa-envelope fa-3x"></i>

                            </div>

                            <div class="col-sm">

                                <h6 class="h5">Write us a few words about your project and we'll get back to you within <b>24 hours</b>.</h6>

                            </div>

                        </div>

                        <hr>

                        <form action="{{Route('mail')}}" method="post" class="needs-validation" novalidate>

                            {{csrf_field()}}

                            <div class="row">

                                <div class="form-group col-md-6 mb-lg-4">

                                    <label for="name" class="small">Your Name <sup class="text-danger">*</sup></label>

                                    <input type="text" class="bg-light form-control" id="name" name="name" placeholder="Your Name" required>

                                    <div class="invalid-feedback">

                                        Please Enter Your Name.

                                    </div>

                                </div>

                                <div class="form-group col-md-6 mb-lg-4">

                                    <label for="email" class="small">Your E-mail <sup class="text-danger">*</sup></label>

                                    <input type="email" class="bg-light form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" id="email" placeholder="Your E-mail" name="email" required>

                                    <div class="invalid-feedback">

                                        Please Enter a Valid Email Address.

                                    </div>

                                </div>

                                <div class="form-group col-md-6 mb-lg-4">

                                    <label for="looking-for" class="small">Looking For <sup class="text-danger">*</sup></label>

                                    <select required class="custom-select bg-light" id="looking-for" name="selection">

                                        <option selected disabled value="">Looking for</option>

                                        

                                        @if(url()->previous() == route('mobile-app-development'))

                                            <option selected="">Mobile App Development</option>

                                            <option>Website Development</option>

                                            <option>Custom Solutions</option>

                                            <option>Artificial Intelligence</option>

                                        @elseif(url()->previous() == route('web-development')) 

                                            <option >Mobile App Development</option>

                                            <option selected="">Website Development</option>

                                            <option>Custom Solutions</option>

                                            <option>Artificial Intelligence</option>

                                        @elseif(url()->previous() == route('custom-solutions'))

                                            <option >Mobile App Development</option>

                                            <option >Website Development</option>

                                            <option selected="">Custom Solutions</option>

                                            <option>Artificial Intelligence</option>

                                        @elseif(url()->previous() == route('artificial-intelligence-development'))

                                            <option >Mobile App Development</option>

                                            <option >Website Development</option>

                                            <option>Custom Solutions</option>

                                            <option selected="">Artificial Intelligence</option>

                                        @else

                                            <option>Mobile App Development</option>

                                            <option>Website Development</option>

                                            <option>Custom Solutions</option>

                                            <option>Artificial Intelligence</option>

                                        @endif

                                    </select>

                                    <div class="invalid-feedback">

                                        Please Select a Service you looking for.

                                    </div>





                                </div>

                                <div class="form-group col-md-6 mb-lg-4">

                                    <label for="country" class="small">Country <sup class="text-danger">*</sup></label>

                                    <!-- <input type="text" class="bg-light form-control" id="country" placeholder="Country" name="country" required> -->



                                    <select required class="custom-select bg-light" id="looking-for" name="country">

                                        <option selected disabled value="">Select Country</option>

                                        <option value="Afganistan">Afghanistan</option>

                                        <option value="Albania">Albania</option>

                                        <option value="Algeria">Algeria</option>

                                        <option value="American Samoa">American Samoa</option>

                                        <option value="Andorra">Andorra</option>

                                        <option value="Angola">Angola</option>

                                        <option value="Anguilla">Anguilla</option>

                                        <option value="Antigua & Barbuda">Antigua & Barbuda</option>

                                        <option value="Argentina">Argentina</option>

                                        <option value="Armenia">Armenia</option>

                                        <option value="Aruba">Aruba</option>

                                        <option value="Australia">Australia</option>

                                        <option value="Austria">Austria</option>

                                        <option value="Azerbaijan">Azerbaijan</option>

                                        <option value="Bahamas">Bahamas</option>

                                        <option value="Bahrain">Bahrain</option>

                                        <option value="Bangladesh">Bangladesh</option>

                                        <option value="Barbados">Barbados</option>

                                        <option value="Belarus">Belarus</option>

                                        <option value="Belgium">Belgium</option>

                                        <option value="Belize">Belize</option>

                                        <option value="Benin">Benin</option>

                                        <option value="Bermuda">Bermuda</option>

                                        <option value="Bhutan">Bhutan</option>

                                        <option value="Bolivia">Bolivia</option>

                                        <option value="Bonaire">Bonaire</option>

                                        <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>

                                        <option value="Botswana">Botswana</option>

                                        <option value="Brazil">Brazil</option>

                                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>

                                        <option value="Brunei">Brunei</option>

                                        <option value="Bulgaria">Bulgaria</option>

                                        <option value="Burkina Faso">Burkina Faso</option>

                                        <option value="Burundi">Burundi</option>

                                        <option value="Cambodia">Cambodia</option>

                                        <option value="Cameroon">Cameroon</option>

                                        <option value="Canada">Canada</option>

                                        <option value="Canary Islands">Canary Islands</option>

                                        <option value="Cape Verde">Cape Verde</option>

                                        <option value="Cayman Islands">Cayman Islands</option>

                                        <option value="Central African Republic">Central African Republic</option>

                                        <option value="Chad">Chad</option>

                                        <option value="Channel Islands">Channel Islands</option>

                                        <option value="Chile">Chile</option>

                                        <option value="China">China</option>

                                        <option value="Christmas Island">Christmas Island</option>

                                        <option value="Cocos Island">Cocos Island</option>

                                        <option value="Colombia">Colombia</option>

                                        <option value="Comoros">Comoros</option>

                                        <option value="Congo">Congo</option>

                                        <option value="Cook Islands">Cook Islands</option>

                                        <option value="Costa Rica">Costa Rica</option>

                                        <option value="Cote DIvoire">Cote DIvoire</option>

                                        <option value="Croatia">Croatia</option>

                                        <option value="Cuba">Cuba</option>

                                        <option value="Curaco">Curacao</option>

                                        <option value="Cyprus">Cyprus</option>

                                        <option value="Czech Republic">Czech Republic</option>

                                        <option value="Denmark">Denmark</option>

                                        <option value="Djibouti">Djibouti</option>

                                        <option value="Dominica">Dominica</option>

                                        <option value="Dominican Republic">Dominican Republic</option>

                                        <option value="East Timor">East Timor</option>

                                        <option value="Ecuador">Ecuador</option>

                                        <option value="Egypt">Egypt</option>

                                        <option value="El Salvador">El Salvador</option>

                                        <option value="Equatorial Guinea">Equatorial Guinea</option>

                                        <option value="Eritrea">Eritrea</option>

                                        <option value="Estonia">Estonia</option>

                                        <option value="Ethiopia">Ethiopia</option>

                                        <option value="Falkland Islands">Falkland Islands</option>

                                        <option value="Faroe Islands">Faroe Islands</option>

                                        <option value="Fiji">Fiji</option>

                                        <option value="Finland">Finland</option>

                                        <option value="France">France</option>

                                        <option value="French Guiana">French Guiana</option>

                                        <option value="French Polynesia">French Polynesia</option>

                                        <option value="French Southern Ter">French Southern Ter</option>

                                        <option value="Gabon">Gabon</option>

                                        <option value="Gambia">Gambia</option>

                                        <option value="Georgia">Georgia</option>

                                        <option value="Germany">Germany</option>

                                        <option value="Ghana">Ghana</option>

                                        <option value="Gibraltar">Gibraltar</option>

                                        <option value="Great Britain">Great Britain</option>

                                        <option value="Greece">Greece</option>

                                        <option value="Greenland">Greenland</option>

                                        <option value="Grenada">Grenada</option>

                                        <option value="Guadeloupe">Guadeloupe</option>

                                        <option value="Guam">Guam</option>

                                        <option value="Guatemala">Guatemala</option>

                                        <option value="Guinea">Guinea</option>

                                        <option value="Guyana">Guyana</option>

                                        <option value="Haiti">Haiti</option>

                                        <option value="Hawaii">Hawaii</option>

                                        <option value="Honduras">Honduras</option>

                                        <option value="Hong Kong">Hong Kong</option>

                                        <option value="Hungary">Hungary</option>

                                        <option value="Iceland">Iceland</option>

                                        <option value="Indonesia">Indonesia</option>

                                        <option value="India">India</option>

                                        <option value="Iran">Iran</option>

                                        <option value="Iraq">Iraq</option>

                                        <option value="Ireland">Ireland</option>

                                        <option value="Isle of Man">Isle of Man</option>

                                        <option value="Israel">Israel</option>

                                        <option value="Italy">Italy</option>

                                        <option value="Jamaica">Jamaica</option>

                                        <option value="Japan">Japan</option>

                                        <option value="Jordan">Jordan</option>

                                        <option value="Kazakhstan">Kazakhstan</option>

                                        <option value="Kenya">Kenya</option>

                                        <option value="Kiribati">Kiribati</option>

                                        <option value="Korea North">Korea North</option>

                                        <option value="Korea Sout">Korea South</option>

                                        <option value="Kuwait">Kuwait</option>

                                        <option value="Kyrgyzstan">Kyrgyzstan</option>

                                        <option value="Laos">Laos</option>

                                        <option value="Latvia">Latvia</option>

                                        <option value="Lebanon">Lebanon</option>

                                        <option value="Lesotho">Lesotho</option>

                                        <option value="Liberia">Liberia</option>

                                        <option value="Libya">Libya</option>

                                        <option value="Liechtenstein">Liechtenstein</option>

                                        <option value="Lithuania">Lithuania</option>

                                        <option value="Luxembourg">Luxembourg</option>

                                        <option value="Macau">Macau</option>

                                        <option value="Macedonia">Macedonia</option>

                                        <option value="Madagascar">Madagascar</option>

                                        <option value="Malaysia">Malaysia</option>

                                        <option value="Malawi">Malawi</option>

                                        <option value="Maldives">Maldives</option>

                                        <option value="Mali">Mali</option>

                                        <option value="Malta">Malta</option>

                                        <option value="Marshall Islands">Marshall Islands</option>

                                        <option value="Martinique">Martinique</option>

                                        <option value="Mauritania">Mauritania</option>

                                        <option value="Mauritius">Mauritius</option>

                                        <option value="Mayotte">Mayotte</option>

                                        <option value="Mexico">Mexico</option>

                                        <option value="Midway Islands">Midway Islands</option>

                                        <option value="Moldova">Moldova</option>

                                        <option value="Monaco">Monaco</option>

                                        <option value="Mongolia">Mongolia</option>

                                        <option value="Montserrat">Montserrat</option>

                                        <option value="Morocco">Morocco</option>

                                        <option value="Mozambique">Mozambique</option>

                                        <option value="Myanmar">Myanmar</option>

                                        <option value="Nambia">Nambia</option>

                                        <option value="Nauru">Nauru</option>

                                        <option value="Nepal">Nepal</option>

                                        <option value="Netherland Antilles">Netherland Antilles</option>

                                        <option value="Netherlands">Netherlands (Holland, Europe)</option>

                                        <option value="Nevis">Nevis</option>

                                        <option value="New Caledonia">New Caledonia</option>

                                        <option value="New Zealand">New Zealand</option>

                                        <option value="Nicaragua">Nicaragua</option>

                                        <option value="Niger">Niger</option>

                                        <option value="Nigeria">Nigeria</option>

                                        <option value="Niue">Niue</option>

                                        <option value="Norfolk Island">Norfolk Island</option>

                                        <option value="Norway">Norway</option>

                                        <option value="Oman">Oman</option>

                                        <option value="Pakistan">Pakistan</option>

                                        <option value="Palau Island">Palau Island</option>

                                        <option value="Palestine">Palestine</option>

                                        <option value="Panama">Panama</option>

                                        <option value="Papua New Guinea">Papua New Guinea</option>

                                        <option value="Paraguay">Paraguay</option>

                                        <option value="Peru">Peru</option>

                                        <option value="Phillipines">Philippines</option>

                                        <option value="Pitcairn Island">Pitcairn Island</option>

                                        <option value="Poland">Poland</option>

                                        <option value="Portugal">Portugal</option>

                                        <option value="Puerto Rico">Puerto Rico</option>

                                        <option value="Qatar">Qatar</option>

                                        <option value="Republic of Montenegro">Republic of Montenegro</option>

                                        <option value="Republic of Serbia">Republic of Serbia</option>

                                        <option value="Reunion">Reunion</option>

                                        <option value="Romania">Romania</option>

                                        <option value="Russia">Russia</option>

                                        <option value="Rwanda">Rwanda</option>

                                        <option value="St Barthelemy">St Barthelemy</option>

                                        <option value="St Eustatius">St Eustatius</option>

                                        <option value="St Helena">St Helena</option>

                                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>

                                        <option value="St Lucia">St Lucia</option>

                                        <option value="St Maarten">St Maarten</option>

                                        <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>

                                        <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>

                                        <option value="Saipan">Saipan</option>

                                        <option value="Samoa">Samoa</option>

                                        <option value="Samoa American">Samoa American</option>

                                        <option value="San Marino">San Marino</option>

                                        <option value="Sao Tome & Principe">Sao Tome & Principe</option>

                                        <option value="Saudi Arabia">Saudi Arabia</option>

                                        <option value="Senegal">Senegal</option>

                                        <option value="Seychelles">Seychelles</option>

                                        <option value="Sierra Leone">Sierra Leone</option>

                                        <option value="Singapore">Singapore</option>

                                        <option value="Slovakia">Slovakia</option>

                                        <option value="Slovenia">Slovenia</option>

                                        <option value="Solomon Islands">Solomon Islands</option>

                                        <option value="Somalia">Somalia</option>

                                        <option value="South Africa">South Africa</option>

                                        <option value="Spain">Spain</option>

                                        <option value="Sri Lanka">Sri Lanka</option>

                                        <option value="Sudan">Sudan</option>

                                        <option value="Suriname">Suriname</option>

                                        <option value="Swaziland">Swaziland</option>

                                        <option value="Sweden">Sweden</option>

                                        <option value="Switzerland">Switzerland</option>

                                        <option value="Syria">Syria</option>

                                        <option value="Tahiti">Tahiti</option>

                                        <option value="Taiwan">Taiwan</option>

                                        <option value="Tajikistan">Tajikistan</option>

                                        <option value="Tanzania">Tanzania</option>

                                        <option value="Thailand">Thailand</option>

                                        <option value="Togo">Togo</option>

                                        <option value="Tokelau">Tokelau</option>

                                        <option value="Tonga">Tonga</option>

                                        <option value="Trinidad & Tobago">Trinidad & Tobago</option>

                                        <option value="Tunisia">Tunisia</option>

                                        <option value="Turkey">Turkey</option>

                                        <option value="Turkmenistan">Turkmenistan</option>

                                        <option value="Turks & Caicos Is">Turks & Caicos Is</option>

                                        <option value="Tuvalu">Tuvalu</option>

                                        <option value="Uganda">Uganda</option>

                                        <option value="United Kingdom">United Kingdom</option>

                                        <option value="Ukraine">Ukraine</option>

                                        <option value="United Arab Erimates">United Arab Emirates</option>

                                        <option value="United States of America">United States of America</option>

                                        <option value="Uraguay">Uruguay</option>

                                        <option value="Uzbekistan">Uzbekistan</option>

                                        <option value="Vanuatu">Vanuatu</option>

                                        <option value="Vatican City State">Vatican City State</option>

                                        <option value="Venezuela">Venezuela</option>

                                        <option value="Vietnam">Vietnam</option>

                                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>

                                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>

                                        <option value="Wake Island">Wake Island</option>

                                        <option value="Wallis & Futana Is">Wallis & Futana Is</option>

                                        <option value="Yemen">Yemen</option>

                                        <option value="Zaire">Zaire</option>

                                        <option value="Zambia">Zambia</option>

                                        <option value="Zimbabwe">Zimbabwe</option>

                                    </select>

                                    <div class="invalid-feedback">

                                        Please Select a Country.

                                    </div>

                                </div>

                                <div class="form-group col-12 mb-lg-4">

                                    <label for="company" class="small">Company</label>

                                    <input type="text" class="bg-light form-control" id="company" placeholder="Company" name="company">

                                </div>

                                <div class="form-group col-12 mb-lg-4">

                                    <label for="details" class="small">Project Details</label>

                                    <textarea class="bg-light form-control" id="details" row="5" placeholder="Project Details" name="projectdetails"></textarea>

                                </div>

                                <input type="hidden" class="bg-light form-control" id="year" row="5" value="" name="year">

                                <input type="hidden" class="bg-light form-control" id="greeting" row="5" value="" name="greeting">

                                <input type="hidden" class="bg-light form-control" id="indianTime" row="5" value="" name="indianTime">



                                <div class="form-group col-12 mb-lg-4">

                                    <label for="captcha" class="small">Math Captcha <sup class="text-danger">*</sup></label><br />

                                    <div class="form-row align-items-center">

                                        <div class="col-auto">

                                            <strong id="question1" class="mb-0"></strong> <strong>=</strong>

                                        </div>

                                        <div class="col-auto">

                                            <input id="ans" type="text" style="max-width: 70px;height: calc(1.5em + 0.75rem + 2px);padding: 0.375rem 0.75rem;border: 1px solid #ced4da;border-radius: 0.25rem;" required>

                                        </div>

                                        <div class="col-auto">

                                            <a type="reset" id="reset" value="reset"><i class="fas fa-sync-alt"></i></a>

                                        </div>

                                        <div class="col-auto">

                                            <div id="message" style="font-size: 14px">Please Enter a Captcha <i class="fa fa-exclamation-circle" style="color: #B8860B;" aria-hidden="true"></i></div>

                                            <div id="success" style="font-size: 14px">Verify complete <i class="fas fa-check-circle" style="color: green;"></i></div>

                                            <div id="fail" class="text-danger" style="font-size: 14px">Verify failed <i class="fas fa-exclamation-triangle" style="color: red;"></i></div>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-12">

                                    <button type="submit" value="submit" onclick="myFunction()" class="btn btn-primary btn-block btn-lg">Submit</button>

                                </div>





                                <!-- <div class="col-12">

                                    <button type="submit" onclick="myFunction()" class="btn btn-primary btn-block btn-lg">Send</button>

                                </div> -->

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>



</main>

@endsection



@section('script')

<script>

    function myFunction() {

        var d = new Date();

        var n = d.getFullYear();

        $('#year').val(n);



        var hourNow = d.getHours();

        var indian = new Date().toLocaleString("en-US", {

            timeZone: "Asia/Kolkata"

        });

        var q = new Date(indian);

        var indianHour = q.getHours();

        var indianTime;

        var greeting;



        if (hourNow >= 0 && hourNow < 6) {

            greeting = 'Very Early Morning';

        } else if (hourNow >= 6 && hourNow < 12) {

            greeting = 'Good Morning';

        } else if (hourNow >= 12 && hourNow < 18) {

            greeting = 'Good Afternoon!';

        } else {

            greeting = 'Good Evening!';

        }

        $('#greeting').val(greeting);



        if (indianHour >= 0 && indianHour < 6) {

            indianTime = 'Very Early Morning';

        } else if (indianHour >= 6 && indianHour < 12) {

            indianTime = 'Good Morning';

        } else if (indianHour >= 12 && indianHour < 18) {

            indianTime = 'Good Afternoon!';

        } else {

            indianTime = 'Good Evening!';

        }

        $('#indianTime').val(indianTime);



    }



  



    if (localStorage.getItem("clickEventName") === "webdevelopment") {

        $('#looking-for').val('Website Development');

        localStorage.clear();

    } else if (localStorage.getItem("clickEventName") === "mobiledevelopment") {

        $('#looking-for').val('Mobile App Development');

        localStorage.clear();

    }

    /*else {

        $('.pharmacist-demo, #Staff').addClass('active');

    } */







    $('button[type=submit]').attr('disabled', 'disabled');

    $('button[type=submit]').removeAttr('disabled');

    $('#message').show();

    $('#success').hide();

    $('#fail').hide();



    var randomNum1;

    var randomNum2;



    //set the largeest number to display



    var maxNum = 15;

    var total;



    randomNum1 = Math.ceil(Math.random() * maxNum);

    randomNum2 = Math.ceil(Math.random() * maxNum);

    total = randomNum1 + randomNum2;



    $("#question1").prepend(randomNum1 + " + " + randomNum2);



    // When users input the value



    $("#ans").keyup(function() {



        var input = $(this).val();

        var slideSpeed = 200;



        $('#message').hide();



        if (input == total) {

            $('button[type=submit]').removeAttr('disabled');

            $('#success').show();

            $('#fail').hide();

            



        } else {

            $('button[type=submit]').attr('disabled', 'disabled');

            $('#fail').show();

            $('#success').hide();



        }



    });



    // Wheen "reset button" click, generating new randomNum1 & randomNum2

    $("#reset").on("click", function() {

        $('#message').show();

        $('#success').hide();

        $('#fail').hide();

        randomNum1 = Math.ceil(Math.random() * maxNum);

        randomNum2 = Math.ceil(Math.random() * maxNum);

        total = randomNum1 + randomNum2;

        $("#question1").empty();

        $("#ans").val('');

        $("#question1").prepend(randomNum1 + " + " + randomNum2);

    });



    setTimeout(function() {

        $('#emailmsg').fadeOut('fast');

    }, 8000);







    (function() {

        'use strict';

        window.addEventListener('load', function() {

            // Fetch all the forms we want to apply custom Bootstrap validation styles to

            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission

            var validation = Array.prototype.filter.call(forms, function(form) {

            form.addEventListener('submit', function(event) {

                if (form.checkValidity() === false) {

                event.preventDefault();

                event.stopPropagation();

                }

                form.classList.add('was-validated');

            }, false);

            });

        }, false);

        })();

    </script>

@endsection