@extends('layout.master')

@section('title', 'Custom Web Solution | Custom Mobile App Solution Services')
@section('meta_description', 'Multipz Technology is the leading custom development services provider, offers a range of custom solutions for web development, mobile app development, web app development, and many more.')
@section('image')
<meta property="og:image" content="{{asset('public/img/custom-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/custom-bg.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="position-relative overflow-hidden">
        <img src="{{asset('public/img/custom-bg.png')}}" alt="Custom Development" class="img-absolute-50 pl-lg-5 right mb-3 mb-lg-0" loading="eager">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-xl-6">
                    <h1 class="font-weight-bold display-4 mb-lg-4" data-aos="fade-up" data-aos-delay="600">Custom Solutions</h1>
                    <div class="font-weight-bold h3  mb-lg-4" data-aos="fade-up" data-aos-delay="800">
                        <span class="letter-space-2 title-strike">A destination to find your personalized solution</span>
                    </div>
                    <p data-aos="fade-up" data-aos-delay="1200">Stand out from the crowd with our custom development service. We have spent years building the architecture, processes, and infrastructure to prepare ourselves to take up any task coming our way. We don’t want to wait for a routine job when an unknown and challenging task can fill each day with thrill.</p>
                    <p data-aos="fade-up" data-aos-delay="1200">We follow an agile method to meet our client’s goals and expectations. Our team of digital warriors helps to bring something extraordinary in each project. No matter the size of your business, you will offer the best service experience at your budget price.</p>
                </div>
            </div>
        </div>
        <img src="{{asset('public/img/bg-element-03.png')}}" alt="" style="bottom: -25%;max-width: 250px;left: 0;" class="d-none d-lg-block position-absolute img-fluid banner-ele-3 animate-rotate slow" data-aos="fade" data-aos-delay="1000" data-aos-offset="-100">
    </div>

    <div class="bg-primary text-white">
        <div class="container">
            <div class="swiper-container custom-dev-button">
                <div class="swiper-wrapper my-3 my-lg-4">
                    <div class="swiper-slide">
                      <h3>  <div class="btn-block btn btn-secondary">Application Integration</div></h3> 
                    </div>
                    <div class="swiper-slide">
                    <h3>   <div class="btn-block btn btn-secondary">Automation</div></h3> 
                    </div>
                    <div class="swiper-slide">
                    <h3>  <div class="btn-block btn btn-secondary">Web application</div></h3> 
                    </div>
                    <div class="swiper-slide">
                    <h3>    <div class="btn-block btn btn-secondary">Mobile App Development</div></h3> 
                    </div>
                    <div class="swiper-slide">
                    <h3>  <div class="btn-block btn btn-secondary">E-Commerce</div></h3> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="swiper-container custom-dev-detail">
            <div class="swiper-wrapper py-3 py-lg-5">
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-3 bg-white">
                            <img src="{{asset('public/img/application-integration.png')}}" alt="application-integration" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <div class="h2">Application Integration</div>
                            <p>Hire our service for application integration to drive business and stay updated with the upcoming technologies. You can also hire us to create API to build new applications, to integrate your app with an online service, for an end to end integration and much more. We are open for all kinds of application integration services.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-3 bg-white">
                            <img src="{{asset('public/img/automation.png')}}" alt="automation" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <div class="h2">Automation</div>
                            <p>We are ready to take up the automation challenge to provide you with a seamless solution. From installation, performance, manufacturing to implementation, we’ll take care of everything. We are ready for your automation project, are you?</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-3 bg-white">
                            <img src="{{asset('public/img/mobile-application.png')}}" alt="mobile-application" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <div class="h2">Web application</div>
                            <p>We believe that one size does not fit all and that is why we offer customized web application service. You can hire us for your end-to-end custom web development to automate your business challenges. We’ll guide you in every step from conception, features planning, development process, quality assurance till going live.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-3 bg-white">
                            <img src="{{asset('public/img/e-commerce.png')}}" alt="e-commerce" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <div class="h2">Mobile App Development</div>
                            <p>Speed up your growth with mobile application development. We provide customized, highly user-friendly, and yet effective <b>custom mobile app development services</b>. Our confidence is backed by our powerful team, who have years of experience in delivering the best mobile apps on different platforms. </p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-3 bg-white">
                            <img src="{{asset('public/img/application-takeover.png')}}" alt="application-takeover" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <div class="h2">eCommerce</div>
                            <p>Create an edge over your competitor with our eCommerce service. We are here to transform your eCommerce market using advanced features, various analytics, personalized functioning, and lots more. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="py-3 py-lg-5" style="background: -moz-linear-gradient(45deg,  #eeeeee 0%, #eeeeee 60%, #ffffff 60%, #ffffff 100%);background: -webkit-linear-gradient(45deg,  #eeeeee 0%,#eeeeee 60%,#ffffff 60%,#ffffff 100%);background: linear-gradient(45deg,  #eeeeee 0%,#eeeeee 60%,#ffffff 60%,#ffffff 100%);">
            <div class="container">
                <div class="h1 text-center font-weight-bold mb-3 mb-lg-5" data-aos="fade-up" data-aos-delay="300">Why us</div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 px-lg-4 mb-3 mb-md-0 pb-md-4">
                        <object data="{{asset('public/img/svg/increase.svg')}}" type="image/svg+xml" style="position: absolute; opacity: 0.08; right: 10px; top: 0; width: 80px;">
                            
                        </object>
                        <div class="font-weight-boldmb-3 h5">Experience</div>
                        <p>Multipz Technology has years of experience in delivering the most innovative solution to always stay ahead. We are supported by an amazing super geek team of backend and frontend who are always ready to walk the extra mile for clients. </p>
                    </div>
                    <div class="col-sm-6 col-md-6 px-lg-4 mb-3 mb-md-0 pb-md-4 border-left-dash">
                        <object data="{{asset('public/img/svg/speech-bubble.svg')}}" type="image/svg+xml" style="position: absolute; opacity: 0.08; right: 10px; top: 0; width: 80px;">
                           
                        </object>
                        <div class="font-weight-boldmb-3 h5">Clear communication</div>
                        <p>We believe in strengthening relationships and that is why we keep things clear with our effective communication. The assigned project manager will keep the client updated about the ongoing progress.</p>
                    </div>
                    <div class="col-sm-6 col-md-6 px-lg-4 mb-3 mb-md-0 pb-md-4">
                        <object data="{{asset('public/img/svg/aim.svg')}}" type="image/svg+xml" style="position: absolute; opacity: 0.08; right: 10px; top: 0; width: 80px;">
                           
                        </object>
                        <div class="font-weight-boldmb-3 h5">Quality focus​</div>
                        <p>Quality is the essence of our service. Despite tight deadlines, we never lose our focus on quality. From discussion to the project delivery, we assure you to provide quality service.</p>
                    </div>
                    <div class="col-sm-6 col-md-6 px-lg-4 mb-3 mb-md-0 border-left-dash">
                        <object data="{{asset('public/img/svg/truck.svg')}}" type="image/svg+xml" style="position: absolute; opacity: 0.08; right: 10px; top: 0; width: 80px;">
                        
                        </object>
                        <div class="font-weight-boldmb-3 h5">Delivery​</div>
                        <p>We don’t make fake promises and we never lie about the delivery and that is what makes us different from others. We aim to deliver projects on time without making any compromises on quality. </p>
                    </div>
                </div>
            </div>
        </section>

        <div class="bg-primary text-white py-3 py-lg-5">
            <div class="container text-center">
                <div class="h1">Give wings to your <b class="text-accent">IDEAS</b>.<br> We love to discuss real business</div>
                <a href="{{Route('contact')}}" class="btn btn-accent btn-lg px-lg-5 mt-lg-3 py-2 py-lg-3 font-weight-bold font-size-16">Let's Talk</a>
            </div>
        </div>
         
        <section class="py-3 py-lg-5">
            <div class="container">
                <h6 class="display-3 text-center text-uppercase font-weight-bold" style="opacity: 0.1;" data-aos="fade-up" data-aos-delay="300">technology</h6>
                <h4 class="h1 text-center font-weight-bold mt-n5" data-aos="fade-up" data-aos-delay="100">Tools and Tech Stack</h4>
                <div class="row mt-3 mt-xl-5 justify-content-center align-items-center tools-tech">
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-Html mb-2 tool-tilt mx-auto"></div>
                        <p>HTML</p>
                    </div>
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-CSS mb-2 tool-tilt mx-auto"></div>
                        <p>CSS</p>
                    </div>
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-php mb-2 tool-tilt mx-auto"></div>
                        <p>PHP</p>
                    </div>
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-Yii mb-2 tool-tilt mx-auto"></div>
                        <p>Yii</p>
                    </div>
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-Codeignitor mb-2 tool-tilt mx-auto"></div>
                        <p>Codeignitor</p>
                    </div>
                    <div class="col-6 col-sm-3 col-lg-2 text-center mb-2 mb-lg-5 d-flex flex-column">
                        <div class="bg-Laravel mb-2 tool-tilt mx-auto"></div>
                        <p>Laravel</p>
                    </div>
                </div>
            </div>
        </section>

    <section class="py-3 py-lg-5 bg-primary text-white mb-lg-5">
        <div class="container">
            <div class="h1 font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="300">FAQs</div>
            <div class="accordion" id="faq">
                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq1" class="text-white d-block h4 pr-4" data-toggle="collapse" aria-expanded="false" aria-controls="faq1">
                        How much does the custom solution cost?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq1" class="collapse show" data-parent="#faq">
                        <p class="text-white py-3">
                            It is very difficult to say random costs without knowing the actual need of a client. You can drop all your requirements and we’ll get back to you with an estimated cost.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq2" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq2">
                        How do you keep me informed about the progress?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq2" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                            Once on discussing your requirement, we will assign you a project manager who will keep you updated about the ongoing progress.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq3" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq3">
                        Should I be familiar with technical details to work with you?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq3" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                            Absolutely not, we have clients with no technical background for <b>custom web application development services</b>, but we are ready to explain to you all unclear doubts.
                        </p>
                    </div>
                </div>

                <div class="border-bottom mb-4 pb-3" data-aos="fade-up" data-aos-delay="400">
                    <a href="#faq4" class="text-white d-block h4 pr-4 collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="faq4">
                        Will you sign NDA?
                        <span class="accordion-icon"><i class="fas fa-plus font-size-20"></i></span>
                    </a>
                    <div id="faq4" class="collapse" data-parent="#faq">
                        <p class="text-white py-3">
                            We give a choice to our client for the NDA policy.
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="mb-5"></div>
    </section>
</main>

@endsection

@section('script')
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity": [{
            "@type": "Question",
            "name": "How much does the custom solution cost?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "It is very difficult to say random costs without knowing the actual need of a client. You can drop all your requirements and we’ll get back to you with an estimated cost."
            }
        }, {
            "@type": "Question",
            "name": "How do you keep me informed about the progress?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Once on discussing your requirement, we will assign you a project manager who will keep you updated about the ongoing progress."
            }
        }, {
            "@type": "Question",
            "name": "Should I be familiar with technical details to work with you?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Absolutely not, we have clients with no technical background for custom web application development services, but we are ready to explain to you all unclear doubts."
            }
        }, {
            "@type": "Question",
            "name": "Will you sign NDA?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We give a choice to our client for the NDA policy."
            }
        }]
    }
</script>
@endsection