@extends('layout.master')
@section('title', 'Holdeed: Administrative Software Application For Web, Android & iOS')
@section('meta_description', 'Holdeed is an administrative software exclusively created to simplify project management, order processing, time management, leave management, and more.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="position-relative overflow-hidden">
        <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-4 col-sm-auto">
                        <img src="{{asset('public/img/portfolio/holdeed/logo.png')}}" alt="holdeed" class="img-fluid">
                    </div>
                </div>
            </div>
        </div> 
    </div>       
    
    <section class="py-3 py-md-5">
        <div class="container py-xl-5">
           <h1> <h2 class="display-3 text-uppercase font-weight-bold text-center">Holdeed</h2></h1>
            <p>Holdeed is an administrative software that simplifies project management, order processing, time management, leave management, and construction documentation tasks. This platform unifies the needs of SMEs, construction companies, and helps them in saving hard earned money. It connects directly to employees, even when they are working outside the office. </p>
            <div class="row mt-lg-3">
                <div class="col-12">
                    <h3 class="h4 font-weight-bold mb-3">Technology Stack</h3>
                </div>
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>iOS: Swift</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Android: Java</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>backend : MySQL, Yii PHP Framework</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model : B2B</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language option : Multi-language (English & German)</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-6 col-sm-auto mb-2 mb-md-0">
                    <a href="https://play.google.com/store/apps/details?id=com.applie.holdeed&hl=en_IN" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                <div class="col-6 col-sm-auto">
                    <a href="https://apps.apple.com/br/app/holdeed/id1469720951?l=en" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <img src="{{asset('public/img/portfolio/holdeed/main.jpg')}}" alt="Administrative Software" class="w-100">


        <div class="py-3 py-lg-5 position-relative overflow-hidden">
            <img src="{{asset('public/img/portfolio/holdeed/about.png')}}" alt="Administrative Mobile App" class="img-absolute-50 right mb-3 mb-lg-0">
            <div class="container py-xl-5">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="display-4 font-weight-bold mb-4">About Client</h2>
                        <p>The client was a former employee of a construction company in Switzerland. He was unhappy with the loopholes in the construction business. These loopholes were costing millions of dollars to business and valuable time. </p>

                        <h3 class="display-4 font-weight-bold mt-lg-5 mb-4">Challenges before the app</h3> 
                        <ul class="pl-0 list-unstyled line-height-lg mb-0">
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Lack of proper project & task planning</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Mismanagement</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No leave management</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Manual work report </li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No organized work</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Employee Timesheet Report</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Bill Generate Report according to job-wise</li>
                        </ul>
                    </div>
                </div>
            </div>
            <img src="{{asset('public/img/portfolio/holdeed/bg-shape.png')}}" alt="BG-Shape" class="img-fluid d-none d-xl-block">

            <div class="container py-3 py-lg-5">
                <h3 class="display-4 font-weight-bold">What are the client’s requirements?</h3> 
                <p>The need of the client was to make the best use of technology for project creation, leave management, and overall smooth functioning of projects. He was expecting software where it should be easy to handle for everyone without compromising on navigation. He wanted software that can handle orders, time, documents, work reports, and other functionalities.</p>

                <h3 class="display-4 font-weight-bold">Strategy</h3> 
                <p>After discussing the needs of the client, our team of experts prepares a software plan to accomplish the goal. We want to cover different aspects of a business, and that is why we want to make it useful for employees and employers. The aim was to create software that can fulfill multiple tasks with easy to handle features.</p>
            </div>
        </div>


        <div class="py-3 py-lg-5 position-relative overflow-hidden">
            <img src="{{asset('public/img/portfolio/holdeed/challanges.png')}}" alt="Administrative Mobile Application" class="img-absolute-50 left mb-3 mb-lg-0 pl-lg-5">
            <div class="container py-xl-5">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <h3> <h4 class="display-4 font-weight-bold mb-4">Challenges with the app </h4> </h3>
                        <ul class="pl-0 list-unstyled line-height-lg mb-0">
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>It was difficult to maintain data for each individual.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>The major challenge during the development phase was the logic part.</li>
                        </ul>

                        <h3> <h4 class="display-4 font-weight-bold mt-lg-5 mb-4">Salient features of the app</h4> </h3>
                        <p>The client, a former employee of a construction company in Switzerland. He was unhappy with the loopholes in the construction business. These loopholes were costing millions of dollars to business and valuable time. </p>
                        <ul class="pl-0 list-unstyled line-height-lg mb-0">
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to create a task or tasks.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to document every task irrespective of its size.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>The field staff can record working hours, work reports, create reports, etc.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Enable direct communication with each other.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Top management can control the entire project and organization through the app.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Offer tight security of confidential documents and data.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to raise a ticket through the ticket system.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Help option: chat, webinar.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Piglet Deed (guider) through audio and text.</li>
                            <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy extraction of monthly data with a click.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="py-3 py-lg-5 bg-mobile-none" style="background: url('public/img/portfolio/simpilli/BG-last.png')no-repeat center / cover;">
            <div class="container">
                <h2> <h4 class="h1 font-weight-bold">Before and after app scenario</h4></h2>
                <p>Before the app, project managers have to do a large part of the paperwork, which leads to work pressure and mismanagement.</p>
                <p>However, after the app, all the roadblocks were removed. Now, it becomes easy to manage projects, employee’s attendance, time tracking, and overall the entire process becomes smoother. It was a win-win situation for employers and employees. It becomes easy for employers to keep track of everything with few clicks and employees can also enjoy features like the freedom to apply leave, track working hours, and other countless benefits. It is a comprehensive administrative software for fulfilling the requirements of different roles of an organization. </p>
            </div>
        </div>

        <div class="work-nav container-fluid py-5">
            <div class="row py-md-5 justify-content-around">
                <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                    <a href="{{Route('presenta')}}">
                        <div class="h1 mb-0">Presenta</div>
                        <span class="text-black-50">Prev Project</span>
                    </a>
                </div>
                <div class="col-sm-auto text-center text-sm-right">
                    <a href="{{Route('gpbo')}}">
                        <div class="h1 mb-0">GPBO</div>
                        <span class="text-black-50">Next Project</span>
                    </a>
                </div>
            </div>
            <div class="list">
                <a href="{{Route('work')}}">
                    <i class="fas fa-th fa-3x"></i>
                </a>
            </div>
        </div>

        <div class="mb-sm-5"></div>
</main>
@endsection

@section('script')
<script>
    $('#count-locations').jQuerySimpleCounter({
        start: 0,
        end: 4,
        duration: 5000
    });
    $('#count-projects').jQuerySimpleCounter({
        start: 0,
        end: 250,
        duration: 20000
    });
    $('#count-years').jQuerySimpleCounter({
        start: 0,
        end: 7,
        duration: 10000
    });
    $('#count-employees').jQuerySimpleCounter({
        start: 0,
        end: 50,
        duration: 10000
    });
</script>
@endsection