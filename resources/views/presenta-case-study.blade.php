@extends('layout.master')

@section('title', 'Presenta: Best Android and iOS Application For Accounting Solution')
@section('meta_description', 'Multipz Technology has developed Presenta, an innovative application for an accounting solution with the ultimate features of uploading, verifying, and saving documents using AI.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio/presenta/logo.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio/presenta/logo.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
<div class="position-relative overflow-hidden">
    <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
        <div class="container-fluid">
            <div class="row justify-content-center">
            <div class="col-4 col-sm-auto">
                    <img src="{{asset('public/img/portfolio/presenta/logo.png')}}" alt="presenta" class="img-fluid">
                </div>
            </div>
        </div>
    </div>  
</div>      
    
    <section class="py-3 py-md-5">
        <div class="container py-xl-5">
           <h1> <h2 class="display-3 text-uppercase font-weight-bold text-center">presenta</h2> </h1>
            <p>Presenta is an AI-enabled accounting solution for Android and iOS platforms. With the help of this application, it becomes easy to do some tough accounting, taxation, and payroll tasks. We have built an app that helps our client to get financial reports with a few clicks. This app not only reduces human effort but also saves valuable time.</p>
            <div class="row mt-lg-3">
                <div class="col-12">
                    <h3 class="h4 font-weight-bold mb-3">Technology Stack</h3>
                </div>
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>iOS: Swift</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Android: Java</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Backend: MySQL, Laravel</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>AI: Scala</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Chat: Node (Socket)</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model: B2B</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language option: English and German</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-6 col-sm-auto mb-2 mb-md-0">
                    <a href="https://play.google.com/store/apps/details?id=com.presenta.app" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                 <div class="col-auto">
                    <a href="https://apps.apple.com/us/app/presenta/id1536569136" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="py-5 bg-tablet-none" style="background: #AF9B64 url('public/img/portfolio/presenta/detail-01.png')no-repeat center / cover;">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <h2 class="h1 font-weight-bold" style="color: #003333;">Our strategy</h2>
                    <p class="text-white h5">After discussing the client's requirements and expectations, our team comes up with a blueprint of software and application. Multipz Technology wants software not just to be limited to a particular problem of this client, but wishes to solve the biggest problem of the accounting sector. This idea brings AI cards on the table. We developed a software where all necessary documents can get uploaded and AI can tally and save it. It also facilitates generating quarterly reports with just a click.</p>
                </div>
            </div>
            <div class="d-none d-lg-block" style="height:600px;"></div>
        </div>
    </div>

    <div class="pb-3 pb-lg-5 mt-lg-3" style="background-color: #003333;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="pt-lg-5 h1 font-weight-bold" style="-webkit-text-stroke-color: #fff; -webkit-text-fill-color: transparent; -webkit-text-stroke-width: 1px;">About Client</h2>
                        <p class="text-white h5">Presenta AG is a leading name in Liechtenstein for taxes, auditing, and accounting services. It helps private individuals and firms in providing a suitable strategy and planning in accounting. It is an over 30 years old company and one of the reliable names in accounting.</p>
                    </div>
                    <div class="col-lg-6">
                        <img src="{{asset('public/img/portfolio/presenta/expense.png')}}" alt="" class="img-fluid mt-lg-n5">
                    </div>
                </div>
            </div>
        </div>

    <div class="py-3 pt-lg-5 bg-mobile-none presenta-client-req" style="background: #fff url('public/img/portfolio/presenta/shape.png')no-repeat right bottom / contain;">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                   <h3> <div class="h1 font-weight-bold">What was the client's requirement?</div></h3>
                    <p>The need of the client was to make the use of technology for a better and reliable accounting process. They want a solution that can reduce human effort by uploading, verifying, and saving documents. Overall, the requirement was to have a convenient and secured app that maximizes revenues and opportunities.</p>

                    <h3> <div class="mt-lg-5 h1 font-weight-bold">Challenges before the app</div></h3>
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>High work pressure</li>
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>Unsatisfied clients</li>
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>Tons of paperwork</li>
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>Less productivity</li>
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>The chances of error were high</li>
                        <li><i class="fa fa-dot-circle mr-2 text-black"></i>Mismanagement resulted in a reduced number of clients</li>
                    </ul>

                    <div class="text-center py-3 py-lg-5 d-none d-md-block">
                        <img src="{{asset('public/img/portfolio/presenta/dash-line.png')}}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>
    </div>

    <div class="py-3 pb-lg-5" style="background-color: #EDEDED;">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card shadow-lg mt-n5" style="background-color: #AF9B64; border-radius: 1rem;">
                        <div class="card-body text-white">
                           <h3> <h4 class="h1 font-weight-bold">Challenges with the project</h4> </h3>
                            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                                <li><i class="fa fa-dot-circle mr-2 text-white"></i>No such app exists, so there was no reference</li>
                                <li><i class="fa fa-dot-circle mr-2 text-white"></i>Automatch of bank receipt was a big hurdle</li>
                                <li><i class="fa fa-dot-circle mr-2 text-white"></i>Setting and implementing AI algorithms</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-none d-md-block">
                    <img src="{{asset('public/img/portfolio/presenta/phone.png')}}" alt="Presenta application" class="img-fluid presenta-mobile thumbnail">
                </div>
            </div>
        </div>
    </div>

    <div class="py-4 py-lg-5" style="background: #003333 url('public/img/portfolio/presenta/bg.png')no-repeat right center / cover;">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-5 col-md-6 text-lg-right pl-lg-5">
                    <img src="{{asset('public/img/portfolio/presenta/Device-2.png')}}" alt="Presenta app" class="img-fluid">
                </div>
                <div class="col-md-6 text-white">
                   <h3> <h4 class="h1 font-weight-bold">Salient features of the app</h4></h3>
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>Reduces the human efforts with AI</li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>AI fetch data from multiple documents </li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>Facilitate auto match of documents</li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>Chat option to connect with the representative of the company</li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>An option to choose quarter or annual base services</li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>Each client gets an assigned individual representative</li>
                        <li><i class="fa fa-dot-circle mr-2" style="color: #AF9B64;"></i>Service option like taxation, auditing, payroll are available </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="py-3 py-lg-5 bg-mobile-none" style="background: #ededed url('public/img/portfolio/presenta/BG-last.png')no-repeat right top / cover;">
        <div class="container">
            <h2 class="h1 font-weight-bold">Before and after app scenario</h2>
            <p>Before the app, Presenta executives used most of their valuable time on doing manual entry of each bill and bank details. Any error on entries adds additional hours on the revision and thus leads to more time consumption and work pressure. Further, multiple clients add up more performance pressure on executives.</p>
            <p>However, after the app, the work of executives became easy as they just have to verify the uploaded documents. It enables them to give more time in planning and proposing clients with many other financial benefits, which were not a usual case before.</p>
            <p>The chat option in the app bridges the communication gap between client and executive, and further, it helps in strengthening the relationship. Multiple filter options give the freedom to sort out the required documents without shuffling tons of other documents.</p>
            <p>Presenta app helps in saving a lot of time and effort, further leading to minimal chances of errors.</p>
        </div>
    </div>
    <div class="work-nav container-fluid py-5">
        <div class="row py-md-5 justify-content-around">
            <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                <a href="{{Route('callhippo')}}">
                    <div class="h1 mb-0">Call Hippo</div>
                    <span class="text-black-50">Prev Project</span>
                </a>
            </div>
            <div class="col-sm-auto text-center text-sm-right">
                <a href="{{Route('holdeed')}}">
                    <div class="h1 mb-0">Holdeed</div>
                    <span class="text-black-50">Next Project</span>
                </a>
            </div>
        </div>
        <div class="list">
            <a href="{{Route('work')}}">
                <i class="fas fa-th fa-3x"></i>
            </a>
        </div>
    </div>
    <div class="mb-sm-5"></div>
</main>
@endsection

@section('script')
    
<script>
    $('#count-locations').jQuerySimpleCounter({ start: 0, end: 4, duration: 5000 });
    $('#count-projects').jQuerySimpleCounter({ start: 0, end: 250, duration: 20000 });
    $('#count-years').jQuerySimpleCounter({ start: 0, end: 7, duration: 10000 });
    $('#count-employees').jQuerySimpleCounter({ start: 0, end: 50, duration: 10000 });
</script>
@endsection
