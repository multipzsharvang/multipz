@extends('layout.master')
@section('title', 'GPBO: A Community App For Android & iOS')
@section('meta_description', 'Multipz Technology has developed GPBO (Gujarat Patidar Business Organization). An app made for the community to grab the business opportunities.')
@section('image')
<meta property="og:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/portfolio-banner-bg.png')}}" />
@endsection


@section('content')
<main class="pb-lg-5">
    <div class="position-relative overflow-hidden">
        <div class="py-4 py-lg-5" style="background: url('public/img/portfolio-banner-bg.png') no-repeat center / cover;">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-4 col-sm-auto">
                        <img src="{{asset('public/img/portfolio/gpbo/logo.png')}}" alt="GPBO" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <section class="py-3 py-md-5">
        <div class="container py-xl-5">
            <h1><h2 class="display-3 text-uppercase font-weight-bold">Global Patidar <br class="d-none d-lg-block">Business
                Organization </h2> </h1>
            <p>A vision to strengthen the business network within the community</p>
            <p>Gujarat Patidar Business Organization a. k. a GPBO is an Android and iOS based app developed to connect the dots between the business community of the Patidar Samaj. It is a platform where business opportunities providers and seekers can connect. </p>
            <div class="row mt-lg-3">
                <div class="col-12">
                    <h3 class="h4 font-weight-bold mb-3">Technology Stack</h3>
                </div>
                <div class="col-md-6 col-lg-5">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>iOS: Swift</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Android: Java</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Backend: MySQL, Laravel</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-7">
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Model : B2B</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Language Option : English</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center pt-3">
                <div class="col-6 col-sm-auto mb-2 mb-md-0">
                    <a href="https://play.google.com/store/apps/details?id=com.multipz.gpbo&amp;hl=en_IN" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/play-store.png" alt="Play Store" class="img-fluid">
                    </a>
                </div>
                <div class="col-6 col-sm-auto">
                    <a href="https://apps.apple.com/gb/app/gpbo-network/id1506168589" target="_blank" rel="noopener noreferrer">
                        <img src="./public/img/app-store.png" alt="App Store" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </section>



    <div class="py-3 py-lg-5 bg-tablet-none mb-lg-5" style="background: url('public/img/portfolio/gpbo/about.png')no-repeat left top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <h2 class="display-4 font-weight-bold">About client</h2> 
                    <p class="h5">Gujarat Sardardham Vishwa Patidar Kendra was established to connect the Patel business community for its overall upliftment. Its purpose is to bring 1,00,000 industrialists together by 2026.</p>
                </div>
            </div>
        </div>
        <div class="d-none d-lg-block" style="height:700px;"></div>
    </div>

    <div class="mt-lg-n5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="display-4 mb-3 font-weight-bold">Challenges<br class="d-none d-lg-block"> before the app</h3> 
                    <ul class="pl-0 list-unstyled line-height-lg mb-0">
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No records of meetings and presentees</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No centralized system or source for data</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Poor decision making and no clarity on future plan</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Business reference was through word of mouth</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No enormous network to share business</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Unawareness about the available business within the community</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No records business flow</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Lack of engagement</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Lack of awareness of government job opportunities</li>
                        <li><i class="fa fa-dot-circle mr-2 text-accent"></i>No connection between rural and urban business opportunities</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="py-3">
        <img src="{{asset('public/img/portfolio/gpbo/requirement.png')}}" alt="GPBO Mobile Application" class="img-fluid mx-auto d-block thumbnail">
        <div class="container">
          <h3> <h4 class="display-4 font-weight-bold mt-n5 mb-4">Client’s requirements</h4></h3>
            <p>The client came to us for a digital solution to meet their goal of generating internal business opportunities. A platform where business providers can post the requirements and opportunity seekers can make the best use of it. The client also wishes to get every member’s activity report with a single touch. </p>

           <h2> <h4 class="display-4 font-weight-bold mt-md-5 mb-4">How did the process get started?</h4></h2>
           <ol>
            <li><P>Our team of mobile app development studied the whole requirement and problems faced by the client.</p></li>
            <li><p>The expert developer prepared a plan for the entire application architecture.</p></li>
            <li><p>After several attempts and discussions with the client, app designers prepared a wireframe, and details about navigation, diagrams, data flows were cited.</p></li>
            <li><p>Once the client approved the plan, the execution was on the floor.</p></li>
           </ol>
        </div>
    </div>


    <div class="py-3 bg-mobile-none"
        style="background: url('public/img/portfolio/gpbo/salient.png')no-repeat right center / contain;">
        <div class="container">
           <h3> <h4 class="display-4 font-weight-bold"> Salient features of App</h4> </h3>
            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Easy to post the requirement</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Seamless and quick navigation</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Meeting features with area and registration name details</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>A platform to advertise about the business</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>A platform to find the right talent for your job requirement</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Community events details </li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Great UX/UI</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Separate registration for members and guests</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Ease to create a world-class business profile </li>
            </ul>
        </div>
    </div>

    <div class="py-3 bg-tablet-none"
        style="background: url('public/img/portfolio/gpbo/waves.png')no-repeat 80% center / contain;">
        <div class="container">
            <h2><h4 class="display-4 font-weight-bold">Company statistics</h4></h2>
            <ul class="pl-0 list-unstyled line-height-lg mb-0">
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Wings: whole community is divided into 11 wings according to geograpical areas</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Total members: 518+</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>Total guests: 374</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>A single wing received 5 crores of business</li>
                <li><i class="fa fa-dot-circle mr-2 text-accent"></i>5000+ business reference were shared </li>
            </ul>
        </div>
    </div>

    <div class="py-3 py-lg-5">
        <div class="container">
           <h3> <h4 class="h1 font-weight-bold">Scenario after the App</h4> </h3>
            <p>GPBO app helped to bring more enlightenment about the business opportunities within the Patel Samaj. The app acted as a mediator between opportunities providers and seekers, which has further strengthened the business community. Since all the statistics get reflected on the app, it boosts the morale of other community members to give business opportunities to each other. The user-friendly and engaging app is perfect to avail of the best business and employment opportunities. Apart from that, the meeting feature records the manual meeting with details like area and name. </p>
        </div>
    </div>

    <div class="work-nav container-fluid py-5">
        <div class="row py-md-5 justify-content-around">
            <div class="col-sm-auto text-center text-sm-left mb-3 mb-sm-0">
                <a href="{{Route('holdeed')}}">
                    <div class="h1 mb-0">Holdeed</div>
                    <span class="text-black-50">Prev Project</span>
                </a>
            </div>
            <div class="col-sm-auto text-center text-sm-right">
                <a href="{{Route('joykick')}}">
                    <div class="h1 mb-0">Joykicks</div>
                    <span class="text-black-50">Next Project</span>
                </a>
            </div>
        </div>
        <div class="list">
            <a href="{{Route('work')}}">
                <i class="fas fa-th fa-3x"></i>
            </a>
        </div>
    </div>

    <div class="mb-sm-5"></div>
</main>

@endsection


@section('script')
<script>
    $('#count-locations').jQuerySimpleCounter({ start: 0, end: 4, duration: 5000 });
    $('#count-projects').jQuerySimpleCounter({ start: 0, end: 250, duration: 20000 });
    $('#count-years').jQuerySimpleCounter({ start: 0, end: 7, duration: 10000 });
    $('#count-employees').jQuerySimpleCounter({ start: 0, end: 50, duration: 10000 });
</script>
@endsection