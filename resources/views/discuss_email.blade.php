
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="x-apple-disable-message-reformatting">
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">

	<style>
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			background: #f1f1f1;
		}

		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		/* What it does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin: 0 !important;
		}

		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
		}

		/* What it does: Fixes webkit padding issue. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;
		}

		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode: bicubic;
		}

		/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
		a {
			text-decoration: none;
		}

		/* What it does: A work-around for email clients meddling in triggered links. */
		*[x-apple-data-detectors],
		/* iOS */
		.unstyle-auto-detected-links *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}

		/* What it does: Prevents Gmail from changing the text color in conversation threads. */
		.im {
			color: inherit !important;
		}

		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img+div {
			display: none !important;
		}

		.table-border td,
		.table-border th{ border: 1px solid #cccccc;padding: 8px;}

		/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
		/* Create one of these media queries for each additional viewport size you'd like to fix */

		/* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
		@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
			u~div .email-container {
				min-width: 320px !important;
			}
		}

		/* iPhone 6, 6S, 7, 8, and X */
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
			u~div .email-container {
				min-width: 375px !important;
			}
		}

		/* iPhone 6+, 7+, and 8+ */
		@media only screen and (min-device-width: 414px) {
			u~div .email-container {
				min-width: 414px !important;
			}
		}
	</style>


	<style>
		h1, h2, h3, h4, h5, h6 {
			font-family: 'Playfair Display', serif;
			color: #000000;
			margin-top: 0;
		}

		body {
			font-family: 'Montserrat', sans-serif;
			font-weight: 400;
			font-size: 15px;
			line-height: 1.8;
			color: rgba(0, 0, 0, .4);
		}

		/*LOGO*/

		.logo h1 {
			margin: 0;
		}

		.logo h1 a {
			color: #000;
			font-size: 20px;
			font-weight: 700;
			text-transform: uppercase;
			font-family: 'Montserrat', sans-serif;
		}
	</style>
</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color:#F1F1F1;">
	<center style="width: 100%; background-color: #f1f1f1;margin: 5rem auto;">
		<div style="max-width: 600px; margin: 0 auto;" class="email-container">

			<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
				style="margin: auto;" bgcolor="#fff">
				<tr>
					<td class="bg_white logo" style="padding: 1em 2.5em; text-align: center">
						<h1><img class="img-fluid logo-dark"  src="{{$message->embed(asset('public/img/loog.png'))}}" alt="multipz"></h1>
					</td>
				</tr>
				<tr>
					<td valign="middle" bgcolor="#fff">
						<table>
							<tr>
								<td>
									<div class="text" style="padding: 0 3em; text-align: center;">
										<h2>Hello Admin,</h2>
										<h2>{{$indianTime}}</h2>
										<p>You received an email from : <b>{{ $name }}</b></p>
										<p>Here are the details:</p>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="middle" bgcolor="#fff">
						<table style="text-align: left;" width="95%" class="table-border">
							<tr>
								<th>Name:</th>
								<td>{{ $name }}</td>
							</tr>
							<tr>
								<th>Email:</th>
								<td>{{ $email }}</td>
							</tr>
							<tr>
								<th>Phone No.:</th>
								<td>{{ $phone }}</td>
							</tr>
							<tr>
								<th>Country:</th>
								<td>{{ $country }}</td>
							</tr>
							
							<tr>
								<th>Message:</th>
								<td>{{ $messagess }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> <td height="10"></td> </tr>
				<tr>
					<td align="center"><em><b>Thank You</b></em></td>
				</tr>
				<tr> <td height="10"></td> </tr>
			</table>
		</div>
	</center>
</body>

</html>