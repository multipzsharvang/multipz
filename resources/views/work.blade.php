@extends('layout.master')

@section('title', 'Work Portfolio Of Mobile And Web')
@section('meta_description', 'Multipz Technology worked on simple to complex projects and always delivered what we promised. Find the showcase of the work delivered by us.')
@section('image')
<meta property="og:image" content="{{asset('public/img/work-bg.png')}}" />
<meta name="twitter:image" content="{{asset('public/img/work-bg.png')}}" />
@endsection

@section('content')
<main class="pb-lg-5">
    <div class="container-fluid" style="background: url('public/img/work-banner-bg.png')no-repeat center / cover;">
        <div class="row align-items-center flex-lg-row-reverse min-vh-100">
            <div class="col-lg-6 col-xl-6 pl-xl-5 text-white mb-4 mb-lg-0">
                <img src="{{asset('public/img/work-bg.png')}}" alt="Our Work" class="img-fluid thumbnail-left">
            </div>
            <div class="col-lg-6 col-xl-6 pl-xl-5 text-white">
                <h4 class="h3 font-weight-bold mb-xl-3" data-aos="fade-up">Our Work</h4>
                <h1 class="font-weight-bold display-3 mb-xl-3" data-aos="fade-up" data-aos-delay="600">Our portfolio is our Identity</h1>
                <p class="mb-xl-3 text-white" data-aos="fade-up" data-aos-delay="1200">Our portfolio is a sheer presentation of our industry experience, collaboration, expertise, and quality deliverables.</p>
            </div>
        </div>
    </div>

    </section>
    <div id="rxtNavTravel" class="headerOnTravel"></div>


    <section class="py-3 py-md-5" style="background: url('public/img/work-odd-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row flex-md-row-reverse align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/presenta/list.png')}}" alt="Presenta Application" class="img-fluid tool-tilt thumbnail">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Presenta Application</div>
                    </h2>

                    <p class="text-white">Presenta is an AI-enabled accounting solution for Android and iOS platforms. With the help of this application, it becomes easy to do some tough accounting, taxation, and payroll tasks. We have built an app that helps our client to get financial reports with a few clicks. This app not only reduces human effort but also saves valuable time.</p>
                    <a href="{{Route('presenta')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-md-5" style="background: url('public/img/work-even-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/holdeed/list.png')}}" alt="Holdeed" class="img-fluid tool-tilt thumbnail-left">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Holdeed</div>
                    </h2>
                    <p class="text-white">Holdeed is an administrative software that simplifies project management, order processing, time management, leave management, and construction documentation tasks. This platform unifies the needs of SMEs, construction companies, and helps them in saving hard earned money. It connects directly to employees, even when they are working outside the office. </p>
                    <a href="{{Route('holdeed')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-md-5" style="background: url('public/img/work-odd-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row flex-md-row-reverse align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/gpbo/list.png')}}" alt="GPBO" class="img-fluid tool-tilt thumbnail">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">GPBO</div>
                    </h2>
                    <p class="text-white">Gujarat Patidar Business Organization a. k. a GPBO is an Android and iOS based app developed to connect the dots between the business community of the Patidar Samaj. It is a platform where business opportunities providers and seekers can connect.</p>
                    <a href="{{Route('gpbo')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-md-5" style="background: url('public/img/work-even-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/joykiks/list.png')}}" alt="Joykick" class="img-fluid tool-tilt thumbnail-left">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Joykicks</div>
                    </h2>
                    <p class="text-white">JoyKicks is an e-commerce app based on Android and iOS. It is a platform where consumers can get connected to the service provider for the need of party packages, catering, rental products, and inflatable bouncers goods.</p>
                    <a href="{{Route('joykick')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-md-5" style="background: url('public/img/work-odd-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row flex-md-row-reverse align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/simpilli/list.png')}}" alt="Simpilli" class="img-fluid tool-tilt thumbnail">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Simpilli</div>
                    </h2>
                    <p class="text-white">Time to say goodbye to long queues of the pharmacy store with simpilli app. It is a mobile and web app, helping deliver medicine at your doorstep with no contact delivery. The platform connects consumers and nearby pharmacies to deliver the prescribed medicines. Its interaction features i.e. real-time chat, audio, and video call can help to remove all queries related to the medication.</p>
                    <a href="{{Route('simpilli')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-3 py-md-5" style="background: url('public/img/work-even-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/ai/list.png')}}" alt="AI" class="img-fluid tool-tilt thumbnail-left">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Call Hippo</div>
                    </h2>
                    <p class="text-white">CallHippo is a cloud call center solution designed to meet the requirements of small, medium, large businesses, and startups. Not only that, it gives a complete analysis of call through call recording, live call monitoring, routing, and other striking features.</p>
                    <a href="{{Route('callhippo')}}" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

   <!-- <section class="py-3 py-md-5" style="background: url('public/img/work-even-bg.png')no-repeat center / cover;">
        <div class="container py-xl-5 text-white">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{asset('public/img/portfolio/ai/list.png')}}" alt="AI" class="img-fluid tool-tilt thumbnail-left">
                </div>
                <div class="col-md-6">
                    <h2>
                        <div class="h1 font-weight-bold">Artificial Intelligence</div>
                    </h2>
                    <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates sapiente pariatur placeat exercitationem, veritatis officia totam impedit corrupti dolorem quod? Laudantium molestias necessitatibus ut sequi veniam quas sint error?</p>
                    <a href="#" class="btn btn-accent">View Project <i class="ml-2 fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>
-->

    <div class="mb-5"></div>

</main>
@endsection

@section('script')
<script>
    $('#count-locations').jQuerySimpleCounter({
        start: 0,
        end: 4,
        duration: 5000
    });
    $('#count-projects').jQuerySimpleCounter({
        start: 0,
        end: 250,
        duration: 20000
    });
    $('#count-years').jQuerySimpleCounter({
        start: 0,
        end: 7,
        duration: 10000
    });
    $('#count-employees').jQuerySimpleCounter({
        start: 0,
        end: 50,
        duration: 10000
    });
</script>
@endsection