<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'WebsiteController@home')->name('index');
Route::get('/page-not-found/', function () {
    return view('404');
})->name('pagenotfound');
Route::get('/about-us/', 'WebsiteController@about')->name('about');
Route::get('/services/', 'WebsiteController@service')->name('service');
Route::get('/work/', 'WebsiteController@work')->name('work');
Route::get('/blog', 'WebsiteController@blog')->name('blog');
Route::get('/contact-us/', 'WebsiteController@contact')->name('contact');

Route::get('/web-development/', function () {
    return view('web-development');
})->name('web-development');

Route::get('/mobile-app-development/', function () {
    return view('mobile-app-development');
})->name('mobile-app-development');

Route::get('/custom-solutions/', function () {
    return view('custom-solutions');
})->name('custom-solutions');

Route::get('/artificial-intelligence-development/', function () {
    return view('artificial-intelligence-development');
})->name('artificial-intelligence-development');

// Route::get('/hire-dedicated-developer/', function () {
//     return view('hire-dedicated-developer');
// })->name('hire-dedicated-developer');

Route::get('/artificial-intelligence-give-a-beautiful-wing-to-future-reality/', function () {
    return view('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality');
})->name('artificial-Intelligence-Give-a-Beautiful-Wing-to-Future-Reality');

Route::get('/who-is-the-ultimate-winner-hybrid-vs-native-app/', function () {
    return view('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App');
})->name('Who-is-the-Ultimate-Winner-Hybrid-vs-Native-App');

Route::get('/which-is-the-best-flutter-or-react-native/', function () {
    return view('Which-is-the-Best-Flutter-or-React-Native');
})->name('Which-is-the-Best-Flutter-or-React-Native');

Route::get('/what-to-choose-open-source-or-custom-development/', function () {
    return view('What-to-choose-Open-Source-or-Custom-Development');
})->name('What-to-choose-Open-Source-or-Custom-Development');

Route::get('/Do-you-know-the-Top-Web-Design-Trends-of-the-Year/', function () {
    return view('Do-you-know-the-Top-Web-Design-Trends-of-the-Year');
})->name('Do-you-know-the-Top-Web-Design-Trends-of-the-Year');

Route::get('/Zip-Zap-Zoom-Its-Picnic-Time/', function () {
    return view('Zip-Zap-Zoom-Its-Picnic-Time');
})->name('Zip-Zap-Zoom-Its-Picnic-Time');

Route::get('/gpbo-case-study/', function () {
    return view('gpbo-case-study');
})->name('gpbo');

Route::get('/holdeed-case-study/', function () {
    return view('holdeed-case-study');
})->name('holdeed');

Route::get('/presenta-case-study/', function () {
    return view('presenta-case-study');
})->name('presenta');

Route::get('/simpilli-case-study/', function () {
    return view('simpilli-case-study');
})->name('simpilli');

Route::get('/joykicks-case-study/', function () {
    return view('joykicks-case-study');
})->name('joykick');

Route::get('/callhippo-case-study/', function () {
    return view('call-hippo-case-study');
})->name('callhippo');

Route::post('/thank_you/', function () {
    return view('thank');
})->name('thank');



Route::post('mail', 'WebsiteController@email')->name('mail');
Route::post('discuss', 'WebsiteController@discusss')->name('discuss');

